using System;
using UnityEngine;

namespace com.tiledmedia.clearvr.demos {
	public static class Helpers {
        #if UNITY_ANDROID && !UNITY_EDITOR
            public const string DEFAULT_LICENSE_FILE_FOLDER = "/storage/emulated/0/";
        #else
            #if UNITY_IOS && !UNITY_EDITOR
                public const string DEFAULT_LICENSE_FILE_FOLDER = "license.tml";
            #else
                public const string DEFAULT_LICENSE_FILE_FOLDER = "";
            #endif
        #endif

        public static byte[] ReadLicenseFile(string argOverrideFolderAndOrFilename = DEFAULT_LICENSE_FILE_FOLDER) {
            #if UNITY_ANDROID && !UNITY_EDITOR
                return ReadLicenseFileFromPhoneStorage(argOverrideFolderAndOrFilename);
            #endif
            #if UNITY_IOS && !UNITY_EDITOR
                return ReadLicenseFileFromResourceFolder(argOverrideFolderAndOrFilename);
            #endif
			#pragma warning disable 0162 // Unreachable code
            return null;
			#pragma warning restore 0162 // Unreachable code
        }

        /*
        Searches for a .tml (Tiledmedia License file) in the root of the phone storage.
        Subsequently, it will read its contents in a byte array which is used by ClearVRForUnity.
        If you are building your own player, you would typically include this license file as a Resource
        in the Assets folder or burn it into the source code on buildtime.
        */
        public static byte[] ReadLicenseFileFromPhoneStorage(string argFolderFullPath) {
            String licenseFileFullPath = "";
            string[] files;
            try {
                files = System.IO.Directory.GetFiles(argFolderFullPath, "*.tml");
            } catch (Exception e) {
                throw new Exception(String.Format("[ClearVR] An error was reported while looking for license (.tml) file in {0}. Error: {1}", argFolderFullPath, e));
            }
            if(files.Length > 0){
                licenseFileFullPath = files[0];	
            } else {
                return null;
            }
            try {
                return System.IO.File.ReadAllBytes(licenseFileFullPath);
            } catch (Exception e) {
                #if UNITY_ANDROID && !UNITY_EDITOR
                    throw new Exception(String.Format("[ClearVR] Cannot read license file from {0}. Error: {1}. Please allow READ permission via Settings -> Apps -> AppName -> Permissions.", licenseFileFullPath, e));
                #else
                    throw new Exception(String.Format("[ClearVR] Cannot read license file from {0}. Error: {1}.", licenseFileFullPath, e));
                #endif
            }
        }

        public static byte[] ReadLicenseFileFromResourceFolder(String argOverrideFilename = "") {
            argOverrideFilename = argOverrideFilename.Replace(".tml", "");
            TextAsset licenseFile = Resources.Load(argOverrideFilename.Equals("") ? "license" : argOverrideFilename) as TextAsset;
            return licenseFile.bytes;
        }

        public static string GetTimeInMillisecondsAsPrettyString(long argTimeInMilliseconds){
            TimeSpan t = TimeSpan.FromMilliseconds(argTimeInMilliseconds);
            return String.Format("{0:D2}:{1:D2}.{2:D3}", 
                                    t.Minutes, 
                                    t.Seconds, 
                                    t.Milliseconds);
        }

		/// <summary>
		/// This method loads a content list json file located in the Assets/Resources/ folder.
		/// </summary>
		/// <returns>true in case of success, false if something went wrong.</returns>
		public static bool LoadContentList(String argContentListFileName, ref ContentItemList argContentItemList) {
			string filePath = argContentListFileName.Replace(".json", ""); // we have to strip the file extension
			TextAsset contentListTextAsset;
			try {
				contentListTextAsset = Resources.Load<TextAsset>(filePath);
			} catch (Exception argException) {
				UnityEngine.Debug.LogError(String.Format("An error was thrown while reading content list: {0}. Error: {1}", argContentListFileName, argException));
				return false;
			}
            try {
                argContentItemList = JsonUtility.FromJson<ContentItemList>(contentListTextAsset.text);
            } catch (Exception argException) {
				throw new Exception(String.Format("Cannot parse content list file: {0}. Error: {1}", argContentListFileName, argException));
            }
            return true;
        }


        /// <summary>
        /// Prints the contents of a ClearVRCoreWrapperVideoDecoderCapabilities event. 
        /// </summary>
        /// <param name="argClearVREvent">The ClearVREvent object to parse.</param>
        /// <param name="argClearVRPlayer">The ClearVRPlayer object that spawned this event.</param>
		[Obsolete("Common.ParseClearVRCoreWrapperVideoDecoderCapabilitiesEvent() is deprecated. Use Common.PrintClearVRCoreWrapperVideoDecoderCapabilitiesEvent() instead.", true)] 

		public static void ParseClearVRCoreWrapperVideoDecoderCapabilitiesEvent(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer) {
			PrintClearVRCoreWrapperVideoDecoderCapabilitiesEvent(argClearVREvent, argClearVRPlayer);
		}

        /// <summary>
        /// Prints the contents of a ClearVRCoreWrapperVideoDecoderCapabilities event. 
        /// </summary>
        /// <param name="argClearVREvent">The ClearVREvent object to parse.</param>
        /// <param name="argClearVRPlayer">The ClearVRPlayer object that spawned this event.</param>
		public static void PrintClearVRCoreWrapperVideoDecoderCapabilitiesEvent(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer) {
			/* This message holds some information on the video decoder that is use. This information includes: 
				* 1. the decoder name (e.g. "OMX.decoder.hevc")
				* 2. the decoder mimetype (e.g. "video/hevc")
				* 3. the reported maximum supported decoder codec level
				* 4. the reported maximum suported codec level by the SDK
				* 5. the reported maximum supported codec level currently in use. 
				* Note that the SDK only supports up to and including HEVC codec level 5.2.
			*/
			if(argClearVREvent.message.GetIsSuccess()) {
				VideoDecoderCapabilities videoDecoderCapabilities;
				if(argClearVREvent.message.ParseClearVRCoreWrapperVideoDecoderCapabilities(out videoDecoderCapabilities)) {
					UnityEngine.Debug.Log(String.Format("[ClearVR] Video decoder capabilities reported. Name: {0}, mimetype: {1}, max/supported/active codec level: {2}/{3}/{4}.", videoDecoderCapabilities.videoDecoderName, videoDecoderCapabilities.mimetype, videoDecoderCapabilities.decoderMaximumCodecLevelSupported, videoDecoderCapabilities.clearVRSdkMaximumCodecLevelSupported, videoDecoderCapabilities.maximumCodecLevelInUse));
				} else {
					UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An error was thrown while parsing video decoder capabilities. {0}", argClearVREvent.message.GetFullMessage()));
				}
			} else {
				if(ParseWarningOrFatalErrorMessage(argClearVREvent, "parsing video decoder capabilities")) {
					argClearVRPlayer.controller.Stop();
				}
			}
		}

		/// <summary>
		/// Log a standard message when a warning or fatal error was returned. 
        /// Notes:
        /// 1. Do not use this to parse a success message.
		/// </summary>
        /// <param name="argClearVREvent">The event that needs to be parsed.</param>  /// 
		/// <param name="argOptionalString">String describing what was going on when the specified event was triggered.</param>
		/// <return>true if a FatalError was parsed, false otherwise.</return>
		public static bool ParseWarningOrFatalErrorMessage(ClearVREvent argClearVREvent, String argOptionalString) {
			if(argClearVREvent.message.GetIsWarning()) {
				UnityEngine.Debug.LogWarning(String.Format("[ClearVR] Warning reported while {0}. {1}", argOptionalString, argClearVREvent.message.GetFullMessage()));
			} else if(argClearVREvent.message.GetIsFatalError()) {
				UnityEngine.Debug.LogError(String.Format("[ClearVR] Fatal error reported while {0}. {1}", argOptionalString, argClearVREvent.message.GetFullMessage()));
				return true;
			}
			return false;
		}


		public static void ParseClearVRCoreWrapperStereoscopicModeChangedEvent(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer) {
			if(argClearVREvent.message.GetIsSuccess()) {
				UnityEngine.Debug.Log(String.Format("[ClearVR] Stereo mode changed to: {0}.", argClearVREvent.message.GetFullMessage()));
			} else {
				if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, "changing stereo mode")) {
					argClearVRPlayer.controller.Stop();
				}
			}
		}

		public static void ParseMediaInfoParsedEvent(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer) {
			/* Make sure that parsing media info was successful */
			if(argClearVREvent.message.GetIsSuccess()) {
				if(argClearVRPlayer.platformOptions.autoPrepareContentItem == null) {
					/* No content item specified to prepare automatically. It is suggested to do this here. */
					//argClearVRPlayer.mediaPlayer.PrepareContentForPlayout(YourContentItem);
				}
			} else {
				throw new Exception ("[ClearVR] An error was reported while parsing media info. Cannot continue");
			}
		}

		/// <summary>
		/// Parses the ClearVRCoreWrapperAduioTrackChangedEvent.
		/// </summary>
		/// <param name="argClearVREvent">The ClearVREvent to parse</param>
		/// <param name="argClearVRPlayer">the calling ClearVRPlayer object</param>
		/// <param name="argUserInterface">The user interface object</param>
		/// <returns>The new active audio track index, or -1 if no audio track is specified/something unexpected went wrong.</returns>
		public static int ParseClearVRCoreWrapperAudioTrackChangedEvent(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer, UserInterface argUserInterface) {
			int newAudioTrackIndex = -1;
			if(argClearVREvent.message.GetIsSuccess()) {
				AudioDecoder newAudioDecoder;
				AudioPlaybackEngine newAudioPlaybackEngine;
				if(argClearVREvent.message.ParseAudioTrackChanged(out newAudioTrackIndex, out newAudioDecoder, out newAudioPlaybackEngine)) {
					UnityEngine.Debug.Log(String.Format("[ClearVR] Audio track switched. Track index: {0}, decoder: {1}, playback engine: {2}", newAudioTrackIndex, newAudioDecoder.audioDecoderType.ToString(), newAudioPlaybackEngine.audioPlaybackEngineType.ToString()));
					if(argUserInterface != null) {
						if(newAudioTrackIndex >= 0) {
							// Let's demonstrate how we can query some of the selected audio track parameters:
							try{
								int numberOfChannels = Convert.ToInt32(argClearVRPlayer.mediaPlayer.GetClearVRCoreArrayParameter("media.audio.num_channels", newAudioTrackIndex));
								int sampleRate = Convert.ToInt32(argClearVRPlayer.mediaPlayer.GetClearVRCoreArrayParameter("media.audio.samplerate", newAudioTrackIndex));
								bool isAmbisonic = Boolean.Parse(argClearVRPlayer.mediaPlayer.GetClearVRCoreArrayParameter("media.audio.is_spatial_audio", newAudioTrackIndex));
								argUserInterface.UpdateAudioTrackStringText(String.Format("Track: {0}/{1}. C: {2}, R: {3} {4}", newAudioTrackIndex + 1, argClearVRPlayer.mediaInfo.GetNumberOfAudioTracks(), numberOfChannels, sampleRate, isAmbisonic ? "(ambix)" : ""));
							}catch (Exception e) {
								UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An error occured while getting audio track properties. Error: {0}", e));
								return newAudioTrackIndex;
							}
						} else {
							argUserInterface.UpdateAudioTrackStringText("Audio track disabled");
						}
					} else {
						// pass, apparently there is no user interface?
					}
				} else {
					UnityEngine.Debug.LogError(String.Format("[ClearVR] An unexpected error occured while parsing message. Expected three semicolon-separated values but got {0} instead.", argClearVREvent.message.GetFullMessage()));
				}
			} else {
				UnityEngine.Debug.LogWarning(String.Format("[ClearVR] Unable to switch to audio track. {0}", argClearVREvent.message.GetFullMessage()));
			}
			return newAudioTrackIndex;
		}

		/// <summary>
		/// Close/push the the application to the background.
		/// 
		/// Notes:
		/// 1. One should not call Application.Quit() on iOS. It is against the rationale of the platform (refer to https://docs.unity3d.com/ScriptReference/Application.Quit.html and https://developer.apple.com/library/archive/qa/qa1561/_index.html#//apple_ref/doc/uid/DTS40007952). If you want to quit the app on exit, specify the "Behaviour in Background" option under Unity's Project Settings.
		/// 2. On Android, calling Application.Quit() can result in instabilities. One should generally consider putting the application to the background instead.
		/// </summary>
		public static void CloseApplication() {
			#if UNITY_ANDROID
			// We push the app to the background instead of calling Application.Quit()
			if (Application.platform == RuntimePlatform.Android) {
				AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
				activity.Call<bool>("moveTaskToBack", true);
				return;
			}
			#endif
			Application.Quit();
		}
    }
}