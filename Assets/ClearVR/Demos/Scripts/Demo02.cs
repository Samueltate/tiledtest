﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.tiledmedia.clearvr.demos {
	public class Demo02 : MonoBehaviour {
		// Note that the playlist items have been moved to a simple json file located in Assets/Resources/ since v3.2rc4. 
		// If you have access to your own ClearVR-encoded content, it is suggested to copy the default content-list.json and 
		// populate it with your own URLs.
		
		// This specifies the file name to load from Assets/Resources/. Must end with .json
		private string  		CONTENT_LIST_FILE_NAME = "content-list.json";
		private ContentItemList contentItemList = null;
		private int				contentItemIndex = 0;
		private UserInterface 	userInterface = null;
		private ClearVRPlayer 	clearVRPlayer = null;
		protected byte[] 		licenseFileBytes = null;
		private GameObject 		mediaPlayerGameObject = null;
		private bool 			isApplicationShutdownRequested = false;
		private bool 			isApplicationShutdownScheduled = false;
		private bool			isUpdateUICoroutineRunning = false;
		private int				audioTrackIndex = -1;

		void Awake() {
#if UNITY_2018_1_OR_NEWER
			UnityEngine.XR.InputTracking.Recenter();
#else
			UnityEngine.VR.InputTracking.Recenter();
#endif
			Application.targetFrameRate = 60;
		}

		/// <summary>
		/// Set-up the scene
		/// </summary>
		void Start () {
			#if UNITY_ANDROID && !UNITY_EDITOR
				Screen.orientation = ScreenOrientation.LandscapeLeft;
				Screen.sleepTimeout = SleepTimeout.NeverSleep;
			#endif
			// Set a higher render scale when GearVR is present (see: https://unity3d.com/learn/tutorials/topics/virtual-reality/getting-started-vr-development - Render Scale)
			// Note that this can have a negative effect on performance, so you can tune this value if you run into performance bottlenecks. 
			if(Utils.GetIsVrDevicePresent()) {
#if UNITY_2018_1_OR_NEWER
				UnityEngine.XR.XRSettings.eyeTextureResolutionScale = 1.5f;
#else
				UnityEngine.VR.VRSettings.eyeTextureWidth = 1.5f;
#endif
			} else {
				// make sure we enforce VSYNC 
				QualitySettings.vSyncCount = 1;
			}
			if(!Helpers.LoadContentList(CONTENT_LIST_FILE_NAME, ref contentItemList)) {
				throw new Exception("[ClearVR] An error was reported while loading the content list. Cannot continue.");
			}
			// Register callback action signal triggered by UI interaction
			userInterface = gameObject.GetComponent<UserInterface>();
			userInterface.SetCbPlayerInteractionRequest(CbPlayerInteractionRequest);
			InitializeClearVRPlayer();
		}

		/// <summary>
		/// /// Initialize ClearVR Player by creating a GameObject to which we can attach our ClearVRPlayer script and, subsequently, initializing the platform-specific implementation 
		/// </summary>
		/// <param name="argStartPositionInMilliseconds">The content position in milliseconds from where to start playback.</param>
		void InitializeClearVRPlayer(long argStartPositionInMilliseconds = 0) {
			if(clearVRPlayer == null && mediaPlayerGameObject == null) {
				/* Check if ClearVR is supported on the current platform */
				if(!ClearVRPlayer.GetIsPlatformSupported()) {
					throw new Exception("[ClearVR] Sorry, ClearVR is not yet supported on this platform!");
				}
				/* In the current version of the SDK, one must reinitialize the underlying libraries again after you have finished watching a clip, unless you use the switchContent() API */
				ContentItem contentItem = contentItemList.content_items[contentItemIndex];
				contentItem.startPositionInMilliseconds = argStartPositionInMilliseconds; // the position in the clip where one wants playback to start (in milliseconds).
				if(licenseFileBytes == null) {
					/* Read license file from default license file folder */
					licenseFileBytes = Helpers.ReadLicenseFile();
					if(licenseFileBytes == null) {
						throw new Exception(String.Format("[ClearVR] Unable to read license file: {0}!", Helpers.DEFAULT_LICENSE_FILE_FOLDER));
					}
				}
				/* Create a parent gameobject to which we can attach our ClearVRPlayer script. */
				mediaPlayerGameObject = new GameObject("ClearVRPlayer");
				clearVRPlayer = mediaPlayerGameObject.AddComponent<ClearVRPlayer>();
				/* Add event listener */
				clearVRPlayer.clearVREvents.AddListener(CbClearVREvent);
				/* Set platform/player options. Note that changing any of these options AFTER calling clearVRPlayer.Initialize() will result in undefined behaviour and is NOT supported. */
				PlatformOptionsBase platformOptions = (PlatformOptionsBase) clearVRPlayer.GetDefaultPlatformOptions();
				platformOptions.licenseFileBytes = licenseFileBytes; /* Byte array containing your private license file data */
				platformOptions.trackingTransform = userInterface.camera.transform; /* The transform of the currently active Camera, e.g. MainCamera or the CenterEyeAnchor on your OVRCameraRig */
				platformOptions.preferredRenderMode = RenderModes.Native; /* Native (the default value) will make sure that the appropriate RenderMode will be selected according to the active (headset) device.  */
				platformOptions.autoPrepareContentItem = contentItem; /* The content item to load */
				platformOptions.autoPlay = true; /* start playout after content has been loaded and buffered */
				platformOptions.loopContent = true; /* whether to loop content or not, note that for LIVE events looping should typically be disabled. */
				platformOptions.audioPlaybackEngine = null; /* null means: let platform-specific MediaPlayer use the default audio playback engine. */
				platformOptions.sphereRadius = 15f; /* A radius of 15 is the default value. You can change this value according to your needs. */
				platformOptions.sphereLayer = 0; /* The layer index on which the sphere will be rendered. */
				platformOptions.enableAutomaticRenderModeSwitching = true; /* By default (true), rendering will change when the core toggles between mono or stereo  mode. You can override this yourself by setting this option to false and implemeting your own ClearVREventTypes.StereoModeSwitched event parser */
				platformOptions.prepareContentForPlayoutTimeoutInMilliseconds = 0; /* Since v4.2: override the default content loading timeout (30000) by setting this parameter > 0. Any value <= 0 will be interpreted as this default value. */
				
				/* To enable or disable ABR, please check the "playback.enable_automatic_abr" parameter which is set in the ClearVREventTypes.StateChangedContentPreparedForPlayout event (see below) in this sample script. */
				
				/* 
				 * There are three modes to handle application focus and pause changes:
				 * 1. Legacy: 		always put player to pause when app looses focus or is paused: drains battery as the player will remain active and resources will not be freed.
				 * 2. Recommended: 	player is killed when app is paused (e.g. pushed to background); player is paused when app looses focus (e.g. in case of a pop-up or when the user puts down the headset). 
				 *                  ClearVREventTypes.StateChangedStopped below demonstrates how one could resume playback when the app resumes after it was paused. Note that this
				 *                  behaviour might not suit your requirements, so please check it thoroughly.
				 * 3. Disabled: 	handling app focus/pause is left to you as an integrator. Use with care!
				 */
				platformOptions.applicationFocusAndPauseHandling = ApplicationFocusAndPauseHandlingTypes.Recommended; 
                #if UNITY_ANDROID
                    /* Set Android-platform specific options */
                    ((PlatformOptionsAndroid)platformOptions).isUseOESFastPathEnabled = true; /* In general, enabling OES Fast Path is recommended for improved performance */
                #endif
                #if UNITY_IOS
                    /* Set iOS-platform specific options */
                    ((PlatformOptionsIOS)platformOptions).isUseOESFastPathEnabled = true; /* In general, enabling OES Fast Path is recommended for improved performance */
                #endif
				// Schedule a request to Initialize a ClearVRPlayer object. Depending on the specified platformOptions, you will receive a callback sooner or later.
				clearVRPlayer.Initialize(platformOptions, 
					new Action<ClearVREvent, ClearVRPlayer>(delegate(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer) {
						// Check the result. Did the request succeed?
						// This callback is fired on the main Unity thread, as such you should refrain from doing any heavy processing in here as it will block your application thread!
						if(argClearVREvent.message.GetIsSuccess()) { 
							UnityEngine.Debug.Log(String.Format("[ClearVR] ClearVRPlayer initialization: success."));
						} else {
							if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, "ClearVRPlayer initialization")) {
								// returns true if something really bad happened
								argClearVRPlayer.controller.Stop();
							}
						}
					})
				);
			} else {
				UnityEngine.Debug.Log("[ClearVR] Cannot load a second player. Stop the current player first.");
			}
		}

		/// <summary>
		/// This perpetual loop is started on scene load and will query ClearVRCore for the current average bitrate and content time.
		/// Note that we can only request this parameter after ClearVRCore has competely loaded the content. The platform specific implementation of the player
		/// (e.g. MediaPlayerAndroid) will make sure that when you call any of these methods at a bad time, you will simply get 0 as return value.
		/// See also the ClearVRCore documentation.
		/// </summary>
		IEnumerator UpdateUI() {
			if(isUpdateUICoroutineRunning) {
				yield break;
			}
			yield return null; // wait one frame
			WaitForSeconds delay = new WaitForSeconds(1.0f);
			isUpdateUICoroutineRunning = true;
			while(isUpdateUICoroutineRunning) {
				if(clearVRPlayer != null && userInterface != null) {
					if(clearVRPlayer.performance != null && clearVRPlayer.controller != null) {
						userInterface.UpdateAverageBitrateText(clearVRPlayer.performance.GetAverageBitrateInMbps().ToString() + " Mbps");
						userInterface.UpdateCurrentContentTimeText(Helpers.GetTimeInMillisecondsAsPrettyString(clearVRPlayer.controller.GetCurrentContentTimeInMilliseconds()) + "/" + Helpers.GetTimeInMillisecondsAsPrettyString(clearVRPlayer.mediaInfo.GetContentDurationInMilliseconds()));
					}
				}
				yield return delay;
			}
		}

		/// <summary>
		/// Update is called once per frame
		/// </summary>
		void Update() {
			CheckForInput();
			if(userInterface != null) {
				userInterface.RefreshStatusTextLabel();
			}
			if(isApplicationShutdownScheduled) {
				isApplicationShutdownScheduled = false;
				Helpers.CloseApplication();
			}
		}

		/// <summary>
		/// Check for user input
		/// </summary>
		void CheckForInput() {
			// Close app if back button is pressed
			if(Input.GetKeyDown(KeyCode.Escape)) {
				isApplicationShutdownRequested = true;
				if(clearVRPlayer != null) {
					clearVRPlayer.controller.Stop();
				} else {
					isApplicationShutdownScheduled = true;
				}
				return;
			}
		}

		/// <summary>
		/// Callback function that handles ClearVRPlayer events.
		/// </summary>
		/// <param name="argClearVRPlayer">the ClearVRPlayer instance that fired the event.</param>
		/// <param name="argClearVREvent">the ClearVREvent that was fired.</param>
		void CbClearVREvent(ClearVRPlayer argClearVRPlayer, ClearVREvent argClearVREvent) {
			//argClearVREvent.Print(); // Enable this line to print the events that are received. Can be handy for debugging purposes.
			/* Parse the event */
			switch (argClearVREvent.type) {
				/* Remember that StateChanged* events are ALWAYS successful as the Core will only change state in case of success. */
				case ClearVREventTypes.StateChangedUninitialized:
					/* This StateChanged event will never be triggered since no listener is attached by the time this happens. */
					/* In other words, you can safely ignore this state transition. It is only listed for completeness sake */
					break;
				case ClearVREventTypes.StateChangedInitializing:
					/* Transient state, do not interfere */
					break;
				case ClearVREventTypes.StateChangedInitialized:
					/* ClearVRPlayer object will already take care of preparing the core, do not interfere */
					break;
				case ClearVREventTypes.StateChangedPreparingCore:
					/* transient state, do not interfere */
					break;
				case ClearVREventTypes.StateChangedCorePrepared:
					/* if platformOptions.autoPrepareContentItem != null, ClearVRPlayer will make sure that MediaInfo will be called automatically when we hit this state.
					Therefore, the next events that you would typically receive in that case are ClearVREventTypes.ParsingMediaInfo and ClearVREventTypes.MediaInfoParsed.
					Subsequently, ClearVRPlayer will start preparing the content for playout. */
					if(argClearVRPlayer.platformOptions.autoPrepareContentItem == null) {
						// In this case, this would be a good spot to parse media info.
						//argClearVRPlayer.mediaPlayer.ParseMediaInfo(yourContentItem)
					}
					break;
				case ClearVREventTypes.StateChangedPreparingContentForPlayout:
					break;
				case ClearVREventTypes.StateChangedContentPreparedForPlayout:
					/* if platformOptions.autoPlay == true, ClearVRPlayer will make sure that content will start playing automatically next. */
					argClearVRPlayer.mediaPlayer.SetClearVRCoreParameter("playback.enable_automatic_abr", "false");
					break;
				case ClearVREventTypes.StateChangedBuffering:
					break;
				case ClearVREventTypes.StateChangedPlaying:
					break;
				case ClearVREventTypes.StateChangedPausing:
					break;
				case ClearVREventTypes.StateChangedPaused:
					break;
				case ClearVREventTypes.StateChangedSeeking:
					break;
				case ClearVREventTypes.StateChangedSwitchingContent:
					break;
				case ClearVREventTypes.StateChangedFinished:
					break;
				case ClearVREventTypes.StateChangedStopping:
					break;
				case ClearVREventTypes.StateChangedStopped:
					ParseStateChangedStoppedEvent(argClearVREvent, argClearVRPlayer);
					break;
				/* Events not tied to state changes. These events can fail. */
				case ClearVREventTypes.ParsingMediaInfo:
					/* This is a transient state, it is highly recommended to not do anything and wait for ClearVREventTypes.MediaInfoParsed to be triggered. */
					break;
				case ClearVREventTypes.MediaInfoParsed:
					Helpers.ParseMediaInfoParsedEvent(argClearVREvent, argClearVRPlayer);
					break;
				case ClearVREventTypes.RenderModeChanged:
					if(argClearVREvent.message.GetIsSuccess()) {
						String newRenderModeText = "";
						if(argClearVRPlayer.renderMode == RenderModes.Monoscopic) {
							newRenderModeText = "Mono";
						} else if(argClearVRPlayer.renderMode == RenderModes.Stereoscopic) {
							newRenderModeText = "Stereo";
						} else if(argClearVRPlayer.renderMode == RenderModes.Native) {
							newRenderModeText = "Native";
						}
						if(userInterface != null) {
							userInterface.UpdateIsMonoOrStereoText(newRenderModeText);
						}
					} else {
						if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, "switching render mode")) {
							argClearVRPlayer.controller.Stop();
						}
					}
					break;
				case ClearVREventTypes.FirstFrameRendered:
					/* Yay! We just rendered the first frame of the selected video.*/
					if(!(argClearVREvent.message.GetIsSuccess())) {
						UnityEngine.Debug.LogError(String.Format("[ClearVR] An error was reported while rendering the first video frame. {0}", argClearVREvent.message.GetFullMessage()));
					}
					if(!isUpdateUICoroutineRunning) {
						StartCoroutine(UpdateUI());
					}
					break;
				case ClearVREventTypes.AudioTrackSwitched:
					if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, "audio track switched")) {
						argClearVRPlayer.controller.Stop();
					}
					break;
				case ClearVREventTypes.ContentSwitched:
					if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, "content switch")) {
						argClearVRPlayer.controller.Stop();
					}
					break;
				case ClearVREventTypes.UnableToInitializePlayer:
					if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, "initializing player")) {
						argClearVRPlayer.controller.Stop();
					}
					break;
				case ClearVREventTypes.ResumingPlaybackAfterApplicationPaused:
					UnityEngine.Debug.Log(String.Format("[ClearVR] Resuming playback after application was paused."));
					break;
				case ClearVREventTypes.StereoModeSwitched:
					if(argClearVREvent.message.GetIsSuccess()) {
						UnityEngine.Debug.Log("[ClearVR] Stereo mode switched successfully.");
					} else {
						if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, "stereo mode switched")) {
							argClearVRPlayer.controller.Stop();
						}
					}
					break;
				// Since v4.0
				case ClearVREventTypes.ContentFormatChanged:
					if(argClearVREvent.message.GetIsSuccess()) {
						UnityEngine.Debug.Log("[ClearVR] Content format changed successfully.");
					} else {
						if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, "content format change")) {
							argClearVRPlayer.controller.Stop();
						}
					}
					break;
				case ClearVREventTypes.ABRSwitch:
					/* Reserved callback, not used yet in v4.0 */
					break;
				case ClearVREventTypes.PrewarmCacheCompleted:
					/* Request cache prewarm completed. */
					if(argClearVREvent.message.GetIsSuccess()) {
						UnityEngine.Debug.Log("[ClearVR] Prewarming cache completed.");
					} else {
						if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, "prewarming cache")) {
							argClearVRPlayer.controller.Stop();
						}
					}
					break;
				case ClearVREventTypes.GenericMessage:
					switch(argClearVREvent.message.type) {
						case ClearVRMessageTypes.FatalError:
						case ClearVRMessageTypes.Warning:
							if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, "something happened")) {
								argClearVRPlayer.controller.Stop();
							}
							break;
						case ClearVRMessageTypes.Info:
							/* Parse harmless info messages */
							switch (argClearVREvent.message.code) {
								case (int) ClearVRMessageCodes.ClearVRCoreWrapperGenericInfo:
									break;
								case (int) ClearVRMessageCodes.ClearVRCorWrapperOpenGLVersionInfo:
									UnityEngine.Debug.Log(String.Format("[ClearVR] Detected OpenGL version: {0}.x", argClearVREvent.message.GetFullMessage()));
									break;
								case (int) ClearVRMessageCodes.ClearVRCoreWrapperVideoDecoderCapabilities:
									Helpers.PrintClearVRCoreWrapperVideoDecoderCapabilitiesEvent(argClearVREvent, argClearVRPlayer);
									break;
								case (int) ClearVRMessageCodes.GenericOK:
									break;
								case (int) ClearVRMessageCodes.ClearVRCoreWrapperAudioTrackChanged:
									audioTrackIndex = Helpers.ParseClearVRCoreWrapperAudioTrackChangedEvent(argClearVREvent, argClearVRPlayer, userInterface);
									break;
								case (int) ClearVRMessageCodes.ClearVRCoreWrapperStereoscopicModeChanged:
									Helpers.ParseClearVRCoreWrapperStereoscopicModeChangedEvent(argClearVREvent, argClearVRPlayer);
									break;
								default:
									UnityEngine.Debug.Log(String.Format("[ClearVR] Info message received. {0}", argClearVREvent.message.GetFullMessage()));
									break;
							}
						break;
					}
					break;
				default:
					break;
			}
			if(userInterface != null) {
				String eventAsString = argClearVREvent.type.ToString();
				if(eventAsString.IndexOf("StateChanged") == 0) {
					String osdStatusString = eventAsString.Replace("StateChanged", "");
					userInterface.UpdateOSDStatusText(osdStatusString);
				}
			}
		}

		/// <summary>
		/// This callback is triggered when one of the UI buttons is clicked.
		/// </summary>
		/// <param name="argPlayerActionRequestType">The requested player action (e.g. pause/resumt/switch audio track)</param>
		void CbPlayerInteractionRequest(ApplicationModel.PlayerInteractionRequests argPlayerActionRequestType) {
			long newPositionInMilliseconds;
			switch (argPlayerActionRequestType) {
				case ApplicationModel.PlayerInteractionRequests.None:
					break;// nothing to do
				case ApplicationModel.PlayerInteractionRequests.Play:
					// Resume playback if paused, load and play content if no content is loaded.
					if(clearVRPlayer != null) {
						// a player is active right now, try to pause it
						clearVRPlayer.controller.Unpause(new Action<ClearVREvent, ClearVRPlayer>(delegate(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer)  {
							if(argClearVREvent.message.GetIsSuccess()) { 
								UnityEngine.Debug.Log(String.Format("[ClearVR] Unpause: success. Optional message: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)));
							} else {
								if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, String.Format("unpausing. Optional message: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)))){
									argClearVRPlayer.controller.Stop();
								}
							}
						}), "Requested unpause.");
					} else {
						// no player active, we should create one.
						InitializeClearVRPlayer();
					}
					break;
				case ApplicationModel.PlayerInteractionRequests.Pause:
					// Pause playback
					if(clearVRPlayer != null) {
						// a player is active right now, try to pause it
						clearVRPlayer.controller.Pause(new Action<ClearVREvent, ClearVRPlayer>(delegate(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer)  {
							if(argClearVREvent.message.GetIsSuccess()) { 
								UnityEngine.Debug.Log(String.Format("[ClearVR] Pause: success. Optional message: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)));
							} else {
								if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, String.Format("pausing. Optional message: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)))){
									argClearVRPlayer.controller.Stop();
								}
							}
						}), "Requested pause.");
					}
					break;
				case ApplicationModel.PlayerInteractionRequests.Stop:
					// Stop playback
					if(clearVRPlayer != null) {
						clearVRPlayer.controller.Stop(stopButtonCallbackHandler, 
							"Requested to stop playback." /* Just a simple string */, 
							this /* by passing this, we can call methods on the parent in the callback */);
					}
					break;
				case ApplicationModel.PlayerInteractionRequests.SeekForward:
					if(clearVRPlayer != null) {
						// Seek forward by 20 seconds. Note that the Core library will properly bound the seek.
						newPositionInMilliseconds = clearVRPlayer.controller.GetCurrentContentTimeInMilliseconds() + 20 * 1000;
						clearVRPlayer.controller.Seek(newPositionInMilliseconds, new Action<ClearVREvent, ClearVRPlayer>(delegate(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer)  {
							if(argClearVREvent.message.GetIsSuccess()) { 
								UnityEngine.Debug.Log(String.Format("[ClearVR] Seek: success. Optional message: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)));
							} else {
								if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, String.Format("seeking. Optional message: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)))){
									argClearVRPlayer.controller.Stop();
								}
							}
						}), String.Format("Requested seek forward to {0} msec.", newPositionInMilliseconds));
					}
					break;
				case ApplicationModel.PlayerInteractionRequests.SeekBackward:
					if(clearVRPlayer != null) {
						// Seek backwards by 10 seconds. Note that the Core library will properly bound the seek.
						newPositionInMilliseconds = Math.Max(0, clearVRPlayer.controller.GetCurrentContentTimeInMilliseconds() - 10 * 1000);
						clearVRPlayer.controller.Seek(newPositionInMilliseconds, new Action<ClearVREvent, ClearVRPlayer>(delegate(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer)  {
							if(argClearVREvent.message.GetIsSuccess()) { 
								UnityEngine.Debug.Log(String.Format("[ClearVR] Seek: success. Optional message: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)));
							} else {
								if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, String.Format("seeking. Optional message: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)))){
									argClearVRPlayer.controller.Stop();
								}
							}
						}), String.Format("Requested seek backward to {0} msec.", newPositionInMilliseconds));
					}
					break;
				case ApplicationModel.PlayerInteractionRequests.NextClip:
				case ApplicationModel.PlayerInteractionRequests.PreviousClip:
					int currentContentItemIndex = contentItemIndex;
					if(argPlayerActionRequestType == ApplicationModel.PlayerInteractionRequests.NextClip)
						contentItemIndex++;
					else
						contentItemIndex--;
					if(contentItemIndex >= contentItemList.content_items.Length)
						contentItemIndex = 0;
					if(contentItemIndex < 0)
						contentItemIndex = contentItemList.content_items.Length - 1;
					if(contentItemIndex == currentContentItemIndex) {
						UnityEngine.Debug.Log(String.Format("[ClearVR] Content list only contains a single clip. Cannot switch content."));
						return;
					}
					if(clearVRPlayer != null) {
						ContentItem contentItem = contentItemList.content_items[contentItemIndex];
						/* Note how we set startYaw and startPitch according to the current camera orientation */
						contentItem.startYaw = userInterface.camera.transform.eulerAngles.y % 360;
						contentItem.startPitch = userInterface.camera.transform.eulerAngles.x;
						contentItem.startPositionInMilliseconds = clearVRPlayer.controller.GetCurrentContentTimeInMilliseconds();
						clearVRPlayer.controller.SwitchContent(contentItem, new Action<ClearVREvent, ClearVRPlayer>(delegate(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer) {
							if(argClearVREvent.message.GetIsSuccess()) {
								UnityEngine.Debug.Log(String.Format("[ClearVR] Content switched successfully. Event type: {0}. Optional arguments: {1}", argClearVRPlayer.mediaInfo.GetEventType(), argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)));
							} else {
								if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, String.Format("switching content. Optional arguments: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)))) {
									argClearVRPlayer.controller.Stop();
								}
							}
							if(userInterface != null) {
								userInterface.UpdateCurrentResolutionAndFramerateStringText(argClearVRPlayer.mediaInfo.GetCurrentResolutionAndFramerate());					
							}
						}), /*optional arguments */ String.Format("Requested to switch to content url: {0}", contentItem.manifestUrl));
					} else {
						UnityEngine.Debug.LogError(String.Format("[ClearVR] Content switching is only available when a player is already instantiated."));
					}
					break;
                case ApplicationModel.PlayerInteractionRequests.NextAudioTrack:
					if(clearVRPlayer != null) {
						audioTrackIndex++;
						if(audioTrackIndex >= clearVRPlayer.mediaInfo.GetNumberOfAudioTracks()) {
							audioTrackIndex = -1;
						}
						clearVRPlayer.controller.SetAudioTrack(audioTrackIndex, new Action<ClearVREvent, ClearVRPlayer>(delegate(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer) {
						}), "Requested to switch to next audio track.");
					}
                    break;
                case ApplicationModel.PlayerInteractionRequests.PreviousAudioTrack:
					if(clearVRPlayer != null) {
						audioTrackIndex--;
						if(audioTrackIndex < -1) {
							audioTrackIndex = clearVRPlayer.mediaInfo.GetNumberOfAudioTracks() - 1;
						}
						clearVRPlayer.controller.SetAudioTrack(audioTrackIndex, new Action<ClearVREvent, ClearVRPlayer>(delegate(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer) {
						}), "Requested to switch to previous audio track.");
					}
                    break;		
			}
		}

		/// <summary>
		/// This example demonstrates how to define a variable that implements a callback handler. In this case, the callback handler is used to parse the response on a Stop() request.
		/// Remember that you can pass any optional arguments when calling a function on the ClearVRPlayer. In this case, we rely on passing a simple string and the parent script object.
		/// </summary>
		/// <param name="argClearVREvent">The ClearVREvent to parse.</param>
		/// <param name="argClearVRPlayer">The ClearVRPlayer object that generated the event</param>
		Action<ClearVREvent, ClearVRPlayer> stopButtonCallbackHandler = new Action<ClearVREvent, ClearVRPlayer>(delegate(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer) {
			if(argClearVREvent.message.GetIsSuccess()) { 
				UnityEngine.Debug.Log(String.Format("[ClearVR] Stop: success. Optional message: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)));
			} else {
				if(Helpers.ParseWarningOrFatalErrorMessage(argClearVREvent, String.Format("stopping. Optional message: {0}", argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(0)))){
					// Nothing to be done. ClearVRPlayer is already shut-down anyway!
				}
			}
			((Demo02) argClearVREvent.clearVRAsyncRequestResponse.optionalArguments.GetValue(1)).ParseStateChangedStoppedEvent(argClearVREvent, argClearVRPlayer);
		});

		/// <summary>
		/// This method is used to destroy the ClearVRPlayer, remove it from the scene and clean-up any object that has become obsolete.
		/// </summary>
		/// <param name="argClearVREvent">The ClearVREvent to parse.</param>
		/// <param name="argClearVRPlayer">The ClearVRPlayer object that generated the event</param>
		void ParseStateChangedStoppedEvent(ClearVREvent argClearVREvent, ClearVRPlayer argClearVRPlayer) {
			userInterface.Reset();
			argClearVRPlayer.DestroyGameComponentAndCleanUp();
			Destroy(argClearVRPlayer);
			clearVRPlayer = null;
			mediaPlayerGameObject = null;
			isUpdateUICoroutineRunning = false;
			if(isApplicationShutdownRequested) {
				isApplicationShutdownRequested = false; // Reset to default value
				isApplicationShutdownScheduled = true;
			}
		}

		/// <summary>
		/// Called when leaving the scene. Note that the clearVRPlayer MUST be properly stopped by calling its Stop() method at all times to free its resources.
		/// </summary>
		void OnApplicationQuit() {
			UnityEngine.Debug.Log("[ClearVR] Quiting player");
			if(clearVRPlayer != null) {
				clearVRPlayer.controller.Stop();
			}
		}
	}
}
