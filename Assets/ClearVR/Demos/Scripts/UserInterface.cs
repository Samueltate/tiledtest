using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.XR;

/// <summary>
/// Simple class that handles the user interface and HUD layout.
/// </summary>
namespace com.tiledmedia.clearvr.demos {
    public class UserInterface  : MonoBehaviour{
        private Action<ApplicationModel.PlayerInteractionRequests> cbPlayerInteractionRequest; 
        private string      _libClearVRCoreStatusString = "Idle";
        private string      _averageBitrateInMegabitPerSecondString = "0 Mbps";
        private string      _currentContentTimeText = "";
        private string      _isMonoOrStereoString = "";
        private string      _highestQualityResolutionAndFramerate = "";
        private string      _audioTrackString = "";
        private bool        _isUIDirty = true;
        private DateTime    _lastFlatTouchTime = DateTime.MinValue;
        private TextMesh    _txtStatusTextMesh;
        private float       _touchDuration;
        private Touch       _touch;
        private GameObject  _goTxtStatus;
        // Our main/center camera object
        #pragma warning disable 0108
        public Camera       camera = null;
        #pragma warning restore 0108

        // Public variable set to the camera used in the flat app
        public Camera       flatCamera;
        // Public variable set to the OVRCameraRig gameobject used in the GearVR app.
        public GameObject    ovrCameraRig;
        
        class UIButton {
            public GameObject gameObject;
            public Vector2 worldSize;
            public Vector2 screenSize;
            public Vector3 pixelSize;
            public Vector3 scaledPixelSize;
            
            public UIButton(GameObject argGameObject){
                gameObject = argGameObject;
            }

            public void GetDimensions(Transform argTransform){
                ApplicationModel.GetWorldScreenAndPixelSizeOfSprite(gameObject, argTransform, out worldSize, out screenSize, out pixelSize, out scaledPixelSize);
            }
        }
        /// <summary>
        /// Setup user interface. Fix correct camera rig depending on whether we are running on a GearVR or not.
        /// </summary>
        private void Awake(){
            Reset();
            // Lets set-up the proper camera depending on whether a GearVR is present or not.
            switch(Utils.GetDeviceType()) {
                case DeviceTypes.GearVR:
                case DeviceTypes.OculusGo:
                case DeviceTypes.Quest:
                    if(flatCamera != null) {
                        flatCamera.gameObject.SetActive(false);
                        DestroyImmediate(flatCamera.gameObject);
                    }
                    ovrCameraRig.SetActive(true);
                    camera = GameObject.Find("CenterEyeAnchor").GetComponent<Camera>() as Camera;
                    GameObject.Find("Canvas").GetComponent<Canvas>().worldCamera = camera;
                    break;
                case DeviceTypes.Cardboard:
                case DeviceTypes.Daydream:
                case DeviceTypes.Flat:
                    flatCamera.gameObject.SetActive(true);
                    if(ovrCameraRig != null) {
                        ovrCameraRig.SetActive(false);
                        DestroyImmediate(ovrCameraRig);
                    }
                    camera = flatCamera;
                    GameObject.Find("Canvas").GetComponent<Canvas>().worldCamera = camera;
                    break;
                default:
                    throw new Exception(String.Format("Headset mode {0} not supported!", Utils.GetDeviceType()));
            }
            // Position UI
            String[] commandButtonsName = {"cmdPause", "cmdPlay", "cmdStop", "cmdSeekBackward", "cmdSeekForward", "cmdPreviousAudioTrack", "cmdNextAudioTrack", "cmdPreviousClip", "cmdNextClip"};
            List<UIButton> uiButtons = new List<UIButton>(commandButtonsName.Length);
            float scaleFactor = 10f;
            for(int i = 0; i < commandButtonsName.Length; i++) {
                uiButtons.Add(new UIButton(GameObject.Find(commandButtonsName[i])));
                if(uiButtons[i].gameObject == null){
                    throw new Exception(String.Format("User interface broken, cannot find button gameobject with name '{0}'", commandButtonsName[i]));
                }
                uiButtons[i].gameObject.transform.localScale = new Vector3(scaleFactor, scaleFactor, 1.0f);
                uiButtons[i].GetDimensions(transform);

            }
            _goTxtStatus = GameObject.Find ("txtStatus");
            _txtStatusTextMesh = _goTxtStatus.GetComponent<TextMesh>();
            // We only show UI buttons in the flat app.
            if(!Utils.GetIsVrDevicePresent()){
                // Crude method to move gamobjects to the right spot on the screen. Will look weird on different kinds of screens/aspect ratios 

                _goTxtStatus.transform.localScale = new Vector3(0.5f, 0.5f, 1f);

                Vector3 parentCanvasLocalScale = GameObject.Find("Canvas").transform.localScale;
                float marginX = 5f / parentCanvasLocalScale.x;
                float marginY = 20f * parentCanvasLocalScale.y + 10;
                float xOffsetSum = 0;
                for(int i = 0; i < commandButtonsName.Length; i++) {
                    uiButtons[i].gameObject.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(marginX * (i) + (xOffsetSum * parentCanvasLocalScale.x), Camera.main.pixelHeight - marginY , 7f));
                    xOffsetSum = xOffsetSum + uiButtons[i].scaledPixelSize.x;
                    Destroy(uiButtons[i].gameObject.GetComponent<BoxCollider>());
                    uiButtons[i].gameObject.AddComponent<BoxCollider>();
                }
                _goTxtStatus.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight - (marginY * 2) - (uiButtons[0].scaledPixelSize.y * 1.5f * parentCanvasLocalScale.y), 7f));
            } else {
                // We do not show any buttons when running in the GearVR, only the status text
                for(int i = 0; i < commandButtonsName.Length; i++) {
                    Destroy(uiButtons[i].gameObject);
                }
                _goTxtStatus.transform.localScale = new Vector3(1f, 1f, 1.0f);
                _goTxtStatus.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(-500, Camera.main.pixelHeight + 500, 12f));
            }
            _currentContentTimeText = Helpers.GetTimeInMillisecondsAsPrettyString(0);
        }

        /// <summary>
        /// Called from Main.cs to set an Action trigger for when a button is clicked in the UI
        /// </summary>
        /// <param name="argCbPlayerInteractionRequest">The callback triggered by user interface interation.</param>
        public void SetCbPlayerInteractionRequest(Action<ApplicationModel.PlayerInteractionRequests> argCbPlayerInteractionRequest){
            cbPlayerInteractionRequest = argCbPlayerInteractionRequest;
        }

        /// <summary>
        /// Check for user input, only when not running in GearVR mode.
        /// </summary>
        void Update(){
            if(Utils.GetIsVrDevicePresent())
                return;
            #if UNITY_EDITOR
                // Trigger on mouse clicks
                if (Input.GetMouseButtonDown (0)) {
                    if(DateTime.Now.Subtract(_lastFlatTouchTime).TotalMilliseconds < 100)
                        return;
                    _lastFlatTouchTime = DateTime.Now;
                    Ray cursorRay = Camera.main.ScreenPointToRay( new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f) );
                    RaycastHit hit;
                    if (Physics.Raycast (cursorRay, out hit, Mathf.Infinity)){
                        CheckIfButtonWasClicked(hit);
                    }
                }
            #endif
            #if !UNITY_EDITOR || UNITY_ANDROID
                // Trigger on screen taps
                if (Input.touchCount == 1) {
                    _touchDuration += Time.deltaTime;
                    _touch = Input.GetTouch(0);
                    if(_touch.phase == TouchPhase.Ended && _touchDuration < 0.2f) //making sure it only check the touch once && it was a short touch/tap and not a dragging.
                        StartCoroutine("SingleOrDouble");
                } else {
                    _touchDuration = 0.0f;
                }
            #endif

        }
        /// <summary>
        /// Helper method to determine whether it was a single or double click.
        /// </summary>
        IEnumerator SingleOrDouble(){
            yield return new WaitForSeconds(0.3f);
            if(_touch.tapCount == 1){
                // Single click
                Ray cursorRay = Camera.main.ScreenPointToRay(_touch.position );
                RaycastHit hit;
                if (Physics.Raycast (cursorRay, out hit, Mathf.Infinity)){
                    CheckIfButtonWasClicked(hit);
                }
            }
        }

        /// <summary>
        /// Parse raycast hit and determine whether a button was clicked.
        /// </summary>
        /// <param name="argHit">The raycast hit</param>
        void CheckIfButtonWasClicked(RaycastHit argHit){
            ApplicationModel.PlayerInteractionRequests playerInteractionRequest = ApplicationModel.PlayerInteractionRequests.None;
            switch (argHit.transform.gameObject.name){
                case "cmdPause":
                    playerInteractionRequest = ApplicationModel.PlayerInteractionRequests.Pause;
                    break;
                case "cmdPlay":
                    playerInteractionRequest = ApplicationModel.PlayerInteractionRequests.Play;
                    break;
                case "cmdStop":
                    playerInteractionRequest = ApplicationModel.PlayerInteractionRequests.Stop;
                    break;
                case "cmdSeekBackward":
                    playerInteractionRequest = ApplicationModel.PlayerInteractionRequests.SeekBackward;
                    break;
                case "cmdSeekForward":
                    playerInteractionRequest = ApplicationModel.PlayerInteractionRequests.SeekForward;
                    break;
                case "cmdNextAudioTrack":
                    playerInteractionRequest = ApplicationModel.PlayerInteractionRequests.NextAudioTrack;
                    break;
                case "cmdPreviousAudioTrack":
                    playerInteractionRequest = ApplicationModel.PlayerInteractionRequests.PreviousAudioTrack;
                    break;
                case "cmdNextClip":
                    playerInteractionRequest = ApplicationModel.PlayerInteractionRequests.NextClip;
                    break;
                case "cmdPreviousClip":
                    playerInteractionRequest = ApplicationModel.PlayerInteractionRequests.PreviousClip;
                    break;
            }
            if(cbPlayerInteractionRequest != null)
                cbPlayerInteractionRequest(playerInteractionRequest);        
        }
        
        /// <summary>
        /// Update ClearVRCore status text.
        /// </summary>
        /// <param name="argText">The new text</param>
        public void UpdateOSDStatusText(string argText){
            _libClearVRCoreStatusString = argText;
            SetUIIsDirty();
        }

        /// <summary>
        /// Update average bitrate text.
        /// </summary>
        /// <param name="argText">The new text</param>
        public void UpdateAverageBitrateText(string argText){
            _averageBitrateInMegabitPerSecondString = argText;
            SetUIIsDirty();
        }

        /// <summary>
        /// Update current content time.
        /// </summary>
        /// <param name="argText">The new text</param>
        public void UpdateCurrentContentTimeText(string argText){
            _currentContentTimeText = argText;
            SetUIIsDirty();
        }

        /// <summary>
        /// Update mono/stereo indicator text
        /// </summary>
        /// <param name="argText">The new text</param>
        public void UpdateIsMonoOrStereoText(string argText){
            RenderTextureDescriptor renderTextureDescriptor = XRSettings.eyeTextureDesc;
            String renderingMode = (renderTextureDescriptor.vrUsage == VRTextureUsage.TwoEyes) ? "SPSR" : "MPSR";
            _isMonoOrStereoString = String.Format("{0}, {1}", argText, renderingMode);
            SetUIIsDirty();
        }

        /// <summary>
        /// Update effective content resolution indicator text
        /// </summary>
        /// <param name="argText">The new text</param>
        public void UpdateCurrentResolutionAndFramerateStringText(string argText){
            _highestQualityResolutionAndFramerate = argText;
            SetUIIsDirty();
        }

        /// <summary>
        /// Update the audio track indicator text
        /// </summary>
        /// <param name="argText">The new text</param>
        public void UpdateAudioTrackStringText(string argText) {
            _audioTrackString = argText;
        }

        /// <summary>
        /// Signals that the UI is dirty and needs to be updated at the next cycle.
        /// </summary>
        private void SetUIIsDirty(){
            _isUIDirty = true;
        }

        /// <summary>
        /// Refresh the text in HUD in the top left corner.
        /// </summary>
        public void RefreshStatusTextLabel(){
            if(_goTxtStatus != null && _isUIDirty) {
                _txtStatusTextMesh.text = String.Format("{0} {1}\n{2} {3}\n{4}\n{5}", 
                    _libClearVRCoreStatusString, 
                    _highestQualityResolutionAndFramerate, 
                    _averageBitrateInMegabitPerSecondString, 
                    _isMonoOrStereoString == "" ? "" : String.Format("({0})", _isMonoOrStereoString),
                    _currentContentTimeText, 
                    _audioTrackString);
            }
            _isUIDirty = false;
        }

        /// <summary>
        /// Reset text in HUD to default values
        /// </summary>
        public void Reset(){
            _libClearVRCoreStatusString = "Idle";
            _averageBitrateInMegabitPerSecondString = "0 Mbps";
            _highestQualityResolutionAndFramerate = "";
            _isMonoOrStereoString = "";
            _currentContentTimeText = "";
            _audioTrackString = "";
            SetUIIsDirty();
        }
    }
}