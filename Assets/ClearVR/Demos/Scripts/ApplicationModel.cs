using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

namespace com.tiledmedia.clearvr.demos {
    public class ApplicationModel {
        
        /* Player interaction enum.
        Note that not all actions are implemented in this example
        */
        public enum PlayerInteractionRequests {
            None,
            Pause,
            Play,
            Stop,
            SeekForward,
            SeekBackward,
            NextClip,
            PreviousClip,
            NextAudioTrack,
            PreviousAudioTrack
        }
        
        // Source: http://answers.unity3d.com/questions/1042119/getting-a-sprites-size-in-pixels.html
        public static void GetWorldScreenAndPixelSizeOfSprite(GameObject argGameObject, Transform argTransform, out Vector2 worldSize, out Vector2 screenSize, out Vector3 pixelSize, out Vector3 scaledPixelSize){
            //get world space size (this version handles rotating correctly)
            Vector2 spriteSize = argGameObject.GetComponent<SpriteRenderer>().sprite.rect.size;
            Vector2 localSpriteSize = spriteSize / argGameObject.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
            worldSize = localSpriteSize;
            worldSize.x *= argTransform.lossyScale.x;
            worldSize.y *= argTransform.lossyScale.y;

            //convert to screen space size
            screenSize = 0.5f * worldSize / Camera.main.orthographicSize;
            screenSize.y *= Camera.main.aspect;

            //size in pixels
            pixelSize = new Vector3(screenSize.x * Camera.main.pixelWidth, screenSize.y * Camera.main.pixelHeight, 0) * 0.5f;
            scaledPixelSize = new Vector3(pixelSize.x * argGameObject.transform.localScale.x, pixelSize.y * argGameObject.transform.localScale.y, pixelSize.z * argGameObject.transform.localScale.z);
        }

        /*
        Simple helper method that capitalizes the first letter of a string.
        */
        public static string FirstLetterToUpper(string argInputString) {
            if (argInputString == null)
                return null;
            if (argInputString.Length > 1)
                return char.ToUpper(argInputString[0]) + argInputString.Substring(1);
            return argInputString.ToUpper();
        }
    }
}