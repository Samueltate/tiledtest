using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.tiledmedia.clearvr {
    /// <summary>
    /// This internal interface is not exposed as a public API, but is rather used to enforce a common API across platform specific implementation of the MediaPlayer class. 
    /// Do not use this interface.
    /// </summary>
    public interface InternalInterface {
        /// <summary>
        /// Prepare core libraries.
        /// </summary>
        ClearVRAsyncRequest PrepareCore();
        /// <summary>
        /// Called every Update() cycle
        /// </summary>
		void Update();
        /// <summary>
        /// Called every LateUpdate() cycle
        /// </summary>
        void LateUpdate();
        /// <summary>
        /// Called at the end of a frame (triggered by WaitForEndOfFrame)
        /// </summary>
        void EndOfFrame();
        /// <summary>
        /// Convenience method called every Update() cycle.
        /// </summary>
		void Render();
        /// <summary>
        /// Called upon destruction of the MediaPlayer object.
        /// </summary>
        void Destroy();
        /// <summary>
        /// Called after state stopped is reached.
        /// </summary>
        void CleanUpAfterStopped();
        /// <summary>
        /// Called when a (new) ClearVRMesh (e.g. a Cubemap sphere) needs to be constructed.
        /// </summary>
        /// <param name="argMeshCreatedStruct"></param>
        ClearVRMeshBase ConstructClearVRMesh(MeshCreatedStruct argMeshCreatedStruct);
        /// <summary>
        /// Called when the ContentFormat needs to be updated, e.g. because there was switch from monoscopic to stereoscopic content.
        /// </summary>
        void UpdateContentFormat();
        /* Mandatory callbacks */
		void CbClearVRCoreWrapperMessage(ClearVRMessage argClearVRMessage);
		void CbClearVRCoreWrapperRequestCompleted(ClearVRAsyncRequestResponse argClearVRAsyncRequestResponse, ClearVRMessage argClearVRMessage);
        void SendSensorInfo();
        List<ClearVRAsyncRequest> clearVRAsyncRequests {get;}
        List<ClearVRAsyncRequest> clearVRAsyncRequestsWithoutCallback {get;}
		void ScheduleClearVRAsyncRequest(ClearVRAsyncRequest argClearVRAsyncRequest, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        ClearVRAsyncRequest ScheduleClearVRAsyncRequestWithoutCallback(ClearVRAsyncRequest argClearVRAsyncRequest);
        ClearVRAsyncRequest _StartPlayout(bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _PrepareCore(bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _Pause(bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _Unpause(bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _TogglePause(bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _Seek(long argNewPositionInMilliseconds, bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _SetStereoMode(bool argStereo, bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _PrewarmCache(String argManifestUrl, int argInitialPositionInMilliseconds, long argFlags, bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _SetAudioTrack(int argIndex, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _SwitchContent(ContentItem argContentItem, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _PrepareContentForPlayout(ContentItem argContentItem, bool argIsCalledAsynchronous);
        ClearVRAsyncRequest _Stop(bool argStoppedAfterApplicationLostFocus, bool argIsCalledAsynchronous);
    }

    /// <summary>
    /// The MediaPlayer interface specifies interactions with the MediaPlayer object.
    /// </summary>
	public interface MediaPlayerInterface {
        /// <summary>
        /// The ClearVRCore exposes a large number of parameters which can be queried when the player is in a certain state. Please refer to the ClearVRCore documentation for an exhaustive list of all parameters.
        /// Notes.
        /// 1. Some common day-to-day-use parameters are exposed through convenience methods.
        /// 2. Note that many parameters can only be queried at a specific moment in time.
        /// 3. Some parameters can only be queried with an array index. See GetClearVRCoreArrayParameter() and the ClearVRCore documentation for details.
        /// </summary>
        /// <param name="argKey">The key to query.</param>
        /// <returns>The value of the queried key, or an empty string if it could not be queried.</returns>
        String GetClearVRCoreParameter(String argKey);
        /// <summary>
        /// Since v4.3
        /// 
        /// The ClearVRCore exposes a large number of parameters which can be queried when the player is in a certain state. Please refer to the ClearVRCore documentation for an exhaustive list of all parameters.
        /// This method can be used to query parameters that take a mandatory array index.
        /// 
        /// Notes.
        /// 1. Some common day-to-day-use parameters are exposed through convenience methods.
        /// 2. Note that many parameters can only be queried at a specific moment in time.
        /// 3. Most parameters do not take an array index. See GetClearVRCoreParameter() and the ClearVRCore documentation for details.
        /// 4. One needs to make sure that your query is properly bounded.
        /// </summary>
        /// <param name="argKey">The key to query.</param>
        /// <param name="argIndex">The index in the array to query.</param>
        /// <returns>The value of the queried key, or an empty string if it could not be queried.</returns>
        String GetClearVRCoreArrayParameter(String argKey, int argIndex);
        /// <summary>
        /// Set a specific ClearVRCore parameter. See also GetClearVRCoreParameter() for more details.
        /// </summary>
        /// <param name="argKey">The key to set.</param>
        /// <param name="argValue">The value to set the key to.</param>
        /// <returns></returns>
        bool SetClearVRCoreParameter(String argKey, String argValue);
        /// <summary>
        /// Parse media info on a specific ContentItem. Note that parsing media info does not load the content item and does not prepare it for immediate playout.
        /// For that, one needs to Initialize() or SwitchContent() instead.
        /// This is an advanced API and should typically not be used.
        /// Callback event: MediaInfoParsed
        /// </summary>
        /// <param name="argUrl">The Url of which to parse media info.</param>
        ClearVRAsyncRequest ParseMediaInfo(String argUrl);
        void ParseMediaInfo(String argUrl, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /// <summary>
        /// Prepares a ContentItem for playout. This is an asynchronous call and one must wait for the StateChangedContentPreparedForPlayout callback to check whether it was successful or not.
        /// Callback event: StateChangedContentPreparedForPlayout
        /// </summary>
        /// <param name="argContentItem">The ContentItem to load.</param>
        ClearVRAsyncRequest PrepareContentForPlayout(ContentItem argContentItem);
        void PrepareContentForPlayout(ContentItem argContentItem, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /// <summary>
        /// This API is used to switch between monoscopic and stereoscopic rendering (if the content and player allows for it). Note that this only changes how the content is *rendered*, NOT how it is retrieved from the network. If you want to switch between monoscopic or stereoscopic content retrieval, you should use the SetStereoMode() API instead.
        /// It is especially suited for temporarily disabling stereoscopic rendering when you show an in-player menu (to prevent depth-fighting from happening) as this API is instantaneous while the SetStereoMode() API will take a bit of time to respond, reducing user experience.
        /// Callback event: RenderModeChanged
        /// </summary>
        /// <param name="argNewRenderMode">The new render mode to switch to.</param>
        void SetRenderMode(RenderModes argNewRenderMode);
        /// <summary>
        /// Gets the currently active RenderMode.
        /// </summary>
        /// <returns>The active RenderMode.</returns>
        RenderModes GetRenderMode();
        /// <summary>
        /// Recenters the sphere based on the current camera orientation.
        /// </summary>
        void Recenter();
        /// <summary>
        /// Rotates the sphere around the specified axis. Please note that only rotation across the y-axis (e.g. yaw) is supported. Rotating the sphere around the x or z axis (pitch and roll respectively) is unsupported. System behaviour is in that case undefined.
        /// Note that rotating the sphere is considered an "expensive" operation and should be used with care. At no instant one should call this API every frame.
        /// </summary>
        /// <param name="argNewEulerAngles">The new orientation.</param>
        void Rotate(Vector3 argNewEulerAngles);
        /// <summary>
        /// Translates the sphere into the specified direction. This API follows the Tranform.Translate() Unity API as defined in https://docs.unity3d.com/ScriptReference/Transform.Translate.html
        /// </summary>
        /// <param name="argTranslation">The translation to perform.</param>
        /// <param name="argRelativeTo">The relative space to which the translation should happen.</param>
        void Translate(Vector3 argTranslation, Space argRelativeTo = Space.Self);
        /// <summary>
        /// Translates the sphere into the specified direction. This API follows the Tranform.Translate() Unity API as defined in https://docs.unity3d.com/ScriptReference/Transform.Translate.html
        /// </summary>
        /// <param name="argTranslation">The translation to perform.</param>
        /// <param name="argRelativeTo">The transform to which the translation should happen.</param>
        void Translate(Vector3 argTranslation, Transform argRelativeTo);
        /// <summary>
        /// Returns the currently active platform options. Note that you can *only* set these prior to initialization. Changing any of its parameters after initialization results in undefined baheviour and must be avoided.
        /// </summary>
        /// <returns>The currently active PlatformOptions. Cast to the platform-specific PlatformOptions to access any platform-specific fields.</returns>
        PlatformOptionsBase GetPlatformOptions();
        /// <summary>
        /// Change the main color of the video texture. This can also be used to make the sphere transparent by setting the alpha component accordingly.
        /// For common performance reasons not specifically related to ClearVR streaming, you should carefully assess the potential negative performance impact of using transparency on mobile devices.
        /// </summary>
        /// <param name="argNewColor">The new color.</param>
        /// <returns>True if successful, false otherwise.</returns>
        bool SetMainColor(Color argNewColor);
        /// <summary>
        /// The SetStereoMode() API can be used to switch between monoscopic and stereoscopic playback if the content supports it. Note that this actually toggles the retrieval of the right-eye tiles, 
        /// so enabling it might take a bit of time and might result in buffering depending on network conditions. If one would only temporarily switch between monoscopic and stereoscopic rendering (e.g. because an in-player menu pops-up and you want to make sure there is no depth-fighting between UI and video), one is advised to use the SetRenderMode() API instead as that only changes how the video rendered, not how it is retrieved from the network.
        /// Callback event: StereoModeChanged. Optionally, one will receive the RenderModeChanged event if a switch in rendering was required (e.g. when you went from stereoscopic to monoscopic).        
        /// </summary>
        /// <param name="argStereo">true if you want to switch to stereoscopic rendering, false if monoscopic rendering is requested.</param>
        ClearVRAsyncRequest SetStereoMode(bool argStereo);
        void SetStereoMode(bool argStereo, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /// <summary>
        /// The PrewarmCache API allows one to preload initialization data for a specific clip prior to actually using it.
        /// Note that the cache is linked to the ClearVRPlayer instance. If you destroy the ClearVRPlayer, the cache will be flushed as well.
        /// This is typically used together with the SwitchContent() API.
        /// 
        /// As each cache warmup will require up to 1 or 2 megabyte of data (varying per content item), please be considerate when to actually call this API as
        /// downloading extra data might interfere with regular playback on limited internet connections. For example, paused state might be a good
        /// moment to prewarm the cache on an item or two.
        /// 
        /// Warning: please do NOT hammer this API (e.g. by looping over your entire content list as fast as possible).
        /// 
        /// Callback event: PrewarmCacheCompleted
        /// </summary>
        /// <param name="argManifestUrl">The manifest file to cache.</param>
        /// <param name="argInitialPositionInMilliseconds">The initial position in the stream</param>
        /// <param name="argFlags">Binary OR-ed PrewarmCacheFlags.</param>
        ClearVRAsyncRequest PrewarmCache(String argManifestUrl, int argInitialPositionInMilliseconds, long argFlags);
        void PrewarmCache(String argManifestUrl, int argInitialPositionInMilliseconds, long argFlags, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /* Determine current player state */
        /// <summary>
        /// Advanced API to determine whether media info was parsed or not. 
        /// </summary>
        /// <returns>True if media info was parsed, false otherwise.</returns>
        bool GetIsMediaInfoParsed();
        /// <summary>
        /// Whether the media player was initialized or not. Note that "initialized" does not neccesarily mean that it is already playing content.
        /// </summary>
        /// <returns>True if the underlying media player is at least initialized, false otherwise.</returns>
        bool GetIsInitialized();
        /// <summary>
        /// Whether the player is busy playing a content item.
        /// </summary>
        /// <returns>True if busy, false otherwise.</returns>
        bool GetIsPlayerBusy();
        /// <summary>
        /// Convenience method to check whether the player is in playing state.
        /// </summary>
        /// <returns>True if in playing state, false if in any other state.</returns>
        bool GetIsInPlayingState();
        /// <summary>
        /// Convenience method to check whether the player is in pausing/paused state.
        /// </summary>
        /// <returns>True if in pausing or paused state, false otherwise.</returns>
        bool GetIsInPausingOrPausedState();

        /// <summary>
        /// Convenience method to check whether the player is in paused state.
        /// </summary>
        /// <returns>True if in paused state, false otherwise.</returns>
        bool GetIsInPausedState();
        /// <summary>
        /// Since v4.1.2
        /// Returns the layout of the fallback tiles.
        /// 
        /// Notes.
        /// 1. You should only call this method after you have received the FirstFrameRendered event.
        /// 2. Remember that the layout of the fallback can change when switching between clips. If it changed, you will receive a new FirstFrameRendered event and you should requery the FallbackLayout.
        /// 3. Currently, only the layout of the fallback tiles of the *left* eye are signalled.
        /// 4. The layout of the left eye's fallback tiles is fixed, it will not change during runtime
        /// </summary>
        /// <returns>The layout of the fallback tiles.</returns>
        FallbackLayout GetFallbackLayout();
        /// <summary>
        /// Since v4.1.2
        /// Returns the Texture2D object of the currently active texture. Note that the tiles in this texture are shuffled.
        /// 
        /// Notes.
        /// 1. You should only call this method after you have received the FirstFrameRendered event.
        /// 2. This texture might change as a result of switchContent() and/or ABR events.
        /// 3. This texture will NOT change when switching from monoscopic to stereoscopic rendering or vice versa.
        /// </summary>
        /// <returns>The currently active video texture as a properly bounded Texture2D.</returns>
        Texture2D GetTexture();
        /// <summary>
        /// Since v4.1.2
        /// Enable or disable viewport tracking. This is an advanced API that allows you to disable viewport tracking, effectively fixating the high quality viewport regardless of camera orientation.
        /// By default, viewport tracking is enabled.
        /// 
        /// Notes.
        /// 1. This is an advanced API, typicaly only used in exceptional cases.
        /// </summary>
        /// <param name="argIsEnabledOrDisabled">True to enable tracking, false otherwise.</param>
        void EnableOrDisableViewportTracking(bool argIsEnabledOrDisabled);
        /// <summary>
        /// Convenience method to check whether the player is in Finished state. The player will only reach Finished state in case it reached the end of the clip AND content looping is disabled.
        /// Notes.
        /// 1. When the player reaches this state, the viewport can no longer be updated and you cannot switch to Pause state. You can call Seek() though and playback will resume immediately if you would do so.
        ///
        /// Since: v4.1.2
        /// </summary>
        /// <returns>True if in Finished state, false otherwise.</returns>
        bool GetIsInFinishedState();
        /// <summary>
        /// Since: v4.1.2
        /// 
        /// Hide or unhide the GameObject rendering the video.
        /// Notes:
        /// 1. This is persistent between content switches. 
        /// </summary>
        void HideOrUnhide(bool argHideOrUnhide);
        /// <summary>
        /// Convenience method to check whether the player is in Stopped state. When in Stopped state, the player can no longer be used and should be destroyed.
        /// Notes.
        /// 1. Before the Stopped state, the mediaPlayer will first transition through the Stopping state. This is a transient state and one MUST wait for it to complete before destroying the mediaPlayer. Failing to do so will result in unexpected behaviour and resources might leak.
        ///
        /// Since: v4.1.2
        /// </summary>
        /// <returns>True if in Stopped state, false otherwise.</returns>
        bool GetIsInStoppedState();
        /// <summary>
        /// Returns the currently active ClearVR gameobject or null if none is active. Note that this *might* change when using the SwitchContent() API. Use the ClearVREventTypes.FirstFrameRendered event as a notification when a new mesh was created.
        /// 
        /// Notes.
        /// Make absolutely sure to set the returned gameobject to null once you're done with it to make sure that it can get garbage collected.
        /// </summary>
        /// <returns>The currently active GameObject, or null if not available.</returns>
        GameObject GetActiveClearVRMesh();
        /// <summary>
        /// Returns the unique device app id as a string. The device app is defined as a MD5-hashed combination of a unique user id and your application's id.
        /// It will return an empty string if queried in an invalid state, or if the device app id could not be generated.
        /// Notes.
        /// 1. One is recommended to query the device app id when one receives the ClearVREventTypes.StateChangedCorePrepared event. 
        /// </summary>
        /// <returns>The unique id as a string or an empty string if unavailable.</returns>
        String GetDeviceAppId();
	}

    /// <summary>
    /// The controller interface is used to control media playback. APIs like Pause(), Unpause() and Seek() are all part of this interface.
    /// </summary>
    public interface MediaControllerInterface {

        /// <summary>
        /// After initialization and content preparation, use this method to actually commence playback. Typically, playback should start more or less immediately after calling this method as video is supposed to be cached by then.
        /// Notes:
        /// 1. during the lifecycle of a media player object one can only call this method once. To unpause playback, use Unpause() instead.
        /// Callback event: StateChangedPlaying
        /// </summary>
        ClearVRAsyncRequest StartPlayout();
        void StartPlayout(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /// <summary>
        /// Pause playback.
        /// Notes:
        /// 1. due to the nature of spatio-temporal video streaming, pausing is only possible on a GOP boundary and might thus take a short while.
        /// Callback event: StateChangedPausing --> StateChangedPaused
        /// </summary>
        ClearVRAsyncRequest Pause();
        void Pause(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /// <summary>
        /// Unpause/resume playback.
        /// Callback event: StateChangedPlaying
        /// </summary>
        ClearVRAsyncRequest Unpause();
        void Unpause(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /// <summary>
        /// Convenience method to easily toggle between pause and unpause. See Pause() and Unpause() for details.
        /// </summary>
        ClearVRAsyncRequest TogglePause();
        /// <summary>
        /// Advanced API. Typically the ClearVRPlayer.Stop() API is used instead. Use with care.
        /// </summary>
        /// <param name="argStoppedAfterApplicationLostFocus">True if playback was stopped after application lost focus, false otherwise.</param>
        void TogglePause(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        ClearVRAsyncRequest Stop();
        void Stop(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        ClearVRAsyncRequest Stop(bool argStoppedAfterApplicationLostFocus);
        void Stop(bool argStoppedAfterApplicationLostFocus, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /// <summary>
        /// Enable to disable content looping. Note that you can only call this API after the player has initialized.
        /// </summary>
        /// <param name="argIsContentLoopEnabled"></param>
        /// <returns></returns>
        bool SetLoopContent(bool argIsContentLoopEnabled);
        /// <summary>
        /// Seek to a new position in the content. The new position should be provided in milliseconds and will be bound between 0 and the content duration.
        /// Notes:
        /// 1. Due to the nature of spatio-temporal streaming, seek will jump to the closest I-frame.
        /// 2. This method can only be called in Running/Pausing/Paused/Buffering state
        /// Callback event: StateChangedSeeking, StateChangedPlaying/StateChangedPaused.
        /// </summary>
        /// <param name="argNewPositionInMilliseconds">The new position in the content, in milliseconds.</param>
        ClearVRAsyncRequest Seek(long argNewPositionInMilliseconds);
        void Seek(long argNewPositionInMilliseconds, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /// <summary>
        /// Get the current content position in milliseconds.
        /// </summary>
        /// <returns>The current position in the content, in milliseconds.</returns>
        long GetCurrentContentTimeInMilliseconds();
        /// <summary>
        /// This API is used to switch to a new content item.
        /// Notes:
        /// 1. This call is asynchronous.
        /// Callback event: ContentSwitched
        /// </summary>
        /// <param name="argContentItem">The new ContentItem to switch to.</param>
        /// <returns>True if a content switch was successfully scheduled, false otherwise.</returns>
        ClearVRAsyncRequest SwitchContent(ContentItem argContentItem);
        void SwitchContent(ContentItem argContentItem, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);

        /// <summary>
        /// This API is used to switch to a new content item. Note that this is an advanced API allowing you to specify audio decoder and audio playback engine. Typically, one would use the simplified API.
        /// Notes:
        /// 1. This call is asynchronous.
        /// Callback event: ContentSwitched
        /// </summary>
        /// <param name="argContentItem">The new ContentItem to switch to.</param>
        /// <param name="argAudioDecoder">The audio decoder to use.</param>
        /// <param name="argAudioPlaybackEngine">The audio playback engine to use.</param>
        /// <returns>True if a content switch was successfully scheduled, false otherwise.</returns>
        ClearVRAsyncRequest SwitchContent(ContentItem argContentItem, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine);
        void SwitchContent(ContentItem argContentItem, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);

        /* Audio control */
        /// <summary>
        /// Mute or unmute audio playback. Check the specification of the return type in the notes below!
        /// 
        /// Notes.
        /// 1. this method returns false if it successfully UNMUTED the audio OR failed to execute the mute/unmute request.
        /// </summary>
        /// <param name="argIsMuted">True if audio should be muted, false is audio should be unmuted.</param>
        /// <returns>True if audio is muted, false if audio is unmuted OR if the call failed.</returns>
        bool SetMuteAudio(bool argIsMuted);
        /// <summary>
        /// Change audio gain. Expected scale: [0, 10]
        /// </summary>
        /// <param name="argGain">The new gain to set.</param>
		void SetAudioGain(float argGain);
        /// <summary>
        /// Retrieve current audio gain.
        /// Notes:
        /// Returns 0 if audio gain could not be queried.
        /// </summary>
        /// <returns>The current audio gain.</returns>
		float GetAudioGain();
        /// <summary>
        /// Check whether audio is muted or not.
        /// </summary>
        /// <returns>True is audio is muted, false otherwise.</returns>
		bool GetIsAudioMuted();
        /// <summary>
        /// Sets the active audio track index. Use -1 to select no audio track (effectively disabling audio playback). 
        /// Note that this call is asynchronous and one must wait for the AudioTrackSwitched event before the switch is done.
        /// Callback event: AudioTrackSwitched
        /// </summary>
        /// <param name="argIndex">The audio track index to set.</param>
        /// <returns>true in case of success, false otherwise.</returns>
        ClearVRAsyncRequest SetAudioTrack(int argIndex);
        void SetAudioTrack(int argIndex, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /// <summary>
        /// Sets the active audio track index. Use -1 to select no audio track (effectively disabling audio playback). 
        /// Note that this call is asynchronous and one must wait for the AudioTrackSwitched event before the switch is done.
        /// Callback event: AudioTrackSwitched
        /// </summary>
        /// <param name="argIndex">The audio track index to set.</param>
        /// <param name="argAudioDecoder">The audio decoder to use.</param>
        /// <param name="argAudioPlaybackEngine">The audio playback engine to use.</param>
        /// <returns>true in case of success, false otherwise.</returns>
        ClearVRAsyncRequest SetAudioTrack(int argIndex, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine);
        void SetAudioTrack(int argIndex, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments);
        /// <summary>
        /// Get the currently selected audio track index.
        /// </summary>
        /// <returns>>=0 if an audio track is selected, -1 if audio is explicitely disabled, -2 is no audio track is (yet) selected, -3 if it was impossible to retrieve the selected audio track (should be treated as an error). </returns>
        int GetAudioTrack();
    }
    
    /// <summary>
    /// Various ClearVRCoreWrapper performance statistics are exposed via the ClearVRCoreWrapperStatistics object and its subclasses ClearVRCoreWrapperVideoStatisticsInterface and ClearVRCoreWrapperAudioStatisticsInterface
    /// Notes:
    /// 1. Android specific: each call to any of its members involves several JNI calls, which are considered to be "expensive". It is therefor advised to use any of these fields with moderation and to not query them every Update() cycle.
    /// </summary>
    public interface ClearVRCoreWrapperStatisticsInterface {
        /// <summary>
        /// the ClearVRCoreWrapperStatistics *must* be properly destroyed to release any claimed resources.
        /// </summary>
        void Destroy();
    }

    /// <summary>
    /// This class is used to expose various video-performance related metrics.
    /// </summary>
    public interface ClearVRCoreWrapperVideoStatisticsInterface {
        /* fields */

        /// <summary>
        /// The average decoder inter-frame latency is defined as the time between two consecutively decoded frames.
        /// Notes.
        /// 1. when in Paused state, this value is expected to increase.
        /// 2. if you are interested in the inter-video-frame render latency (e.g. the average "render framerate"), please check interFrameRenderLatencyMean.
        /// </summary>
        /// <value>The mean inter-frame decoder latency in milliseconds or 0 if unknown.</value>
        float interFrameDecoderLatencyMean { get; }
        /// <summary>
        /// The average decoder inter-frame latency standard deviation is defined as the time between two consecutively decoded frames.
        /// Notes.
        /// 1. when in Paused state, this value is expected to increase.
        /// </summary>
        /// <value>The average inter-frame decoder latency standard deviation in milliseconds, or 0 if unknown.</value>
        float interFrameDecoderLatencyStandardDeviation { get; }
        /// <summary>
        /// The average render inter-frame latency is defined as the time between two consecutively rendered frames.
        /// Notes.
        /// 1. when in Paused state, this value is expected to increase.
        /// </summary>
        /// <value>The mean inter-frame render latency in milliseconds or 0 if unknown.</value>
        float interFrameRenderLatencyMean { get; }
        /// <summary>
        /// The average render inter-frame latency standard deviation is defined as the time between two consecutively rendered frames.
        /// Notes.
        /// 1. when in Paused state, this value is expected to increase.
        /// </summary>
        /// <value>The average inter-frame render latency standard deviation in milliseconds, or 0 if unknown.</value>
        float interFrameRenderLatencyStandardDeviation { get; }
        /// <summary>
        /// Number of frames rendered since creation of the ClearVRPlayer object. 
        /// A rendered frame is defined as a frame that was actually shown to the user.
        /// </summary>
        /// <value>The number of rendered frames.</value>
        long framesRendered { get; }
        /// <summary>
        /// Number of frames dropped since creation of the ClearVRPlayer object. In well behaving environment, this value should be close to zero (0).
        /// A dropped frame is defined as a frame that could not be shown to the user, e.g. because the application framerate was too low, or because there was a hick-up in the decoder pipeline.
        /// </summary>
        /// <value>The number of dropped frames.</value>
        long framesDropped { get; }
        /// <summary>
        /// An advanced metric giving an indication of the quality of the vsync rhythm. In ideal situations, this value should be (very close to) 100%.
        /// This value is averaged over approximately 6 seconds.
        /// </summary>
        /// <value>The vsync quality as a percentage.</value>
        float vsyncQuality { get; }
        /// <summary>
        /// An advanced metric giving an indication of the quality of the frame release rhythm. In ideal situations, this value should be (very close to) 100%. The lower this vallue, the more likely the end-user will notice stuttering/jittering.
        /// This value is averaged over approximately 6 seconds.
        /// </summary>
        /// <value>The frame release quality as a percentage.</value>
        float frameReleaseQuality { get; }
        /// <summary>
        /// An advanced metric measuring the average decoder input queue size. This value should be < 0.3 under normal conditions. A value > 1 indicates that the device might not be able to keep up with the video, resulting in very poor user experience.
        /// This value is averaged over approximately 6 seconds.
        /// </summary>
        /// <value>The average decodere input queue size.</value>
        float averageDecoderInputQueueSize { get; }
        /// <summary>
        /// An advanced metric measuring the average decoder output queue size (e.g. how many decoded frames are queued up and ready for release). This value should be < 3 under normal conditions. A value > 5 indicates that the device might not be able to keep up with the video, resulting in very poor user experience.
        /// This value is averaged over approximately 6 seconds.
        /// </summary>
        /// <value>The average decodere output queue size.</value>
        float averageDecoderOutputQueueSize { get; }
        /// <summary>
        /// The average end-to-end inter-frame latency is defined as the time between the moment an encoded (HEVC) frame was created and it actually being displayed to the end-user.
        /// This metric should be < 90 msec and can be as low as 30 msec.
        /// Notes.
        /// 1. when in Paused state, this value is expected to increase.
        /// 2. this metric has a bit of overshoot. Actual end-to-end latency is slightly lower than reported.
        /// </summary>
        /// <value>The mean end-to-end inter-frame latency in milliseconds or 0 if unknown.</value>
        float endToEndFrameLatencyMean { get; }
        /// <summary>
        /// The average end-to-end inter-frame latency is defined as the time between the moment an encoded (HEVC) frame was created and it actually being displayed to the end-user.
        /// This metric should be < 90 msec and can be as low as 30 msec.
        /// Notes.
        /// 1. when in Paused state, this value is expected to increase.
        /// 2. this metric has a bit of overshoot. Actual end-to-end latency is slightly lower than reported.
        /// </summary>
        /// <value>The average end-to-end inter-frame latency standard deviation in milliseconds, or 0 if unknown.</value>
        float endToEndFrameLatencyStandardDeviation { get; }
        /// <summary>
        /// The average application inter-frame latency is defined as the time between two consequtive application frame updates. 
        /// In Unity, this equals to the time between two Update() cycles.
        /// This metric should be (very) close to the vsync.
        /// </summary>
        /// <value>The mean application inter-frame latency in milliseconds or 0 if unknown.</value>
        float interFrameApplicationLatencyMean { get; }
        /// <summary>
        /// The average application inter-frame latency is defined as the time between two consequtive application frame updates. 
        /// In Unity, this equals to the time between two Update() cycles.
        /// This metric should be (very) close to the vsync.
        /// In well behaving application, standard deviation should be very low < 3 msec. Higher values might indicate a performance bottleneck.
        /// </summary>
        /// <value>The average application inter-frame latency standard deviation in milliseconds, or 0 if unknown.</value>
        float interFrameApplicationLatencyStandardDeviation { get; }

        /* methods */

        /// <summary>
        /// Returns the inter-frame render latency as a formatted string. Example: <mean> +/- <stddev>. Both values have two decimal values to it.
        /// </summary>
        /// <returns>A formatted string.</returns>
        String GetInterFrameRenderLatencyAsPrettyString();
        /// <summary>
        /// Returns the inter-frame decoder latency as a formatted string. Example: <mean> +/- <stddev>. Both values have two decimal values to it.
        /// </summary>
        /// <returns>A formatted string.</returns>
        String GetInterFrameDecoderLatencyAsPrettyString();
        /// <summary>
        /// Returns the inter-frame end-to-end latency as a formatted string. Example: <mean> +/- <stddev>. Both values have two decimal values to it.
        /// Note that this metric has a slight overshoot, and in reality is lower than reported.
        /// </summary>
        /// <returns>A formatted string.</returns>
        String GetEndToEndFrameLatencyAsPrettyString();
        /// <summary>
        /// Returns the inter-frame application latency as a formatted string. Example: <mean> +/- <stddev>. Both values have two decimal values to it.
        /// </summary>
        /// <returns>A formatted string.</returns>
        String GetInterFrameApplicationLatencyAsPrettyString();

        /// <summary>
        /// Returns the inter frame render latency as a two-decimal pretty print framerate string. Example: 59.94.
        /// </summary>
        /// <returns>The pretty-printed framerate.</returns>
        float GetInterFrameRenderRateInFramesPerSecond();
        /// <summary>
        /// Returns the inter frame decoder latency as a two-decimal pretty print framerate string. Example: 59.94.
        /// </summary>
        /// <returns>The pretty-printed framerate.</returns>
        float GetInterFrameDecoderRateInFramesPerSecond();
        /// <summary>
        /// Returns the inter frame application latency as a two-decimal pretty print framerate string. Example: 59.94.
        /// </summary>
        /// <returns>The pretty-printed framerate.</returns>
        float GetInterFrameApplicationRateInFramesPerSecond();
        /// <summary>
        /// Destroys the object, freeing any resources it might have claimed.
        /// </summary>
        void Destroy();
    }

    /// <summary>
    /// This class is used to expose various audio-performance related metrics.
    /// </summary>
    public interface ClearVRCoreWrapperAudioStatisticsInterface {
        /* fields */

        /// <summary>
        /// Number of audio frames rendered since creation of the ClearVRPlayer object. An audio frame is defined as a set of PCM samples of a specific length. For AAC-encoded audio, this audio frame is approximately 21 msec.
        /// A rendered frame is defined as a frame that was actually played to the user.
        /// </summary>
        /// <value>The number of rendered frames.</value>
        long framesRendered { get; }
        /// <summary>
        /// Number of frames dropped since creation of the ClearVRPlayer object. In well behaving environment, this value should be close to zero (0).
        /// A dropped audio frame is defined as an audio frame that could not be played to the user, e.g. because there was a hick-up in the pipeline or performance was not sufficient.
        /// </summary>
        /// <value>The number of dropped frames.</value>
        long framesDropped { get; }
        /// <summary>
        /// The number of buffer underruns during playback. A buffer underrun is defined as a moment in time where no audio data was available to fill the playback buffer. A buffer underrun can result in an audible glitch. 
        /// </summary>
        /// <value>The number of underruns.</value>
        int playbackUnderrunCount { get; }
        
        /* methods */

        /// <summary>
        /// Destroys the object, freeing any resources it might have claimed.
        /// </summary>
        void Destroy();
    }

    /// <summary>
    /// This interface specifies various convenience methods that allws quick access to the most important media stream properties. 
    /// </summary>
    public interface MediaInfoInterface {
        /// <summary>
        /// Query the content duration. 
        /// Notes:
        /// 1. for VOD content, this method returns the duration of the clip
        /// 2. for live content, this method returns 0
        /// 3. if called at an illegal moment (e.g. when no content has yet been loaded), this method returns 0
        /// 4. this is considered an "expensive" method, one is encouraged to call this method too often, but rather cache its value.
        /// </summary>
        /// <returns>The content duration in milliseconds, or 0 otherwise (see notes)</returns>
        long GetContentDurationInMilliseconds();
        /// <summary>
        /// Returns the highest available resolution and framerate as a formatted string: <width>x<height>p<framerate> or 0x0p0 if unknown.
        /// Helpfull if one wants to determine the highest available representation
        /// </summary>
        /// <returns>The highest available quality as a formatted string.</returns>
        String GetHighestQualityResolutionAndFramerate();
        /// <summary>
        /// Returns the resolution and framerate of the currently active represenation as a formatted string: <width>x<height>p<framerate> or 0x0p0 if unknown.
        /// </summary>
        /// <returns>The current quality as a formatted string.</returns>
        String GetCurrentResolutionAndFramerate();
        /// <summary>
        /// Returns the number of audio tracks
        /// Notes:
        /// 1. Returns 0 even if the number of audio tracks could not be queried.
        /// </summary>
        /// <returns>The number of available audio tracks.</returns>
        int GetNumberOfAudioTracks();
        /// <summary>
        /// Returns the content format. Can be used to figure out whether the content is 360 or 180 degrees and if it is monoscopic or stereoscopic.
        /// Notes.
        /// 1. if, for whatever reason, the content format changes, the callback event ContentFormatChanged will be invoked.
        /// </summary>
        /// <returns>The current content format.</returns>
        ContentFormat GetContentFormat();
        /// <summary>
        /// Convenience method that returns whether the current content is stereoscopic or not.
        /// </summary>
        /// <returns>True if the content is stereoscopic, false if monoscopic.</returns>
        bool GetIsContentFormatStereoscopic();
        /// <summary>
        /// Returns the width of decoded video frame. 
        /// Notes:
        /// 1. This width is *not* equal to the width of the actual video frame. One should use GetCurrentResolutionAndFramerate() to retrieve the video width instead.
        /// </summary>
        /// <returns>The decoded video frame width, or 0 if unavailable.</returns>
        int GetDecoderVideoFrameWidth();
        /// <summary>
        /// Returns the height of decoded video frame. 
        /// Notes:
        /// 1. This width is *not* equal to the height of the actual video frame. One should use GetCurrentResolutionAndFramerate() to retrieve the video height instead.
        /// </summary>
        /// <returns>The decoded video frame height, or 0 if unavailable.</returns>
        int GetDecoderVideoFrameHeight();
        /// <summary>
        /// Query the event type of the currently active content item. 
        /// Note that this might change after every SwitchContent(), so one is encouraged to requery this parameter in the ClearVREventTypes.ContentSwitched event.
        /// </summary>
        /// <returns>Returns the appropriate EventTypes enum.</returns>
        EventTypes GetEventType();
    }

    /// <summary>
    /// The performance interface exposes various methods to query the performance of the ClearVRCoreWrapper and the underlying ClearVRCore. Note that the ClearVRCore exposes many more metrics, refer to the ClearVRCore documentation and the "perf.*" parameter keys for details.
    /// </summary>
    public interface PerformanceInterface {
        /// <summary>
        /// Convience method exposing the ClearVRCore "perf.network.current_avg_kbps" metric.
        /// Notes.
        /// 1. This is considered an "expensive" operation and one is encouraged *not* to call this method every frame.
        /// </summary>
        /// <returns>The current network throughput in megabit per second.</returns>
        float GetAverageBitrateInMbps();
        /// <summary>
        /// The ClearVRCoreWrapper class exposes various runtime performance metrics. Use this object to access them. Note that this *must* be destroyed in order to not leak any memory. Typically, this is being taken care of by the underlying layers.
        /// You typically get a reference to this object once for every ClearVRPlayer object you instantiate. The object stays valid thourhgout the entire lifetime of said object, but will be destroyed if the underlying ClearVRPlayer object is destroyed.
        /// </summary>
        /// <value>A ClearVRCoreWrapperStatistics object that can be queried for metrics.</value>
        StatisticsBase clearVRCoreWrapperStatistics { get; }
    }

    /// <summary>
    /// Please do *not* use these APIs in your own project. They are for debugging purposes only and will change without notice
    /// Notes:
    /// 1. this interface is deprecated.
    /// </summary>
    public interface DebugInterface {
    }

    /// <summary>
    /// This is an internal interface and should not be used by the end-user.
    /// </summary>
    public interface PlatformOptionsInterface {
        /// <summary>
        /// Verifies whether the PlatformOptions are correct. 
        /// </summary>
        /// <returns>True if verification succeeded, false otherwise.</returns>
        bool Verify();
    }

    
    public interface ClearVRMeshInterface {
        void Initialize(Vector3 argInitialPostion, Vector3 argInitialRotation, PlatformOptionsBase argPlatformOptions, MeshCreatedStruct argMeshCreatedStruct, System.Object argReserved);
        bool SetMainColor(Color argNewColor);
        void SetShader(Shader argShader);
        void EnableOrDisableMeshRenderer(bool argIsEnabled);
        void SetNativeTexture(Texture2D argNativeTexture);
        void Translate(Vector3 argTranslation, Space argRelativeTo);
        void Translate(Vector3 argTranslation, Transform argRelativeTo);
        void SetContainerGameObject(GameObject argClearVRMeshParentGameObject);
    }

}