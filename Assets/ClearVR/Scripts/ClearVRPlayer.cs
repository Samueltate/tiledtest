﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

/*
This is the MediaPlayer GameObject that wraps the platform specific MediaPlayer<Platform> class that in turn wraps the MediaPlayerBase class
 */
namespace com.tiledmedia.clearvr {
	public class ClearVRPlayer : MonoBehaviour {
		/* Supported platforms */
        private static List<RuntimePlatform> supportedPlatforms = new List<RuntimePlatform>() {RuntimePlatform.Android, RuntimePlatform.IPhonePlayer};
		/* Interfaces */		
		private MediaPlayerInterface 		_mediaPlayerInterface; 
		private MediaControllerInterface 	_mediaControllerInterface;
		private MediaInfoInterface	 		_mediaInfoInterface;
		private PerformanceInterface 		_performanceInterface;
		private DebugInterface		 		_debugInterface;
		private InternalInterface 			_internalInterface; // the internal interface should never be exposed to the application.
		/* internal variables */
		private int  						_wasPausedWhenHeadsetWasTakenOffOrFocusWasLost = 0; // 0 = not set, 1 = was paused, 2 = was not paused. 
		private bool 						_isStoppingStateTriggered = false; // triggered as soon as we hit finalizing state.
		private bool						_isInitializeCalled = false;
		private bool 						_isGameObjectDestroyed = false; // triggered if this object is destroyed (void OnDestroy() was called)
		private int 						_appPauseAndFocusState = (int)AppPauseAndFocusState.UnpausedOrPaused ^ (int)AppPauseAndFocusState.FocusOrNoFocus;
		private enum AppPauseAndFocusState {
			UnpausedOrPaused = 0x01,
			FocusOrNoFocus = 0x02,
		}
		/* internal ClearVREvents event channel */
		private ClearVREvents 				_clearVREvents = new ClearVREvents();
		public class ClearVREvents : UnityEngine.Events.UnityEvent<ClearVRPlayer, ClearVREvent> { 
		}

		/* Enum specifying the actions that can only be triggered on the main Unity thread. */
		enum MainThreadActionTypes {
			PrepareCore,
			Play
		}

		private class MainThreadAction {
			public MainThreadActionTypes type;
			public String payload;
			public MainThreadAction(MainThreadActionTypes argMainThreadActionType, String argPayload){
				type = argMainThreadActionType;
				payload = argPayload;
			}
		}

		/* A FIFO queue that allows us to schedule actions on the main Unity thread. */
		private Queue<MainThreadAction> _mainThreadActionsQueue = new Queue<MainThreadAction>();
		
		public PlatformOptionsBase GetDefaultPlatformOptions() {
			#if UNITY_ANDROID && !UNITY_EDITOR
				return new PlatformOptionsAndroid();
			#endif
            #if UNITY_IOS && !UNITY_EDITOR
                return new PlatformOptionsIOS();
            #endif
			#pragma warning disable 0162
			return new PlatformOptionsBase();
			#pragma warning restore 0162
		}

		public PlatformOptionsBase platformOptions {
			get{
				if(_mediaPlayerInterface != null) {
					return _mediaPlayerInterface.GetPlatformOptions();
				}
				return null;
			}
		}

		public RenderModes renderMode {
			get {
				if(_mediaPlayerInterface != null) {
					return _mediaPlayerInterface.GetRenderMode();
				} else {
					return RenderModes.Native;
				}
			}
			set {
				if(_mediaPlayerInterface != null) {
					_mediaPlayerInterface.SetRenderMode(value);
				}
			}
		}

		/* public accessible interfaces */
		public PerformanceInterface performance {
			get { return _performanceInterface; }
		}

		public MediaInfoInterface mediaInfo {
			get { return _mediaInfoInterface; }
		}

		public MediaControllerInterface controller {
			get { return _mediaControllerInterface; }
		}

		public MediaPlayerInterface mediaPlayer {
			get { return _mediaPlayerInterface; }
		}
		
		public DebugInterface debug {
			get { return _debugInterface; }
		}

		public ClearVREvents clearVREvents {
			get {
				return _clearVREvents;
			}
		}

		/// <summary>
		/// Query whether ClearVR supports the current active Application.platform
		/// </summary>
		/// <returns>true if the platform is supported, false otherwise</returns>
		public static bool GetIsPlatformSupported() {
			return GetIsPlatformSupported(Application.platform);
		}

		public static String GetClearVRCoreVersion() {
            #if UNITY_ANDROID && !UNITY_EDITOR
                return MediaPlayerAndroid.GetClearVRCoreVersion();
            #endif
            #if UNITY_IOS && !UNITY_EDITOR
                return MediaPlayerIOS.GetClearVRCoreVersion();
            #endif
			#pragma warning disable 0162 // Unreachable code
            return "Unknown";
			#pragma warning restore 0162 // Unreachable code
		}

		/// <summary>
		/// Query whether ClearVR supports a specific RuntimePlatform */
		/// </summary>
		/// <param name="argPlatform"> The platform to check for.</param>
		/// <returns></returns>
		public static bool GetIsPlatformSupported(RuntimePlatform argPlatform) {
			return supportedPlatforms.Contains(argPlatform);
		}

		/// <summary>
		/// Create and initialize the ClearVR player. Depending on the platformOptions, content will be loaded automatically
		/// and playback could start as soon as soon as content loading completed.
		/// </summary>
		/// <param name="argPlatformOptions">the platform specific (player) options as set by the parent.</param>
		public ClearVRAsyncRequest Initialize(PlatformOptionsBase argPlatformOptions) {
			ClearVRAsyncRequest clearVRAsyncRequest = _Initialize(argPlatformOptions, false);
			// _Initialize() never returns null, so we can safely access _internalInterface here.
			return _internalInterface.ScheduleClearVRAsyncRequestWithoutCallback(clearVRAsyncRequest);
		}

		public void Initialize(PlatformOptionsBase argPlatformOptions, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ClearVRAsyncRequest clearVRAsyncRequest = _Initialize(argPlatformOptions, true);
			// _Initialize() never returns null, so we can safely access _internalInterface here.
			_internalInterface.ScheduleClearVRAsyncRequest(clearVRAsyncRequest, argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

		private ClearVRAsyncRequest _Initialize(PlatformOptionsBase argPlatformOptions, bool argIsCalledAsynchronous /* unused for now */) {
			_isInitializeCalled = true;
			_isStoppingStateTriggered = false;
			argPlatformOptions.isVRDevicePresent = Utils.GetIsVrDevicePresent();
			if(argPlatformOptions.parentGameObject == null) {
				argPlatformOptions.parentGameObject = this.gameObject;
			}
			// Since v4.0, we need to set the screen width and screen height.
			if(argPlatformOptions.screenWidth == 0) {
				argPlatformOptions.screenWidth = (short) Screen.width;
			}
			if(argPlatformOptions.screenHeight == 0) {
				argPlatformOptions.screenHeight = (short) Screen.height;
			}
			Utils.RedetectDeviceType(); // forcefully redetect headset
			if(argPlatformOptions.deviceType == DeviceTypes.Unknown) {
				argPlatformOptions.deviceType = Utils.GetDeviceType();
			}
			argPlatformOptions.isVRDevicePresent = Utils.GetIsVrDevicePresent();
			if(!argPlatformOptions.Verify()){
				throw new Exception("[ClearVR] Platform options are invalid/incomplete. Please fix your code.");
			}
			MediaPlayerBase _mediaPlayer = CreateMediaPlayerForPlatform(argPlatformOptions);
			if(_mediaPlayer == null) {
				throw new Exception("[ClearVR] Cannot create platform specific media player. Platform not yet supported?");
			}
			_mediaPlayerInterface = _mediaPlayer;
			_mediaControllerInterface = _mediaPlayer;
			_mediaInfoInterface = _mediaPlayer;
			_performanceInterface = _mediaPlayer;
			_debugInterface = _mediaPlayer;
			_internalInterface = _mediaPlayer;
			ClearVRAsyncRequest clearVRAsyncRequest = new ClearVRAsyncRequest(RequestTypes.ClearVRPlayerInitialize);
#if UNITY_IPHONE
            StartCoroutine(WaitForEndOfFrame());
#endif

            return clearVRAsyncRequest;
		}

		private MediaPlayerBase CreateMediaPlayerForPlatform(PlatformOptionsBase argPlatformOptions) {
			#if UNITY_ANDROID && !UNITY_EDITOR
				return new MediaPlayerAndroid(argPlatformOptions, CbClearVREvent);
			#endif
            #if UNITY_IOS && !UNITY_EDITOR
                return new MediaPlayerIOS(argPlatformOptions, CbClearVREvent);
            #endif
			#pragma warning disable 0162 // Unreachable code
            return null;
			#pragma warning restore 0162 // Unreachable code
		}

        IEnumerator WaitForEndOfFrame() {
            var w = new WaitForEndOfFrame();
            while(_internalInterface != null) {
                _internalInterface.EndOfFrame();
                yield return w;
            }
        }

        /// <summary>
        /// Called every frame on the main Unity thread. Will schedule video frame rendering as well as check for pending
        /// player actions scheduled to be performed on the Main Unity thread.
        /// </summary>
        void Update() {
            /* Check if there is anything scheduled to be done on the main Unity thread. */
			if(_mainThreadActionsQueue.Count > 0) {
				while(_mainThreadActionsQueue.Count > 0) {
					MainThreadAction mainThreadAction = _mainThreadActionsQueue.Dequeue();
					switch (mainThreadAction.type) {
						case MainThreadActionTypes.PrepareCore:
							_internalInterface.PrepareCore();
							break;
						case MainThreadActionTypes.Play:
							_mediaControllerInterface.StartPlayout();
							break;
						default:
							break;
					}
				}
			}
			if(_internalInterface != null) {
				_internalInterface.Update();
				_internalInterface.Render();
			}
		}

		/// <summary>
		/// LateUpdate will be called at the end of each rendered frame.
		/// </summary>
		void LateUpdate() {
			if(_internalInterface != null) {
				_internalInterface.LateUpdate();
			}
		}

		/// <summary>
		/// An asynchronous call, allowing you to parse media info of a specific ContentItem. Note that parsing media info 
		/// is only allowed prior loading content for playout and typically only useful when `platformOptions.autoPrepareContentItem == null`
		/// After parsing has completed (wait for the ClearVREventTypes.MediaInfoParsed event), one can query the Core for media properties. 
		/// Refer to the libClearVRCore.pdf documentaton for valid parameters and their meaning.
		/// </summary>
		/// <param name="argContentItem">The content item of which you need to parse media info.</param>
		[Obsolete("ParseMediaInfo() is deprecated, please use clearVRPlayer.mediaPlayer.ParseMediaInfo() instead.", true)]
		public void ParseMediaInfo(ContentItem argContentItem) {
			// This call is asynchronous
			if(_mediaPlayerInterface != null) {
				_mediaPlayerInterface.ParseMediaInfo(argContentItem.manifestUrl);
			} else {
				// should we signal the callee of failure?
			}
		}

		/// <summary>
		/// Apply an absolute and non-incremental rotation to the sphere. Note that you must use this method to rotate the object, as rotating the object yourself
		/// will cause a desynchronization between camera viewport and gameobject. It is recommended to only rotate in the y-plane (yaw) as behaviour is undefined when rotating 
		/// on the x or z axis.
		/// 
		/// For example, to rotate the sphere by 90 degrees clockwise, set argNewEulerAngles = new Vector3(0, +90, 0). This results in the user looking at the content 90 degree to the *left* of him.
		/// </summary>
		/// <param name="argNewEulerAngles">The new absolute angle</param>
		public void Rotate(Vector3 argNewEulerAngles) {
			if(_mediaPlayerInterface != null){
				_mediaPlayerInterface.Rotate(argNewEulerAngles);
			}
		}

		/// <summary>
		/// Recenter the sphere in the y (yaw) plane. Note that x and z axis (pitch and roll respectively) are not compensated. If the user would be looking up, he would still
		/// be looking up after recentering.
		/// </summary>
		public void Recenter() {
			if(_mediaPlayerInterface != null){
				_mediaPlayerInterface.Recenter();
			}
		}

		/// <summary>
		/// This is called AFTER parseMediaInfo returned true (success) until we support spatial audio playback
		/// </summary>
		/// <returns>true if successful, false otherwise</returns>
		public bool SelectNonSpatialAudioTrack() {
			if(_mediaPlayerInterface == null || _mediaControllerInterface == null)
				return false; /// not yet initialized
			if(!_mediaPlayerInterface.GetIsMediaInfoParsed())
				return false;
			// Set to non-foa/non-hoa track if available, or default to the first audio track otherwise.
			try {
				int numberOfAudioTracks = _mediaInfoInterface.GetNumberOfAudioTracks();
				if(numberOfAudioTracks > 0) {
					bool isCompatibleAudioTrackSelected = false;
					for (int i = 0; i < numberOfAudioTracks; i++) {
						if (!bool.Parse(_mediaPlayerInterface.GetClearVRCoreArrayParameter("media.audio.is_spatial_audio", i))) {
							// TODO: Remove this check when broken content VLV content has been removed.
							if(int.Parse(_mediaPlayerInterface.GetClearVRCoreArrayParameter("media.audio.num_channels", i)) < 7) {
								if(_mediaPlayerInterface.SetClearVRCoreParameter("playback.active_audio_track", i.ToString())) {
									isCompatibleAudioTrackSelected = true;
								} else {
									UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An error occured while switching to audio track {0}", i));
								}
								break;
							}
						} else {
							// Unfortunately, this implementation does not yet support spatial audio :(
						}
					}
					if(!isCompatibleAudioTrackSelected) {
						_mediaPlayerInterface.SetClearVRCoreParameter("playback.active_audio_track", "0");
					}
				} else {
					// no audio available
				}
			} catch (Exception e) {
				UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An error occured while setting audio track. Error: {0}", e));
				return false;
			}
			return true;
		}

		/// <summary>
		/// Callback function that handles ClearVRCore events. 
		/// </summary>
		/// <param name="argMediaPlayer">The mediaplayer that triggered the event.</param>
		/// <param name="argClearVREvent">The event that was triggered.</param>
		public virtual void CbClearVREvent(MediaPlayerBase argMediaPlayer, ClearVREvent argClearVREvent) {
			// argClearVREvent.Print(); // Enable this line to print the events that are received. Can be handy for debugging purposes.
			switch (argClearVREvent.type) {
				case ClearVREventTypes.StateChangedUninitialized:
					/* This StateChanged event will never be triggered since no listener is attached by the time this happens. */
					/* In other words, you can safely ignore this state transition. It is only listed for completeness sake */
					break;
				case ClearVREventTypes.StateChangedInitializing:
					break;
				case ClearVREventTypes.StateChangedInitialized:
					if(argClearVREvent.message.result == ClearVRResult.Success) {
						ScheduleMainThreadAction(MainThreadActionTypes.PrepareCore);
					} else {
						// Errors were reported
						FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback(RequestTypes.ClearVRPlayerInitialize, argClearVREvent);
					}
					break;
				case ClearVREventTypes.StateChangedPreparingCore:
					break;
				case ClearVREventTypes.StateChangedCorePrepared:
					if(!_isStoppingStateTriggered) {
						// MediaPlayerBase.CbClearVRCoreWrapperRequestCompleted already takes care of that this is ONLY triggered in case of success.
						// Failures are already converted to a UnableToInitializePlayer by this method, so they never end-up here
						if(argClearVREvent.message.result == ClearVRResult.Success) {
							if(argMediaPlayer.platformOptions.autoPrepareContentItem != null) {
								_mediaPlayerInterface.ParseMediaInfo(argMediaPlayer.platformOptions.autoPrepareContentItem.manifestUrl);
							} else {
								// No content item specified to prepare
								FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback(RequestTypes.ClearVRPlayerInitialize, new ClearVREvent(ClearVREventTypes.GenericMessage, new ClearVRMessage(ClearVRMessageTypes.Info,ClearVRMessageCodes.GenericOK, "ClearVRPlayer prepared, ready to load content.", ClearVRResult.Success)));
							}
						} else {
							// Errors were reported, this should be impossible to trigger
						}
					} else {
						// TODO: should we trigger the FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback() here? We do not do it in the UnitySDK nor the Native Android SDK.
					}
					break;
				case ClearVREventTypes.StateChangedPreparingContentForPlayout:
					break;
				case ClearVREventTypes.StateChangedContentPreparedForPlayout:
					if(!_isStoppingStateTriggered) {
						// MediaPlayerBase.CbClearVRCoreWrapperRequestCompleted already takes care of that this is ONLY triggered in case of success.
						// Failures are already converted to a UnableToInitializePlayer by this method, so they never end-up here.
						if(argClearVREvent.message.result == ClearVRResult.Success) {
							if(argMediaPlayer.platformOptions.isVRDevicePresent) {
								switch(argMediaPlayer.platformOptions.preferredRenderMode) {
									case RenderModes.Native:
										// Do nothing, let core decide.
										break;
									case RenderModes.Monoscopic:
										_mediaPlayerInterface.SetStereoMode(false);
										break;
									case RenderModes.Stereoscopic:
										// This is probably redundant as the core defaults to preferred stereoscopic mode when force_mono = false.
										_mediaPlayerInterface.SetStereoMode(true);
										break;
									default:
										throw new Exception(String.Format("[ClearVR] Render mode {0} not implemented.", argMediaPlayer.platformOptions.preferredRenderMode));
								}
							}
							if(argMediaPlayer.platformOptions.autoPlay) {
								ScheduleMainThreadAction(MainThreadActionTypes.Play);
							} else {
								// It's up to the parent to hit play
								FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback(RequestTypes.ClearVRPlayerInitialize, new ClearVREvent(ClearVREventTypes.GenericMessage, new ClearVRMessage(ClearVRMessageTypes.Info, ClearVRMessageCodes.GenericOK, "ClearVRPlayer loaded content and is ready for playback.", ClearVRResult.Success)));
								// Note that we continue as we need to notify the parent of the state change.
							}
						} else {
							// Errors were reported, but this is code-path is impossible to get triggered
						}
					} else {
						// TODO: should we trigger the FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback() here? We do not do it in the UnitySDK nor the Native Android SDK.
					}
					break;
				case ClearVREventTypes.StateChangedBuffering:
					break;
				case ClearVREventTypes.StateChangedPlaying:
					if(argClearVREvent.message.result == ClearVRResult.Success) {
						if(argMediaPlayer.platformOptions.autoPlay) {
							FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback(RequestTypes.ClearVRPlayerInitialize, new ClearVREvent(ClearVREventTypes.GenericMessage, new ClearVRMessage(ClearVRMessageTypes.Info, ClearVRMessageCodes.GenericOK, "ClearVRPlayer is playing content.", ClearVRResult.Success)));
						} else {
							// No auto play
						}
					} else {
						FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback(RequestTypes.ClearVRPlayerInitialize, argClearVREvent);
					}
					break;
				case ClearVREventTypes.StateChangedPausing:
					break;
				case ClearVREventTypes.StateChangedPaused:
					break;
				case ClearVREventTypes.StateChangedSeeking:
					break;
				case ClearVREventTypes.StateChangedSwitchingContent:
					break;
				case ClearVREventTypes.StateChangedFinished:
					break;
				case ClearVREventTypes.StateChangedStopping:
					_isStoppingStateTriggered = true;
					// Trigger ClearVRPlayerInitialize if applicable. We continue regardless to notify the parent of the state change.
					FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback(RequestTypes.ClearVRPlayerInitialize, new ClearVREvent(ClearVREventTypes.GenericMessage, new ClearVRMessage(ClearVRMessageTypes.Warning, ClearVRMessageCodes.GenericWarning, "Stop request received while initializing player.", ClearVRResult.Failure)));
					break;
				case ClearVREventTypes.StateChangedStopped:
					if(_mediaPlayerInterface != null) {
						switch(_mediaPlayerInterface.GetPlatformOptions().applicationFocusAndPauseHandling) {
							case ApplicationFocusAndPauseHandlingTypes.Legacy:
								break; // do nothing
							case ApplicationFocusAndPauseHandlingTypes.Recommended:
								PlatformOptionsBase platformOptions = _mediaPlayerInterface.GetPlatformOptions();
								if(argClearVREvent.ParseClearVRCoreWrapperStateChangedStoppedEvent(ref platformOptions.autoPrepareContentItem)) { // returns true only in case last active content item was serialized in the message
									// Destroy mediaPlayer
									DestroyMediaPlayer();
									// Restart player
									_isStoppingStateTriggered = false;
									platformOptions.autoPlay = true; // set to true to automatically start playback
									// Make sure that we properly offset start yaw and start pitch to match the latest camera orientation that we have.
									platformOptions.autoPrepareContentItem.startYaw = platformOptions.trackingTransform.eulerAngles.y;
									platformOptions.autoPrepareContentItem.startPitch = platformOptions.trackingTransform.eulerAngles.x;
									Initialize(platformOptions);
									_clearVREvents.Invoke(this, ClearVREvent.GetGenericOKEvent(ClearVREventTypes.ResumingPlaybackAfterApplicationPaused));
									return;
								} else {
									// Do not resume playback
								}
								break;
							case ApplicationFocusAndPauseHandlingTypes.Disabled:
								break; // do nothing
						}
						if(_isGameObjectDestroyed) {
							DestroyMediaPlayer();
						}
					} else {
						// This shouldn't happen
					}
					break;
				/* Non-state change related events */
				case ClearVREventTypes.ParsingMediaInfo:
					break;
				case ClearVREventTypes.MediaInfoParsed:
					if(!_isStoppingStateTriggered) {
						if(argClearVREvent.message.result == ClearVRResult.Success) {
							if(argMediaPlayer.platformOptions.autoPrepareContentItem != null) {
								if(!argMediaPlayer.platformOptions.audioPlaybackEngine.supportsSpatialAudio) {
									SelectNonSpatialAudioTrack();
								} else {
									// We just pick the first audio track for now...
								}
								_mediaPlayerInterface.PrepareContentForPlayout(argMediaPlayer.platformOptions.autoPrepareContentItem);
							} else {
								// It's up to to the parent to prepare content for playout.
								if(FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback(RequestTypes.ClearVRPlayerInitialize, argClearVREvent)) {
									return;
								}
							}
						} else {
							// Warning or FatalError was reported while parsing media info.
							if(argClearVREvent.message.GetIsFatalError()) {
								if(FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback(RequestTypes.ClearVRPlayerInitialize, argClearVREvent)) {
									return;
								} // else this was a parseMediaInfo request that was NOT part of the ClearVRPlayerInitialize process.
							} // else this was a warning message and we safely continue without triggering the ClearVRPlayerInitialize if applicable
						}
					}
					break;
				case ClearVREventTypes.RenderModeChanged:
					break;
				case ClearVREventTypes.FirstFrameRendered:
					break;
				case ClearVREventTypes.AudioTrackSwitched:
					break;
				case ClearVREventTypes.ContentSwitched:
					break;
				case ClearVREventTypes.UnableToInitializePlayer:
					if(argClearVREvent.message.GetIsFatalError()) {
						// If it is a FatalError, all hope is lost on a successful ClearVRPlayerInitialize. 
						if(FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback(RequestTypes.ClearVRPlayerInitialize, argClearVREvent)) {
							return;
						}
					} else {
						// There is still hope, so we continue
					}
					break;
				case ClearVREventTypes.ResumingPlaybackAfterApplicationPaused:
					break;
				case ClearVREventTypes.StereoModeSwitched:
					break;
				// Since v4.0
				case ClearVREventTypes.ContentFormatChanged:
					break;
				case ClearVREventTypes.ABRSwitch:
					break;
				case ClearVREventTypes.PrewarmCacheCompleted:
					break;
				case ClearVREventTypes.GenericMessage:
					break;
				default:
					break;
			}
			switch(argClearVREvent.message.type) {
				case ClearVRMessageTypes.FatalError:
					break; // intentionally ignored
				case ClearVRMessageTypes.Warning:
					break; // intentionally ignored
				case ClearVRMessageTypes.Info:
					/* Parse harmless info messages */
                    switch (argClearVREvent.message.code) {
						case (int) ClearVRMessageCodes.ClearVRCoreWrapperGenericInfo:
							break; // intentionally ignored
						case (int) ClearVRMessageCodes.ClearVRCorWrapperOpenGLVersionInfo:
							break; // intentionally ignored
						case (int) ClearVRMessageCodes.ClearVRCoreWrapperVideoDecoderCapabilities:
							break; // intentionally ignored
						case (int) ClearVRMessageCodes.GenericOK:
							break; // intentionally ignored
						case (int) ClearVRMessageCodes.ClearVRCoreWrapperAudioTrackChanged:
							break; // intentionally ignored
						case (int) ClearVRMessageCodes.ClearVRCoreWrapperStereoscopicModeChanged:
							// This needs to be handled on the Main Unity Thread
							bool isSuccess = (argClearVREvent.message.result == ClearVRResult.Success);
							if(platformOptions.enableAutomaticRenderModeSwitching) {
								if(isSuccess) {
									switch(argClearVREvent.message.message){
										case "stereo":
										case "stereoscopic":
											_mediaPlayerInterface.SetRenderMode(RenderModes.Stereoscopic);
											break;
										case "mono":
										case "monoscopic":
											_mediaPlayerInterface.SetRenderMode(RenderModes.Monoscopic);
											break;
										default:
											UnityEngine.Debug.LogWarning(String.Format("[ClearVR] Got an unexpected value when switching stereo mode. Got: {0}, allowed: 'mono' and 'stereo'.", argClearVREvent.message.message));
											break;
									}
								} else {
									UnityEngine.Debug.LogWarning(String.Format("[ClearVR] Unable to change stereo mode. Error: {0}.", argClearVREvent.message.message));
								}
							}// else, the application has to do the hard work himself.
							break;
						default:
							break;
					}
				break;
			}			
			if(argClearVREvent.clearVRAsyncRequestResponse != null) {
				if(argClearVREvent.clearVRAsyncRequestResponse.requestId != ClearVRAsyncRequest.CLEAR_VR_REQUEST_ID_NOT_SET) {
					if(FindAndForwardClearVREventAsClearVRAsyncRequestResponse(argClearVREvent)) {
						return;
					} else {
						bool isFound = false;
						for(int i = 0; i < _internalInterface.clearVRAsyncRequestsWithoutCallback.Count; i++) {
							if(_internalInterface.clearVRAsyncRequestsWithoutCallback[i].requestId == argClearVREvent.clearVRAsyncRequestResponse.requestId) { 
								_internalInterface.clearVRAsyncRequestsWithoutCallback.Remove(_internalInterface.clearVRAsyncRequestsWithoutCallback[i]);
								isFound = true;
								break;
							}
						}
						if(!isFound) {
							// A response to a Stop "request" can be received even if we didn't ask for it (e.g. when something bad happened). In that case, we don't need this warning and we should just forward the event.
							if(argClearVREvent.clearVRAsyncRequestResponse.requestType != RequestTypes.Stop) {
								UnityEngine.Debug.LogWarning(String.Format("No matching request found for request response. Request id: {0}, type: {1}. Message: {2}", argClearVREvent.clearVRAsyncRequestResponse.requestId, argClearVREvent.clearVRAsyncRequestResponse.requestType, argClearVREvent.message.GetFullMessage()));
							}
						}
					}
				}
			}
			/* Forward event to any listener that may have been subscribed */
			_clearVREvents.Invoke(this, argClearVREvent);
		}

		bool FindAndForwardClearVREventAsClearVRAsyncRequestResponse(ClearVREvent argClearVREvent, ClearVRAsyncRequest argClearAsyncRequest = null) {
			bool isMatchingClearVRAsyncRequestFound = false;
			ClearVRAsyncRequest clearVRAsyncRequest = argClearAsyncRequest;
			if(clearVRAsyncRequest == null) {
				int count = _internalInterface.clearVRAsyncRequests.Count;
				for(int i = 0; i < count; i++) {
					clearVRAsyncRequest = _internalInterface.clearVRAsyncRequests[i];
					if(clearVRAsyncRequest.requestId == argClearVREvent.clearVRAsyncRequestResponse.requestId) {
						isMatchingClearVRAsyncRequestFound = true;
						break;
					}
				}
				if(!isMatchingClearVRAsyncRequestFound) {
					clearVRAsyncRequest = argClearAsyncRequest; // reset to original value.
				}
			} else {
				isMatchingClearVRAsyncRequestFound = true;
			}
			if(isMatchingClearVRAsyncRequestFound) {
				argClearVREvent.clearVRAsyncRequestResponse.optionalArguments = clearVRAsyncRequest.optionalArguments;
				if(clearVRAsyncRequest.cbClearVRAsyncResponseReceived != null) {
					clearVRAsyncRequest.cbClearVRAsyncResponseReceived(argClearVREvent, this);
				}
				_internalInterface.clearVRAsyncRequests.Remove(clearVRAsyncRequest);
			}
			return isMatchingClearVRAsyncRequestFound;
		}
		
		bool FindPendingClearVRAsyncRequestByRequestTypeAndTriggerCallback(RequestTypes argRequestType, ClearVREvent argClearVREvent) {
			ClearVRAsyncRequest clearVRAsyncRequest;
			for(int i = 0; i < _internalInterface.clearVRAsyncRequests.Count; i++) {
				clearVRAsyncRequest = _internalInterface.clearVRAsyncRequests[i];
				if(clearVRAsyncRequest.requestType == argRequestType) {
					FindAndForwardClearVREventAsClearVRAsyncRequestResponse(new ClearVREvent(argClearVREvent.type, new ClearVRAsyncRequestResponse(RequestTypes.ClearVRPlayerInitialize, clearVRAsyncRequest.requestId), argClearVREvent.message), clearVRAsyncRequest);
					return true;
				}
			}
			for(int i = 0; i < _internalInterface.clearVRAsyncRequestsWithoutCallback.Count; i++) {
				clearVRAsyncRequest = _internalInterface.clearVRAsyncRequestsWithoutCallback[i];
				if(clearVRAsyncRequest.requestType == argRequestType) {
					_internalInterface.clearVRAsyncRequestsWithoutCallback.Remove(clearVRAsyncRequest);
					return false;
				}
			}
			return false;
		}			

		/// <summary>
		/// Gets and reqrites the ClearVREvent so that it is linked to the ClearVRPlayerInitialize Request.
		/// If ClearVRPlayer.Initialize() was called without callback Action, this will return false and the ClearVREvent won't be rewritten.
		/// </summary>
		/// <param name="argClearVREvent"></param>
		/// <returns>true if a pending ClearVRAsyncRequest was found, false otherwise.</returns>
		bool GetClearVRPlayerInitializeClearVRAsyncRequest(ref ClearVREvent argClearVREvent) {
			ClearVRAsyncRequest clearVRAsyncRequest;
			for(int i = 0; i < _internalInterface.clearVRAsyncRequests.Count; i++) {
				clearVRAsyncRequest = _internalInterface.clearVRAsyncRequests[i];
				if(clearVRAsyncRequest.requestType == RequestTypes.ClearVRPlayerInitialize) {
					ClearVREvent newClearVREvent = new ClearVREvent(argClearVREvent.type, new ClearVRAsyncRequestResponse(RequestTypes.ClearVRPlayerInitialize, clearVRAsyncRequest.requestId), argClearVREvent.message);
					argClearVREvent = newClearVREvent;
					return true;
				}
			}
			return false;
		}

		/// <summary>
		/// Triggered as soon as the application looses or regains focus. Note that loss of focus typically happens when a pop-up modal surfaces.
		/// Application pause, on the other hand, is triggered if the app is pushed to the background.
		/// </summary>
		/// <param name="argHasFocus"></param>
		void OnApplicationFocus(bool argHasFocus) {
			_appPauseAndFocusState = _appPauseAndFocusState ^ (int) AppPauseAndFocusState.FocusOrNoFocus;
			if(_mediaPlayerInterface != null) {
				switch(_mediaPlayerInterface.GetPlatformOptions().applicationFocusAndPauseHandling) {
					case ApplicationFocusAndPauseHandlingTypes.Legacy:
						/*
						In case the application looses focus we have to at least pause playback, otherwise the application
						would keep draining our battery in the background. Note that even when in paused, our
						ClearVR	library still consumes some resources! This is more of a work-around than a proper solution!
						 */
						OnApplicationPause(!argHasFocus);
						break;
					case ApplicationFocusAndPauseHandlingTypes.Recommended:
						/* 
						This will completely destroy the player object, freeing any resources that might have been claimed
						 */
						PausePlaybackAfterAppFocusOrPauseChangedRecommended(argHasFocus);
						break;
					case ApplicationFocusAndPauseHandlingTypes.Disabled:
					default:
						break;
				}
			}
		}

		/// <summary>
		/// Triggered when the application is pushed to the background. Please read the note at OnApplicationFocus() for more details.
		/// </summary>
		void OnApplicationPause(bool argPauseStatus) {
			_appPauseAndFocusState = _appPauseAndFocusState ^ (int) AppPauseAndFocusState.UnpausedOrPaused;
			if(_mediaPlayerInterface == null) {
				return;
			}
			switch(_mediaPlayerInterface.GetPlatformOptions().applicationFocusAndPauseHandling) {
				case ApplicationFocusAndPauseHandlingTypes.Legacy:
					PauseOrUnpausePlaybackAfterAppFocusOrPauseChangedLegacy(argPauseStatus);
					break;
				case ApplicationFocusAndPauseHandlingTypes.Recommended:
					if(argPauseStatus) {
						_mediaControllerInterface.Stop(true);
					} else {
						// Cannot happen
					}
					break;
				case ApplicationFocusAndPauseHandlingTypes.Disabled:
				default:
					break;
			}
		}

		void PausePlaybackAfterAppFocusOrPauseChangedRecommended(bool argValue) {
			if(_mediaPlayerInterface == null || _mediaControllerInterface == null || (_appPauseAndFocusState & (int)AppPauseAndFocusState.UnpausedOrPaused) == 0) {
				return;
			}
			bool isPaused = _mediaPlayerInterface.GetIsInPausingOrPausedState();
			if(!argValue) {
				// We lost focus
				_wasPausedWhenHeadsetWasTakenOffOrFocusWasLost = isPaused ? 1 : 2;
				if(!isPaused) {
					_mediaControllerInterface.Pause();
				} else{
					// already paused
				}
			} else {
				// regained focus, only triggered when we are still alive
				if(_wasPausedWhenHeadsetWasTakenOffOrFocusWasLost == 2) {
					_mediaControllerInterface.Unpause();
				}
				_wasPausedWhenHeadsetWasTakenOffOrFocusWasLost = 0;
			}
		}

		void PauseOrUnpausePlaybackAfterAppFocusOrPauseChangedLegacy(bool argValue) {
			if(_mediaPlayerInterface == null || _mediaControllerInterface == null)
				return;
			if(argValue) { // true = we should pause, false = we should unpause.
				bool isPaused = _mediaPlayerInterface.GetIsInPausingOrPausedState();
				if(_wasPausedWhenHeadsetWasTakenOffOrFocusWasLost == 0) {
					_wasPausedWhenHeadsetWasTakenOffOrFocusWasLost = isPaused ? 1 : 2;
					if(!isPaused) {
						_mediaControllerInterface.Pause();
					} else {
						// already paused
					}
				} else {
					// no need to pause again.
				}
			} else {
				if(_wasPausedWhenHeadsetWasTakenOffOrFocusWasLost == 2) {
					// We give it a little bit of delay so that Unity and (optionally) the OVR stack can resume
					// If we would unpause right away, some video frames could get dropped.
					StartCoroutine(DelayedUnpause(0.35f));
				}
				_wasPausedWhenHeadsetWasTakenOffOrFocusWasLost = 0;
			}
		}

		private IEnumerator DelayedUnpause(float argDelayInSeconds) {
	        	yield return new WaitForSeconds(argDelayInSeconds);
			_mediaControllerInterface.Unpause();
		}

		void ScheduleMainThreadAction(MainThreadActionTypes argMainThreadActionType, String argPayload = "") {
			/* Schedule action to be invoked in the next Update() cycle */
			_mainThreadActionsQueue.Enqueue(new MainThreadAction(argMainThreadActionType, argPayload));
		}

		/// <summary>
		/// Since v4.1.2
		/// Destroys the internal MediaPlayer object. When destroyed, one is free to create a new mediaplayer by calling clearVRPlayer.Initialize() again. 
		/// This is an advanced API and one is strongly discouraged to use it. In general, we highly recommended to destroy the ClearVRPlayer object after every Initialize -> Stopped -> Destroy() cycle.
		/// 
		/// Notes.
		/// 1. One must first Stop() a clearVRPlayer object before calling this method. The clearVRPlayer must be in Stopped state.
		/// 2. When ApplicationFocusAndPauseHandlingTypes.Recommended is used, one should not call this method. A typical usecase is when one uses ApplicationFocusAndPauseHandlingTypes.Disabled to override application suspend/resume behaviour.
		/// 3. If one calls Destroy(clearVRPlayer);, this method will take care of first stopping (if it wasn't already in Stopped) and subsequently destroying the ClearVRPlayer as to not leak any resources.
		/// </summary>
		public void DestroyMediaPlayer() {
			if(!_isInitializeCalled) {
				return;
			}
			if(_internalInterface != null) {
				if(_mediaPlayerInterface != null) { // Highly unlikely
					if(_mediaPlayerInterface.GetIsInStoppedState()) {
						_internalInterface.Destroy();
						Reset();
					} else {
						if(_isGameObjectDestroyed) {
							// First call stop before we can destroy the MediaPlayer.
							// Next, in Stopped, we destroy the mediaPlayer only if _isGameObjectDestroyed = true
							_mediaControllerInterface.Stop(false);
						} else {
							throw new Exception("[ClearVR] Cannot destroy a mediaPlayer if it is not in Stopped state. Call clearVRPlayer.Stop() first and wait for the ClearVREventTypes.StateChangedStopped event.");
						}
					}
				} else {
					throw new Exception("[ClearVR] Cannot destroy mediaPlayer. Do not call this method multiple times.");
				}
			} else {
				// This would be extremely unlikely to get triggered, only possible in case of a race condition
				throw new Exception("[ClearVR] Cannot destroy mediaPlayer. No mediaPlayer active or mediaPlayer already destroyed? This might indicate a problem with your application logic. Make sure you do not call this method twice.");
			}
		}

		/// <summary>
		/// This will give us one more frame to flush any pending AsyncMeshEvents. 
		/// This is required to destroy our ClearVRMesh in case a fatal error occured in the ClearVRCore or ClearVRCoreWrapper.
		/// </summary>
		/// <returns></returns>
		void Reset() {
			if(_internalInterface != null) {
				_internalInterface.Update();
			}
			_mediaPlayerInterface = null;
			_mediaControllerInterface = null;
			_mediaInfoInterface = null;
			_performanceInterface = null;
			_debugInterface = null;
			_internalInterface = null;
			_isInitializeCalled = false;
		}

		void OnDestroy() {
            _isGameObjectDestroyed = true;
			DestroyMediaPlayer();
		}

		public void DestroyGameComponentAndCleanUp() {
			// intentionally left blank for now
		}

		public void OnApplicationQuit() {
			DestroyMediaPlayer();
		}


		/* These methods are obsolete. */

		/// <summary>
		/// Stop playback. Note that it is up to the parent to listen for the `ClearVREventTypes.StateChangedStopped` event and clean up the gameobject associated with the player.
		/// </summary>
		[Obsolete("Stop() is deprecated, please use clearVRPlayer.controller.Stop() instead.", true)]
		public void Stop() {
			if(_mediaControllerInterface != null) {
				_mediaControllerInterface.Stop(false);
			} else {
				ClearVREvent clearVREvent = ClearVREvent.GetGenericOKEvent(ClearVREventTypes.StateChangedStopped);
				_clearVREvents.Invoke(this, clearVREvent);
				// In this case, literally nothing has been instantiated yet. Is it even possible to get in this state?
			}
		}

		/// <summary>
		/// Start playback
		/// </summary>
		[Obsolete("StartPlayout() is deprecated, please use clearVRPlayer.controller.StartPlayout() instead.", true)]
		public void StartPlayout() {
			if(_mediaControllerInterface != null) {
				_mediaControllerInterface.StartPlayout();
			}
		}
		
		/// <summary>
		/// Pause playback. Note that this call is asynchronous.
		/// </summary>
		[Obsolete("Pause() is deprecated, please use clearVRPlayer.controller.Pause() instead.", true)]
		public ClearVRAsyncRequest Pause() {
			if(_mediaControllerInterface != null) {
				return _mediaControllerInterface.Pause();
			}
			return null;
		}


		/// <summary>
		/// Resume playback. Note that this call is asynchronous.
		/// </summary>
		[Obsolete("Unpause() is deprecated, please use clearVRPlayer.controller.Unpause() instead.", true)]
		public void Unpause() {
			if(_mediaControllerInterface != null) {
				_mediaControllerInterface.Unpause();
			}
		}
		
		/// <summary>
		/// Seek to specified position (in milliseconds). Note that this will see to the closest I-frame in the video.
		/// </summary>
		/// <param name="argNewPositionInMilliseconds">The absolute position to seek to.</param>
		[Obsolete("Seek() is deprecated, please use clearVRPlayer.controller.Seek() instead.", true)]
		public void Seek(long argNewPositionInMilliseconds) {
			if(_mediaControllerInterface != null) {
				_mediaControllerInterface.Seek(argNewPositionInMilliseconds);
			}
		}
	}
}
