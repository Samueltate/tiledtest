﻿using System;
using UnityEngine;
namespace com.tiledmedia.clearvr {
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public abstract class ClearVRMeshBase : MonoBehaviour, ClearVRMeshInterface {
        public GameObject containerGameObject { get; private set; }
        public Mesh mesh;
        public int signature {
            get {
                return _signature;
            }
        }
        public bool isReadyForDestruction {
            get {
                return _isReadyForDestruction;
            }
            set {
                _isReadyForDestruction = value;
            }
        }

        protected bool _isReadyForDestruction = false;
        protected MeshRenderer _meshRenderer;
        protected Material _material;
        protected Vector3 _initialEulerAngles;
        protected Vector3 _eulerAngles;
        protected int _frameWidth = 0;
        protected int _frameHeight = 0;
        protected bool _isStereoCustomUVsOnKeywordEnabled = false;
        protected bool _isOESFastPathOnKeywordEnabled = false;
        protected bool _isTransparencyOnKeywordEnabled = false;
        protected bool _isZWriteEnabled = true /* default value is true */;
        protected int _signature;
        public ClearVRMeshTypes clearVRMeshType {get; private set; }

        public Vector3 eulerAngles {
            get { return _eulerAngles; }
        }

        public int frameWidth {
            get { return _frameWidth; }
        }

        public int frameHeight {
            get { return _frameHeight; }
        }

        /// <summary>
        /// Initialize a new ClearVRMesh for 360 degree video, setting its position and orientation. Actual construction is done in this.CreateMesh()
        /// </summary>
        /// <param name="argInitialPostion">The initial position (relative to the parent) of the gameobject</param>
        /// <param name="argInitialRotation">The initial rotation (relative to the parent) of the gameobject</param>
        /// <param name="argPlatformOptions">The platformoptions object</param>
        public virtual void Initialize( Vector3 argInitialPostion, 
                                Vector3 argInitialRotation, 
                                PlatformOptionsBase argPlatformOptions, 
                                MeshCreatedStruct argMeshCreatedStruct,
                                System.Object argReserved) {
            _initialEulerAngles = argInitialRotation;
            _eulerAngles = _initialEulerAngles;
            gameObject.transform.localRotation = Quaternion.Euler(argInitialRotation);
            gameObject.transform.localPosition = argInitialPostion;
            gameObject.layer = argPlatformOptions.sphereLayer;
            _signature = argMeshCreatedStruct.signature;
            _frameWidth = argMeshCreatedStruct.frameWidth;
            _frameHeight = argMeshCreatedStruct.frameHeight;
            mesh = new Mesh();
            mesh.name = "Procedural ClearVRMesh";
            // Initialize with empty vertices/normals/triangles and UVs to allocate the appropriate amount of memory that we will access from the native renderer plugin
            mesh.vertices = new Vector3[argMeshCreatedStruct.vertexCount];
            mesh.normals = new Vector3[argMeshCreatedStruct.vertexCount];
            mesh.triangles = new int[argMeshCreatedStruct.indexCount];
            mesh.uv = new Vector2[argMeshCreatedStruct.vertexCount];
            mesh.uv2 = new Vector2[argMeshCreatedStruct.vertexCount];
            // Set proper bounds on this mesh, otherwise it would be culled by the camera (default values would be 0 for all bounds fields as all vertices are 0 at this point)
            Bounds bounds = new Bounds();
            bounds.center = gameObject.transform.localPosition;
            bounds.extents = new Vector3(argMeshCreatedStruct.boundsX, argMeshCreatedStruct.boundsY, argMeshCreatedStruct.boundsZ);
            mesh.bounds = bounds;
            mesh.MarkDynamic();
            MeshFilter meshFilter = this.GetComponent<MeshFilter>();
            meshFilter.mesh = mesh;
            _meshRenderer = meshFilter.GetComponent<MeshRenderer>();
            _material = _meshRenderer.material;
            // Disable MeshRenderer until its texture is available (otherwise it would show up as a texture-less (purple) sphere)
            EnableOrDisableMeshRenderer(false);
        }

        public void Rotate(Vector3 argNewEulerAngles) {
            // We have to take the minus of the two angles for ClearVRCore.
            _eulerAngles = _initialEulerAngles - argNewEulerAngles;
            _eulerAngles.y = (360 + (_eulerAngles.y % 360)) % 360;

            // Yet the Unity orientation requires us to take the plus of the two
            argNewEulerAngles = (_initialEulerAngles + argNewEulerAngles); 
            // Properly bound yaw between [0, 360]
            argNewEulerAngles.y = (360 + (argNewEulerAngles.y % 360)) % 360;
            gameObject.transform.localRotation = Quaternion.Euler(argNewEulerAngles);
        }

        public virtual void Translate(Vector3 argTranslation, Space argRelativeTo) {
            gameObject.transform.Translate(argTranslation, argRelativeTo);
        }

        public virtual void Translate(Vector3 argTranslation, Transform argRelativeTo) {
            gameObject.transform.Translate(argTranslation, argRelativeTo);
        }

        public void EnableOrDisableStereoscopicRendering(bool argValue) {
            if(_meshRenderer != null) {
                if(argValue != _isStereoCustomUVsOnKeywordEnabled) {
                    if(argValue) {
                        _material.EnableKeyword("STEREO_CUSTOM_UV_ON");
                        _material.DisableKeyword("STEREO_CUSTOM_UV_OFF");
                    } else {
                        _material.DisableKeyword("STEREO_CUSTOM_UV_ON");
                        _material.EnableKeyword("STEREO_CUSTOM_UV_OFF");
                    }
                    _isStereoCustomUVsOnKeywordEnabled = argValue;
                }
            }
        }

        public void EnableOrDisableOESFastPath(bool argValue) {
            if(_meshRenderer != null) {
                if(argValue != _isOESFastPathOnKeywordEnabled) {
                    if(argValue) {
                        _material.EnableKeyword("USE_OES_FAST_PATH_ON");
                        _material.DisableKeyword("USE_OES_FAST_PATH_OFF");
                    } else {
                        _material.DisableKeyword("USE_OES_FAST_PATH_ON");
                        _material.EnableKeyword("USE_OES_FAST_PATH_OFF");
                    }
                    _isOESFastPathOnKeywordEnabled = argValue;
                }
            }
        }
        
        public void EnableOrDisableTransparency(bool argValue) {
            if(_meshRenderer != null) {
                if(argValue != _isTransparencyOnKeywordEnabled) {
                    if(argValue) {
                        _material.SetOverrideTag("RenderType", "Transparent");
                        _material.SetOverrideTag("Queue", "Transparent");
                        _material.SetInt ("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                        _material.SetInt ("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                        _material.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;

                    } else {
                        _material.SetOverrideTag("RenderType", "Opaque");
                        _material.SetOverrideTag("Queue", "Opaque");
                        _material.SetInt ("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                        _material.SetInt ("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                        _material.renderQueue = -1;
                    }
                    _isTransparencyOnKeywordEnabled = argValue;
                }
            }
        }

        
        public void SetZWrite(bool argValue) {
            if(_meshRenderer != null) {
                if(argValue != _isZWriteEnabled) {
                    _material.SetInt ("_ZWrite", Convert.ToInt32(argValue));
                    _isZWriteEnabled = argValue;
                }
            }
        }

        public bool SetMainColor(Color argNewColor) {
            if (_material != null) {
                if(argNewColor.a != 1) { 
                    EnableOrDisableTransparency(true);
                } else {
                    EnableOrDisableTransparency(false);
                }
                _material.SetColor("_Color", argNewColor);
                return true;
            }
            return false;
        }

        // Set-up the shader
        public virtual void SetShader(Shader argShader) {
            _material.shader = argShader;
        }

        // Bind the texture to the shader
        public virtual void SetNativeTexture(Texture2D argNativeTexture) {
            _material.SetTexture("_MainTex", argNativeTexture);
            if(argNativeTexture == null) {
                _material.mainTexture = null;
            }
        }

        // Enable or disable whether we actually see the mesh that rendering video
        public virtual void EnableOrDisableMeshRenderer(bool argIsEnabled) {
            _meshRenderer.enabled = argIsEnabled;
        }

        void OnDestroy() {
            if(mesh != null)
                UnityEngine.Object.Destroy(mesh);
            if(containerGameObject != null)
                UnityEngine.Object.Destroy(containerGameObject);
        }

        public virtual void SetContainerGameObject(GameObject argClearVRMeshParentGameObject) {
            containerGameObject = argClearVRMeshParentGameObject;
        }
    }
}
