﻿#if UNITY_ANDROID
/*
Tiledmedia Sample Player for Unity3D
(c) Tiledmedia, 2017 - 2018

Author: Arjen Veenhuizen
Contact: arjen@tiledmedia.com

*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Reflection;
using System;
using UnityEngine.XR;
using System.Diagnostics;
using System.Threading;
using AOT;
using System.Runtime.CompilerServices;

namespace com.tiledmedia.clearvr {
	public class MediaPlayerAndroid : MediaPlayerBase {
		protected class ClearVRAndroidJNIConverter {

			public const String clearVRAsyncRequestResponseClassName = "com/tiledmedia/clearvrcorewrapper/ClearVRAsyncRequestResponse";
			public const String clearVRAsyncRequestClassName = "com/tiledmedia/clearvrcorewrapper/ClearVRAsyncRequest";
			public const String clearVRRequestTypesClassName = "com/tiledmedia/clearvrenums/ClearVRAsyncRequestTypes";
			public const String clearVRMessageTypesClassName = "com/tiledmedia/clearvrenums/ClearVRMessageTypes";
			public const String clearVREventTypesClassName = "com/tiledmedia/clearvrenums/EventTypes";	
			public const String clearVRMessageClassName = "com/tiledmedia/clearvrcorewrapper/ClearVRMessage";
			public const String clearVRInitializeParametersClassName = "com/tiledmedia/clearvrparameters/InitializeParameters";

			private jvalue[] emptyJvalue = new jvalue[0];

			/* Classes */
			private IntPtr pClearVRAsyncRequestResponseClass = IntPtr.Zero;
			private IntPtr pClearVRAsyncRequestClass = IntPtr.Zero;
			private IntPtr pClearVRMessageClass = IntPtr.Zero;
			private IntPtr pClearVRAsyncRequestTypesClass = IntPtr.Zero;
			private IntPtr pClearVRMessageTypesClass = IntPtr.Zero;
			private IntPtr pClearVREventTypesClass = IntPtr.Zero;
			private IntPtr clearVRInitializeParametersRawClassGlobalRef = IntPtr.Zero;
			/* ClearVRInitializeParameters constructor method id */
			private IntPtr clearVRInitializeParametersClassConstructorMethodId = IntPtr.Zero;
			private IntPtr initializeParametersGlobalRef = IntPtr.Zero;
			/* ClearVRRequestTypes getValue() method */
			private IntPtr pClearVRRequestTypesGetValueMethodID = IntPtr.Zero;

			/* ClearVRMessageTypes getValue() method */
			private IntPtr pClearVRMessageTypesGetValueMethodID = IntPtr.Zero;

			/* ClearVREventTypes getValue() method */
			private IntPtr pClearVREventTypesGetValueMethodID = IntPtr.Zero;

			/* ClearVRAsyncRequest Fields */
			private IntPtr pClearVRAsyncRequestRequestTypeFieldID = IntPtr.Zero;
			private IntPtr pClearVRAsyncRequestRequestIdFieldID = IntPtr.Zero;

			/* ClearVRAsyncRequestResponse Fields */
			private IntPtr pClearVRAsyncRequestResponseMessageFieldID = IntPtr.Zero;
			private IntPtr pClearVRAsyncRequestResponseRequestTypeFieldID = IntPtr.Zero;
			private IntPtr pClearVRAsyncRequestResponseRequestIdFieldID = IntPtr.Zero;
			/* ClearVRMessage fields */
			private IntPtr pIsSuccessFieldID = IntPtr.Zero;
			private IntPtr pClearVRMessageMessageTypeFieldID = IntPtr.Zero;
			private IntPtr pClearVRMessageCodeFieldID = IntPtr.Zero;
			private IntPtr pClearVRMessageMessageFieldID = IntPtr.Zero;

			public ClearVRAndroidJNIConverter() {
				// Get references to the various classes we need.
				pClearVRAsyncRequestResponseClass = AndroidJNI.FindClass(clearVRAsyncRequestResponseClassName);
				pClearVRAsyncRequestClass = AndroidJNI.FindClass(clearVRAsyncRequestClassName);
				pClearVRMessageClass = AndroidJNI.FindClass(clearVRMessageClassName);
				pClearVRAsyncRequestTypesClass = AndroidJNI.FindClass(clearVRRequestTypesClassName);
				pClearVRMessageTypesClass = AndroidJNI.FindClass(clearVRMessageTypesClassName);
				pClearVREventTypesClass = AndroidJNI.FindClass(clearVREventTypesClassName);
				IntPtr clearVRInitializeParametersClass = AndroidJNI.FindClass(clearVRInitializeParametersClassName);
				clearVRInitializeParametersRawClassGlobalRef = AndroidJNI.NewGlobalRef(clearVRInitializeParametersClass);

				// Get value field from ClearVRRequestTypes "class" (which is actually an enum)
				pClearVRRequestTypesGetValueMethodID = AndroidJNI.GetMethodID(pClearVRAsyncRequestTypesClass, "getValue", "()I");
				pClearVRMessageTypesGetValueMethodID = AndroidJNI.GetMethodID(pClearVRMessageTypesClass, "getValue", "()I");
				pClearVREventTypesGetValueMethodID = AndroidJNI.GetMethodID(pClearVREventTypesClass, "getValue", "()Ljava/lang/String;");
				// The requestType field is an enum so we need to access it as if it were a class
				pClearVRAsyncRequestResponseRequestTypeFieldID = AndroidJNI.GetFieldID(pClearVRAsyncRequestResponseClass, "requestType", String.Format("L{0};", clearVRRequestTypesClassName));
				// requestId
				pClearVRAsyncRequestResponseRequestIdFieldID = AndroidJNI.GetFieldID(pClearVRAsyncRequestResponseClass, "requestId", "I");
				// clearVRMessage
				pClearVRAsyncRequestResponseMessageFieldID = AndroidJNI.GetFieldID(pClearVRAsyncRequestResponseClass, "clearVRMessage", String.Format("L{0};", clearVRMessageClassName));
				// ClearVRMessage fields
				pIsSuccessFieldID = AndroidJNI.GetFieldID(pClearVRMessageClass, "isSuccess", "Z");
				pClearVRMessageMessageTypeFieldID = AndroidJNI.GetFieldID(pClearVRMessageClass, "messageType", String.Format("L{0};", clearVRMessageTypesClassName));
				pClearVRMessageCodeFieldID = AndroidJNI.GetFieldID(pClearVRMessageClass, "code", "I"); // Code is just an integer, not an enum
				pClearVRMessageMessageFieldID = AndroidJNI.GetFieldID(pClearVRMessageClass, "message", "Ljava/lang/String;");

				// Now we can disect ClearVRAsyncRequest
				// The requestType field is an enum so we need to access it as if it were a class
				pClearVRAsyncRequestRequestTypeFieldID = AndroidJNI.GetFieldID(pClearVRAsyncRequestClass, "requestType", String.Format("L{0};", clearVRRequestTypesClassName));
				// requestId
				pClearVRAsyncRequestRequestIdFieldID = AndroidJNI.GetFieldID(pClearVRAsyncRequestClass, "requestId", "I");

				// Get InitializeParameters constructor, this thing takes a lot of arguments...
				clearVRInitializeParametersClassConstructorMethodId = AndroidJNI.GetMethodID(clearVRInitializeParametersRawClassGlobalRef, "<init>", String.Format("([BLandroid/view/Surface;SSSJLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V"));

				// Cleanup, release local refs.
				AndroidJNI.DeleteLocalRef(pClearVRAsyncRequestResponseClass);
				AndroidJNI.DeleteLocalRef(pClearVRAsyncRequestClass);
				AndroidJNI.DeleteLocalRef(pClearVRMessageClass);
				AndroidJNI.DeleteLocalRef(pClearVRAsyncRequestTypesClass);
				AndroidJNI.DeleteLocalRef(pClearVRMessageTypesClass);
				AndroidJNI.DeleteLocalRef(pClearVREventTypesClass);
				AndroidJNI.DeleteLocalRef(clearVRInitializeParametersClass);
			}

			/// <summary>
			/// Constructs an InitializeParameters object and returns it as a jvalue.
			/// </summary>
			public jvalue[] GetInitializeParameters(PlatformOptionsBase argPlatformOptions, IntPtr argAndroidSurface) {
				IntPtr licenseFileBytesAsPointer = AndroidJNI.ToByteArray(argPlatformOptions.licenseFileBytes);
				
				jvalue[] constructorParams = new jvalue[14];
				constructorParams[0] = new jvalue();
				constructorParams[0].l = licenseFileBytesAsPointer; // License file byte array
				constructorParams[1] = new jvalue();
				constructorParams[1].l = argAndroidSurface; // Surface instance
				constructorParams[2] = new jvalue();
				constructorParams[2].s = argPlatformOptions.screenWidth; // the width of the screen, in pixels
				constructorParams[3] = new jvalue();
				constructorParams[3].s = argPlatformOptions.screenHeight; // the height of the screen, in pixels
				constructorParams[4] = new jvalue();
				constructorParams[4].s = (short) argPlatformOptions.deviceType; // the device type (0 = flat, 1 = generic HMD)
				constructorParams[5] = new jvalue();
				constructorParams[5].j = (long) argPlatformOptions.initializeFlags; // the initialize flags
				constructorParams[6] = new jvalue();
				constructorParams[6].l = AndroidJNI.NewStringUTF(argPlatformOptions.httpProxyParameters.host); // http proxy host
				constructorParams[7] = new jvalue();
				constructorParams[7].i = argPlatformOptions.httpProxyParameters.port; // http proxy port
				constructorParams[8] = new jvalue();
				constructorParams[8].l = AndroidJNI.NewStringUTF(argPlatformOptions.httpProxyParameters.username); // http proxy host
				constructorParams[9] = new jvalue();
				constructorParams[9].l = AndroidJNI.NewStringUTF(argPlatformOptions.httpProxyParameters.password); // http proxy host
				constructorParams[10] = new jvalue();
				constructorParams[10].l = AndroidJNI.NewStringUTF(argPlatformOptions.httpsProxyParameters.host); // http proxy host
				constructorParams[11] = new jvalue();
				constructorParams[11].i = argPlatformOptions.httpsProxyParameters.port; // http proxy port
				constructorParams[12] = new jvalue();
				constructorParams[12].l = AndroidJNI.NewStringUTF(argPlatformOptions.httpsProxyParameters.username); // http proxy host
				constructorParams[13] = new jvalue();
				constructorParams[13].l = AndroidJNI.NewStringUTF(argPlatformOptions.httpsProxyParameters.password); // http proxy host
				// Construct object
				IntPtr intializeParameters = AndroidJNI.NewObject(clearVRInitializeParametersRawClassGlobalRef, clearVRInitializeParametersClassConstructorMethodId, constructorParams);
				// Create a global ref
				initializeParametersGlobalRef = AndroidJNI.NewGlobalRef(intializeParameters);
				AndroidJNI.DeleteLocalRef(intializeParameters);
				AndroidJNI.DeleteLocalRef(licenseFileBytesAsPointer);
				jvalue[] returnJValue = new jvalue[1];
				returnJValue[0] = new jvalue();
				returnJValue[0].l = initializeParametersGlobalRef;
				return returnJValue;
			}
			
			public ClearVRAsyncRequest ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(IntPtr argClearVRAsyncRequestRawObject) {
				IntPtr clearVRRequestTypesObject = AndroidJNI.GetObjectField(argClearVRAsyncRequestRawObject, pClearVRAsyncRequestRequestTypeFieldID);
				return new ClearVRAsyncRequest(
					AndroidJNI.CallIntMethod(
						clearVRRequestTypesObject, 
						pClearVRRequestTypesGetValueMethodID,
						emptyJvalue
					),
					AndroidJNI.GetIntField(
						argClearVRAsyncRequestRawObject, 
						pClearVRAsyncRequestRequestIdFieldID
					)
				);
			}

			public ClearVRAsyncRequestResponse GetClearVRAsyncRequestResponseFromClearVRAsyncRequestResponseRawObject(IntPtr argClearVRAsyncRequestResponseRawObject) { 		
				IntPtr clearVRRequestTypesObject = AndroidJNI.GetObjectField(argClearVRAsyncRequestResponseRawObject, pClearVRAsyncRequestResponseRequestTypeFieldID);
				int requestType = AndroidJNI.CallIntMethod(
						clearVRRequestTypesObject, 
						pClearVRRequestTypesGetValueMethodID,
						emptyJvalue
					);

				int requestId = AndroidJNI.GetIntField(argClearVRAsyncRequestResponseRawObject, pClearVRAsyncRequestResponseRequestIdFieldID);
				return new ClearVRAsyncRequestResponse((RequestTypes) requestType, requestId);
			}

			public ClearVRMessage GetClearVRMessageFromClearVRAsyncRequestResponseRawObject(IntPtr argClearVRAsyncRequestResponseRawObject) { 
				IntPtr clearVRMessageObject = AndroidJNI.GetObjectField(argClearVRAsyncRequestResponseRawObject, pClearVRAsyncRequestResponseMessageFieldID);
				return GetClearVRMessageFromRawJavaObject(clearVRMessageObject);
			}

			public ClearVRMessage GetClearVRMessageFromRawJavaObject(IntPtr argClearVRMessageRawObject) { 
				IntPtr clearVRMessageTypeRawObject = AndroidJNI.GetObjectField(argClearVRMessageRawObject, pClearVRMessageMessageTypeFieldID);
				int messageType = AndroidJNI.CallIntMethod(clearVRMessageTypeRawObject, pClearVRMessageTypesGetValueMethodID, emptyJvalue);
				
				
				int messageCode = AndroidJNI.GetIntField(argClearVRMessageRawObject, pClearVRMessageCodeFieldID);
				String message = AndroidJNI.GetStringField(argClearVRMessageRawObject, pClearVRMessageMessageFieldID);
				bool isSuccess = AndroidJNI.GetBooleanField(argClearVRMessageRawObject, pIsSuccessFieldID);
				return new ClearVRMessage(messageType, 
						messageCode, 
						message, 
						ClearVRMessage.ConvertBooleanToClearVRResult(isSuccess));				
			}

			public EventTypes GetEventTypeFromRawObject(IntPtr argEventTypeRawObject) {
				String eventType = AndroidJNI.CallStringMethod(argEventTypeRawObject, pClearVREventTypesGetValueMethodID, emptyJvalue);
				try {
					switch(eventType) {
						case "vod":
							return EventTypes.Vod;
						case "live":
							return EventTypes.Live;
						default:
							throw new Exception(String.Format("Unsupported event type: {0}", eventType));
					}
				} catch (Exception e) {
					UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An error was thrown while getting the event type. Details: {0}.", e));
				}
				return EventTypes.Unknown;
			}
			public void Release() {
				// Do not release these GlobalRefs for now as that results in a crash.
				// Note that we never freed them in the first place, but this is a TODO
				// if(clearVRInitializeParametersRawClassGlobalRef != IntPtr.Zero) {
				// 	AndroidJNI.DeleteGlobalRef(clearVRInitializeParametersRawClassGlobalRef);
				// 	clearVRInitializeParametersRawClassGlobalRef = IntPtr.Zero;
				// }
				// if(initializeParametersGlobalRef != IntPtr.Zero) {
				// 	AndroidJNI.DeleteGlobalRef(initializeParametersGlobalRef);
				// }
				// initializeParametersGlobalRef = IntPtr.Zero;
				// if(clearVRInitializeParametersClassConstructorMethodId != IntPtr.Zero) {
				// 	AndroidJNI.DeleteGlobalRef(clearVRInitializeParametersClassConstructorMethodId);
				// }
				// clearVRInitializeParametersClassConstructorMethodId = IntPtr.Zero;
			}
		}
		/*
		This class implements com.tiledmedia.clearvrcorewrapper.ClearVRCoreWrapperExternalInterface java interface as an AndroidJavaProxy
		 */
		private class ClearVRCoreWrapperExternalInterface : AndroidJavaProxy {
			/* The underlying SDK makes sure that this callback is always invoked from the main thread. Otherwise, random SEGFAULTS are to be expected! */
			private MediaPlayerAndroid mediaPlayer;
			public ClearVRCoreWrapperExternalInterface(MediaPlayerAndroid argMediaPlayer) : base("com.tiledmedia.clearvrcorewrapper.ClearVRCoreWrapperExternalInterface") {
				mediaPlayer = argMediaPlayer;
			}

			public void cbClearVRCoreWrapperRequestCompleted(AndroidJavaObject argClearVRAsyncRequestResponseJavaObject) {
				IntPtr clearVRAsyncRequestResponseRawObject = argClearVRAsyncRequestResponseJavaObject.GetRawObject();				
				mediaPlayer.CbClearVRCoreWrapperRequestCompleted(mediaPlayer.clearVRAndroidJNIConverter.GetClearVRAsyncRequestResponseFromClearVRAsyncRequestResponseRawObject(clearVRAsyncRequestResponseRawObject), 
					mediaPlayer.clearVRAndroidJNIConverter.GetClearVRMessageFromClearVRAsyncRequestResponseRawObject(clearVRAsyncRequestResponseRawObject));
			}

			public void cbClearVRCoreWrapperMessage(AndroidJavaObject argClearVRMessage) {
				ClearVRMessage clearVRMessage = mediaPlayer.clearVRAndroidJNIConverter.GetClearVRMessageFromRawJavaObject(argClearVRMessage.GetRawObject());
				mediaPlayer.CbClearVRCoreWrapperMessage(clearVRMessage);
			}
		}

		[DllImport ("ClearVRNativeRendererPlugin")]
		public static extern IntPtr CVR_NRP_GetRenderEventFunc();

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern void CVR_NRP_Load();

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern IntPtr CVR_NRP_GetSurfaceObject();

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern void CVR_NRP_SetOESFastPathEnabled(bool argValue);

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern void CVR_NRP_SetClearVRCoreWrapper(IntPtr argObject);

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern long CVR_NRP_GetNumberOfFramesReadyOnSurface();

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern long CVR_NRP_GetNumberOfFramesInSurfaceTextureQueueReleased();

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern void CVR_NRP_SetMeshBuffersFromUnity(IntPtr argVertexBuffer,
			IntPtr argIndexBuffer,
			int argSignature);

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern void CVR_NRP_SetSphereRadius(float argSphereRadius);

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern void CVR_NRP_SetCallbackMeshCreated(CbMeshCreatedDelegate argCbMeshCreatedDelegate);

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern void CVR_NRP_SetCallbackMeshTextureAvailable(CbMeshTextureAvailableDelegate argCbMeshTextureAvailableDelegate);

		[DllImport("ClearVRNativeRendererPlugin")]
		public static extern void CVR_NRP_SetCallbackMeshDestroyed(CbMeshDestroyedDelegate argCbMeshDestroyedDelegate);

		public enum ClearVRNativeRendererPluginEvent {
			Unknown = 0,
			Initialize = 1,
			Update = 2,
			Destroy = 999
		}

		public static void IssuePluginEvent(ClearVRNativeRendererPluginEvent argType, long optionalOffset = 0) {
			if(CVRNRPRenderEventFunctionPointer.Equals(IntPtr.Zero)) {
				throw new Exception("[ClearVR] You have to call GetRenderEventFunc() in Start() first!");
			}
			GL.InvalidateState(); // Essential for proper operation in OpenGLES 3.x
			GL.IssuePluginEvent(CVRNRPRenderEventFunctionPointer, (int) argType + (int) optionalOffset);
		}

		public static IntPtr CVRNRPRenderEventFunctionPointer = IntPtr.Zero;

		private jvalue[] emptyJvalue = new jvalue[0];
		/* Global raw class reference. Only AndroidJNI.DeleteGlobalRef() this pointer when you are no longer calling any native library method */
		protected IntPtr clearVRCoreWrapperRawClassGlobalRef = IntPtr.Zero;
		/* Global raw object reference. Only AndroidJNI.DeleteGlobalRef() this pointer when you are no longer calling any native library method */
		protected IntPtr clearVRCoreWrapperGlobalRef = IntPtr.Zero;
		/* Global raw object reference of the current application context. */
		protected IntPtr applicationContextRawObjectGlobalRef = IntPtr.Zero;

		const string clearVRCoreWrapperClassName = "com/tiledmedia/clearvrcorewrapper/ClearVRCoreWrapper";

		// These values are only set when running in GearVR or Oculus Go
		private Vector3 ovrCameraAngularAcceleration;
		private Vector3 ovrCameraAngularVelocity;

		/* class private pointers to the various methods that are exposed in the ClearVRCoreWrapper class */
		private IntPtr prepareContentForPlayoutMethodId = IntPtr.Zero;
		private IntPtr getParameterMethodId = IntPtr.Zero;
		private IntPtr getArrayParameterSafelyMethodId = IntPtr.Zero;
		private IntPtr setParameterMethodId = IntPtr.Zero;
		private IntPtr startPlayoutMethodId = IntPtr.Zero;
		private IntPtr pauseMethodId = IntPtr.Zero;
		private IntPtr unpauseMethodId = IntPtr.Zero;
		private IntPtr parseMediaInfoMethodId = IntPtr.Zero;
		private IntPtr stopClearVRCoreMethodId = IntPtr.Zero;
		private IntPtr sendSensorDataMethodId = IntPtr.Zero;
		private IntPtr getAverageBitrateInKbpsMethodId = IntPtr.Zero;
		private IntPtr muteAudioMethodId = IntPtr.Zero;
		private IntPtr unmuteAudioMethodId = IntPtr.Zero;
		private IntPtr setAudioGainMethodId = IntPtr.Zero;
		private IntPtr getAudioGainMethodId = IntPtr.Zero;
		private IntPtr getIsAudioMutedMethodId = IntPtr.Zero;
		private IntPtr getAudioTrackMethodId = IntPtr.Zero;
		private IntPtr setAudioTrackAndAudioDecoderAndAudioPlaybackEngineMethodId = IntPtr.Zero;
		private IntPtr seekMethodId = IntPtr.Zero;
		private IntPtr prewarmCacheMethodId = IntPtr.Zero;
		private IntPtr getContentDurationMethodId = IntPtr.Zero;
		private IntPtr getCurrentContentTimeMethodId = IntPtr.Zero;
		private IntPtr getHighestQualityResolutionAndFramerateMethodId = IntPtr.Zero;
		private IntPtr getNumberOfAudioTracksMethodId = IntPtr.Zero;
		private IntPtr switchContentMethodId = IntPtr.Zero;
		private IntPtr setStereoscopicModeMethodId = IntPtr.Zero;
		private IntPtr getEventTypeMethodId = IntPtr.Zero;
		private IntPtr getDeviceAppIdMethodId = IntPtr.Zero;
		private readonly float RAD_TO_DEG = (float) (180F / Math.PI);

		protected IntPtr clearVRCoreWrapperExternalInterfaceJavaProxyGlobalRef = IntPtr.Zero;
		private jvalue[] sendSensorDataParams = new jvalue[9]; // yaw, pitch, roll (absolute, angular velocity and angular acceleration)
		private ClearVRAndroidJNIConverter clearVRAndroidJNIConverter;
		/* We keep track of angular velocity and acceleration when running on the GearVR and Oculus GO */
		protected object ovrDisplay = null;
	    protected delegate Vector3 AngularVelocityDelegate(); // takes the signature of the OVRDisplay.angularVelocity getter
	    protected delegate Vector3 AngularAccelerationDelegate(); // takes the signature of the OVRDisplay.angularAcceleration getter
	    protected AngularVelocityDelegate angularVelocity;
	    protected AngularAccelerationDelegate angularAcceleration;
		/* Dummy load a simple function from the OVRPlugin. This is used lateron to verify whether the OVRPlugin is at all available or not. */
		[DllImport("OVRPlugin", CallingConvention = CallingConvention.Cdecl)]
		public static extern bool ovrp_GetInitialized();

		/// <summary>
		/// Class constructor
		/// </summary>
		/// <param name="argPlatformOptions">The options to use when constructing the class</param>
		/// <param name="argCbClearVREvent">The app-level ClearVREvent callback to trigger.</param>
		public MediaPlayerAndroid(PlatformOptionsBase argPlatformOptions, UnityEngine.Events.UnityAction<MediaPlayerBase,ClearVREvent> argCbClearVREvent) : base(argPlatformOptions, argCbClearVREvent) {
			SetPlatformOptions(argPlatformOptions);
			if(!LoadShader()){
				// error has been handled in LoadShader().
				return;
			}
			DoOVRPluginReflection();
			if(!InitializePlatformBindings()) {
				ScheduleClearVREvent(new ClearVREvent(ClearVREventTypes.UnableToInitializePlayer, ClearVRMessageTypes.FatalError, ClearVRMessageCodes.GenericFatalError, "An error occured while initializing Android-specific platform bindings. Cannot continue.", ClearVRResult.Failure));
				return;
			}
			if(_state != States.Initializing) {
				throw new Exception("[ClearVR] You must call InitializePlatformBindings() before you can instantiate this class.");
			}
			SetState(States.Initialized, ClearVRMessage.GetGenericOKMessage());
		}

		/// <summary>
		/// Enable or disable sustained performance mode on Android 7.0+
		/// </summary>
		/// <param name="argEnabled">true to enable sustained performance mode, false to disable it.</param>
		private static void SetSustainedPerformanceMode(bool argEnabled) {
			AndroidJavaObject androidActivity = null;
			try {
				AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				androidActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
			} catch (AndroidJavaException e) {
				UnityEngine.Debug.LogError("Exception while connecting to the Activity: " + e);
				return;
			}
			AndroidJavaObject androidWindow = androidActivity.Call<AndroidJavaObject>("getWindow");
			if (androidWindow == null) {
				UnityEngine.Debug.LogError("No window found on the current android activity");
				return;
			}
			try {
				androidActivity.Call("runOnUiThread", new AndroidJavaRunnable(() => {
						androidWindow.Call("setSustainedPerformanceMode", argEnabled);
					})
				);
			} catch (Exception argException) {
				UnityEngine.Debug.LogError(String.Format("[ClearVR] Cannot set sustained performance mode. Not supported? Error: {0}", argException));
			}
		}

		
		/// <summary>
		/// Uses reflection to get the angularVelocity and angularAcceleration fields from the OVRDisplay class if available.
		/// </summary>
		protected void DoOVRPluginReflection() {
			#pragma warning disable 0618
			if(!_platformOptions.deviceType.GetIsAndroidOculusDevice() || !_platformOptions.useHeadsetAccelerationAndVelocityIfAvailable) {
				return;
			}
			#pragma warning restore 0618
			Type ovrDisplayType = Utils.GetType("OVRDisplay");
			if(ovrDisplayType != null) {
				bool isOVRPluginLibraryFound = false;
				try {
					ovrp_GetInitialized();
					isOVRPluginLibraryFound = true;
				} catch (DllNotFoundException) {
					// OVR source code is there, but the OVR plugin cannot be loaded.
				}
				if(isOVRPluginLibraryFound) {
					try {
						ovrDisplay = Activator.CreateInstance(ovrDisplayType);
						PropertyInfo angularVelocityPropertyInfo = ovrDisplayType.GetProperty("angularVelocity");
						PropertyInfo angularAccelerationPropertyInfo = ovrDisplayType.GetProperty("angularAcceleration");
						if(angularVelocityPropertyInfo == null || angularAccelerationPropertyInfo == null){
							ovrDisplay = null; // Cannot track acceleration and/or velocity. Old OVR plugin?
						} else {
							MethodInfo angularVelocityMethodInfo = angularVelocityPropertyInfo.GetGetMethod();
							MethodInfo angularAccelerationMethodInfo = angularAccelerationPropertyInfo.GetGetMethod();
							if(angularVelocityMethodInfo == null || angularAccelerationMethodInfo == null){
								ovrDisplay = null; // Cannot track acceleration and/or velocity. Old OVR plugin?
							} else {
								// We use delegates so that we do not incure the penalty of introspection every time we call this method.
								angularVelocity = (AngularVelocityDelegate) Delegate.CreateDelegate(typeof(AngularVelocityDelegate), ovrDisplay, angularVelocityMethodInfo);
								angularAcceleration = (AngularAccelerationDelegate) Delegate.CreateDelegate(typeof(AngularAccelerationDelegate), ovrDisplay, angularAccelerationMethodInfo);
							}
						}
					} catch { // explicit fallthrough as we do not care for the exception in this case
						ovrDisplay = null;
					}
				} else {
					// no OVRPlugin.so present in application
				}
			}
		}
		private bool InitializePlatformBindings() {
			SetState(States.Initializing, ClearVRMessage.GetGenericOKMessage());
			AndroidJNIHelper.debug = false;
			GetAndroidActivity();
			// This is called to merely load the native plugin as soon as possible. It is doing *nothing* under the hood
			// but it is essential to call this (at least once) in your code.
			CVR_NRP_Load();
			// Next, we register our callbacks *before* we initialize the plugin on the GL render thread through IssuePluginEvent()
			CVR_NRP_SetCallbackMeshCreated(new CbMeshCreatedDelegate(CbMeshCreated));
			CVR_NRP_SetCallbackMeshDestroyed(new CbMeshDestroyedDelegate(CbMeshDestroyed));
			CVR_NRP_SetCallbackMeshTextureAvailable(new CbMeshTextureAvailableDelegate(CbMeshTextureAvailable));
			CVR_NRP_SetSphereRadius(_platformOptions.sphereRadius);
			CVRNRPRenderEventFunctionPointer = CVR_NRP_GetRenderEventFunc();
			// Initialize the native plugin.
			// Note that this asynchronous call can take one or two frames to complete.
			// Must enable OES Fast Path before calling initialize on the rendering thread. By default, it will be disabled.
			CVR_NRP_SetOESFastPathEnabled(_isUseOESFastPathEnabled);
			IssuePluginEvent(ClearVRNativeRendererPluginEvent.Initialize);
			for(int i = 0; i < sendSensorDataParams.Length; i++) {
				sendSensorDataParams[i] = new jvalue();
				sendSensorDataParams[i].f = 0f; // default value when values are not available (0...2 = yaw/pitch/roll, 3...5 = angular velocity, 6...8 = angular acceleration)
			}
			return true;
		}

		private void GetAndroidActivity() {
			AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject currentActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaObject applicationContext = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
			/* Find raw class */
			IntPtr clearVRCoreWrapperClass = AndroidJNI.FindClass(clearVRCoreWrapperClassName); // Must be released as it returns a LocalRef
			/* Store reference as GlobalRef for safe-keeping */
			clearVRCoreWrapperRawClassGlobalRef = AndroidJNI.NewGlobalRef(clearVRCoreWrapperClass);
			AndroidJNI.DeleteLocalRef(clearVRCoreWrapperClass); // delete local ref
			applicationContextRawObjectGlobalRef = AndroidJNI.NewGlobalRef(applicationContext.GetRawObject());
			GetClearVRCoreVersion();
		}

		/// <summary>
		/// Apply platform specific options
		/// </summary>
		/// <param name="argPlatformOptions"></param>
		private void SetPlatformOptions(PlatformOptionsBase argPlatformOptions) {
			_platformOptions = argPlatformOptions;
			_platformOptions.platform = RuntimePlatform.Android;
			if(_platformOptions.audioPlaybackEngine == null) {
				_platformOptions.audioPlaybackEngine = AudioPlaybackEngine.GetDefaultAudioPlaybackEngineForPlatform(_platformOptions.platform);
			}
			if(_platformOptions.audioDecoder == null) {
				_platformOptions.audioDecoder = AudioDecoder.GetDefaultAudioDecoderForPlatform(_platformOptions.platform);
			}
#if UNITY_5_6_0 || UNITY_5_6_1
				if(_isUseOESFastPathEnabled)
					throw new Exception("[ClearVR] Unity 5.6.0 and 5.6.1 do not support the Fast OES rendering path, see Unity bug #899502 for details. Please switch to a newer version of Unity, e.g. 2018.3.0f2.");
#endif
		}

		/// <summary>
		/// Called to prepare the underlying libraries for playout.
		/// This will yield a single frame that is used to construct the ClearVRMesh.
		/// </summary>
		public override ClearVRAsyncRequest _PrepareCore(bool argIsCalledAsynchronous) {
			if(!_state.Equals(States.Initialized))
				return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot prepare core when in {0} state.", GetCurrentStateAsString()), false, RequestTypes.Initialize);
			SetState(States.PreparingCore, ClearVRMessage.GetGenericOKMessage());
			/* Get Android Surface object pointer from native plugin */
			IntPtr androidSurface = CVR_NRP_GetSurfaceObject();
			clearVRAndroidJNIConverter = new ClearVRAndroidJNIConverter();
			ClearVRCoreWrapperExternalInterface clearVRCoreWrapperExternalInterface = new ClearVRCoreWrapperExternalInterface(this);

			clearVRCoreWrapperExternalInterfaceJavaProxyGlobalRef = AndroidJNI.NewGlobalRef(AndroidJNIHelper.CreateJavaProxy(clearVRCoreWrapperExternalInterface));
			/* Find proper constructor */
			IntPtr classConstructorMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "<init>", "(Lcom/tiledmedia/clearvrcorewrapper/ClearVRCoreWrapperExternalInterface;Landroid/content/Context;Ljava/lang/String;)V");
			IntPtr deviceAppIdAsPointer = AndroidJNI.NewStringUTF(""); // Empty string means: generate one for me
			jvalue[] constructorParams = new jvalue[3];
			constructorParams[0] = new jvalue();
			constructorParams[0].l = clearVRCoreWrapperExternalInterfaceJavaProxyGlobalRef; // the callback interface
			constructorParams[1] = new jvalue();
			constructorParams[1].l = applicationContextRawObjectGlobalRef; // application context
			constructorParams[2] = new jvalue();
			constructorParams[2].l = deviceAppIdAsPointer; // device app id (empty string)

			/*
			After initialize() it will take some time before the internal library is fully initialized. Wait for the
			cbClearVRCoreWrapperInitialized() callback to know when this process has completed. After that,
			it is safe to load content and hit play! Note that initialize() will return true if initial setup went according to plan.
			You still have to wait for the callback!
			*/
			ClearVRAsyncRequest clearVRAsyncRequest;
			try {
				/* Initialize the ClearVRCoreWrapper class which extends the Thread class */
				IntPtr clearVRCoreWrapper = AndroidJNI.NewObject(clearVRCoreWrapperRawClassGlobalRef, classConstructorMethodId, constructorParams);
				clearVRCoreWrapperGlobalRef = AndroidJNI.NewGlobalRef(clearVRCoreWrapper);
				AndroidJNI.DeleteLocalRef(clearVRCoreWrapper);
				/* Start the ClearVRCoreWrapper thread */
				IntPtr initializeMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef,"initialize", String.Format("(L{0};)L{1};", ClearVRAndroidJNIConverter.clearVRInitializeParametersClassName, ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));
				IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, initializeMethodId, clearVRAndroidJNIConverter.GetInitializeParameters(_platformOptions, androidSurface));
				clearVRAsyncRequest = clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);

			} catch (Exception argException) {
				throw new Exception(String.Format("[ClearVR] Unable to initialize ClearVR! Error: {0}", argException));
			}
			AndroidJNI.DeleteLocalRef(deviceAppIdAsPointer);
			// Get a reference to methods exposed by our library
			prepareContentForPlayoutMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef,"prepareContentForPlayout", String.Format("(Ljava/lang/String;FFJIII)L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));
			sendSensorDataMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "sendSensorData", "(FFFFFFFFF)V");
			getParameterMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef,"getParameterSafely", "(Ljava/lang/String;)Ljava/lang/String;");
			getArrayParameterSafelyMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef,"getArrayParameterSafely", "(Ljava/lang/String;I)Ljava/lang/String;");
			setParameterMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef,"setParameterSafely", "(Ljava/lang/String;Ljava/lang/String;)Z");
			startPlayoutMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef,"startPlayout", String.Format("()L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));

			pauseMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "pause", String.Format("()L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));
			unpauseMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "unpause", String.Format("()L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));

			muteAudioMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "muteAudio", "()Z");
			unmuteAudioMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "unmuteAudio", "()Z");
			setAudioGainMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "setAudioGain", "(F)V");
			getAudioGainMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "getAudioGain", "()F");
			getIsAudioMutedMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "getIsAudioMuted", "()Z");
			getAudioTrackMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "getAudioTrack", "()I");
			setAudioTrackAndAudioDecoderAndAudioPlaybackEngineMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "setAudioTrack", String.Format("(III)L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));

			stopClearVRCoreMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "stopClearVRCore", String.Format("()L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));
			parseMediaInfoMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "parseMediaInfo", String.Format("(Ljava/lang/String;)L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));
			getAverageBitrateInKbpsMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "getAverageBitrateInKbps", "()I");
			seekMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "seek", String.Format("(J)L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));
			prewarmCacheMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "prewarmCache", String.Format("(Ljava/lang/String;IJ)L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));
			getCurrentContentTimeMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "getCurrentContentTime", "()J");
			getContentDurationMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "getContentDuration", "()J");
			getHighestQualityResolutionAndFramerateMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "getHighestQualityResolutionAndFramerate", "()Ljava/lang/String;");
			getEventTypeMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "getEventType", String.Format("()L{0};", ClearVRAndroidJNIConverter.clearVREventTypesClassName));
			getDeviceAppIdMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef,"getDeviceAppId", "()Ljava/lang/String;");
			getNumberOfAudioTracksMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "getNumberOfAudioTracks", "()I");
			switchContentMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "switchContent", String.Format("(Ljava/lang/String;FFJIIIJ)L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));
			setStereoscopicModeMethodId = AndroidJNI.GetMethodID(clearVRCoreWrapperRawClassGlobalRef, "setStereoscopicMode", String.Format("(Z)L{0};", ClearVRAndroidJNIConverter.clearVRAsyncRequestClassName));
			/* Initialize statistics */
			_clearVRCoreWrapperStatistics = new StatisticsAndroid(clearVRCoreWrapperRawClassGlobalRef, clearVRCoreWrapperGlobalRef);
			return clearVRAsyncRequest;
		}

		public static string GetClearVRCoreVersion() {
			if(_clearVRCoreVersionString.Equals("Unknown")) {
				/* Find raw class */
				IntPtr clearVRCoreWrapperClass = AndroidJNI.FindClass(clearVRCoreWrapperClassName); // Must be released as it returns a LocalRef
				// Note that static methods MUST be retrieved from an non-instantiated (raw) class
				IntPtr getVersionMethodId = AndroidJNI.GetStaticMethodID(clearVRCoreWrapperClass, "getClearVRCoreVersion", "()Ljava/lang/String;");
				/* Get ClearVRCore version and store it for safe-keeping */
				/* We MUST call this on the raw class and not on the instantiated class */
				_clearVRCoreVersionString = AndroidJNI.CallStaticStringMethod(clearVRCoreWrapperClass, getVersionMethodId, new jvalue[0]);
				/* By now we can release the local ref */
				AndroidJNI.DeleteLocalRef(clearVRCoreWrapperClass);
			}
			return _clearVRCoreVersionString;
		}

		public override void Render() {
			/* Intentionally left blank for now */
		}

		public override void Update() {
   		    base.UpdateClearVREventsToInvokeQueue();
		}

		public override void LateUpdate() {
			/* sending sensor data is only allowed after the first frame has been rendered. */
			if(!isFirstFrame) {
				if(_isViewportTrackingEnabled) {
					SendSensorInfo();
				}
				IssuePluginEvent(ClearVRNativeRendererPluginEvent.Update);
			}
		}

        public override ClearVRMeshBase ConstructClearVRMesh(MeshCreatedStruct argMeshCreatedStruct) {
            ClearVRMeshBase clearVRMesh = base.ConstructClearVRMesh(argMeshCreatedStruct);
            CVR_NRP_SetMeshBuffersFromUnity(clearVRMesh.mesh.GetNativeVertexBufferPtr(0),
                                 clearVRMesh.mesh.GetNativeIndexBufferPtr(),
                                 argMeshCreatedStruct.signature);
            return clearVRMesh;
        }

		public override void SendSensorInfo() {
			UpdateOrientationInfo();
			sendSensorDataParams[0].f = orientationData[0];
			sendSensorDataParams[1].f = orientationData[1];
			sendSensorDataParams[2].f = orientationData[2];
			if(ovrDisplay != null) {
				ovrCameraAngularVelocity = angularVelocity();
				ovrCameraAngularAcceleration = angularAcceleration();
				sendSensorDataParams[3].f = ovrCameraAngularVelocity.y * RAD_TO_DEG;
				sendSensorDataParams[4].f = ovrCameraAngularVelocity.x * RAD_TO_DEG;
				sendSensorDataParams[5].f = ovrCameraAngularVelocity.z * RAD_TO_DEG;
				sendSensorDataParams[6].f = ovrCameraAngularAcceleration.y * RAD_TO_DEG;
				sendSensorDataParams[7].f = ovrCameraAngularAcceleration.x * RAD_TO_DEG;
				sendSensorDataParams[8].f = ovrCameraAngularAcceleration.z * RAD_TO_DEG;
			}
			AndroidJNI.CallVoidMethod(clearVRCoreWrapperGlobalRef, sendSensorDataMethodId, sendSensorDataParams);
		}

		/// <summary>
		/// Query the content duration. Can only be queried after at least parseMediaInfo() returned.
		/// </summary>
		/// <returns>The content duration in milliseconds</returns>
		public override long GetContentDurationInMilliseconds() {
			long contentDurationInMilliseconds = 0;
			if(GetIsPlayerBusy()) {
				if(GetIsMediaInfoParsed()) {
					try {
						contentDurationInMilliseconds = (long) AndroidJNI.CallLongMethod(clearVRCoreWrapperGlobalRef, getContentDurationMethodId, emptyJvalue);
					} catch (Exception e) {
						UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An error was thrown while getting content duration. Details: {0}.", e));
					}
				}
			}
			return contentDurationInMilliseconds;
		}

		/// <summary>
		/// Convenience method to return the number of audio tracks.
		/// </summary>
		/// <returns>The number of audio tracks, or -1 if this information is not (yet) available.</returns>
		public override int GetNumberOfAudioTracks() {
			int numberOfAudioTracks = -1;
			if(GetIsPlayerBusy()) {
				if(GetIsMediaInfoParsed()) {
					try {
						numberOfAudioTracks = AndroidJNI.CallIntMethod(clearVRCoreWrapperGlobalRef, getNumberOfAudioTracksMethodId, emptyJvalue);
					} catch (Exception e) {
						UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An error was thrown while getting number of audio tracks. Details: {0}.", e));
					}
				}
			}
			return numberOfAudioTracks;
		}


		/// <summary>
		/// Actually load and start playout of the content item
		/// </summary>
		/// <param name="argContentItem">The content item to prepare</param>
		public override ClearVRAsyncRequest _PrepareContentForPlayout(ContentItem argContentItem, bool argIsCalledAsynchronous) {
			if(_state != States.CorePrepared) {
				return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot prepare content for playout when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.PrepareContentForPlayout);
			}
			/* Media info must have been parsed prior to preparing content for playout */
			if(!GetIsMediaInfoParsed()) {
				return InvokeClearVRCoreWrapperInvalidStateEvent("Cannot prepare content for playout in current state. Parse media info first!", argIsCalledAsynchronous, RequestTypes.PrepareContentForPlayout);
			}
			contentItem = argContentItem;
			SetState(States.PreparingContentForPlayout, ClearVRMessage.GetGenericOKMessage());
			SetLoopContent(_platformOptions.loopContent);

			if(_platformOptions.clearVRCoreLogToFile != "") {
				SetClearVRCoreParameter("config.verbosity", _platformOptions.clearVRCoreVerbosity.ToString());
				SetClearVRCoreParameter("config.filelog", _platformOptions.clearVRCoreLogToFile);
			}
			if(!_platformOptions.isVRDevicePresent) {
				SetClearVRCoreParameter("config.force_mono", "true");
				SetRenderMode(RenderModes.Monoscopic);
			}
			UpdateOrientationInfo();
			float yaw = (float) orientationData[0] + contentItem.startYaw;
			float pitch = (float) orientationData[1] + argContentItem.startPitch;
			jvalue[] parms = new jvalue[7];
			parms[0] = new jvalue(); // manifest url (string)
			parms[0].l = AndroidJNI.NewStringUTF(contentItem.manifestUrl);
			parms[1] = new jvalue();
			parms[1].f = yaw; // start yaw, can be fixed or (better) the actual direction the user is looking at (float)
			parms[2] = new jvalue();
			parms[2].f = pitch; // start pitch (float)
			parms[3] = new jvalue();
			parms[3].j = contentItem.startPositionInMilliseconds; // start position in content in milliseconds (long)
			parms[4] = new jvalue();
			parms[4].i = (int) _platformOptions.audioDecoder.audioDecoderType; // audio decoder type (int)
			parms[5] = new jvalue();
			parms[5].i = (int) _platformOptions.audioPlaybackEngine.audioPlaybackEngineType; // audio playback engine type (int)
			parms[6] = new jvalue();
			parms[6].i = (int) _platformOptions.prepareContentForPlayoutTimeoutInMilliseconds;
			/* This next call is entirely asynchronous */
			IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, prepareContentForPlayoutMethodId, parms);
			CVR_NRP_SetClearVRCoreWrapper(clearVRCoreWrapperGlobalRef);
			return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);			
		}

		/// <summary>
		/// This method is directly linked to ClearVRCoreWrapper.getParameter().
		/// See the ClearVRCore docs for details about valid keys and when they can be queried. Note that this will return an empty string in case you try to query a parameter
		/// when in an illegal state.
		/// </summary>
		/// <param name="argKey"></param> The key to query.
		/// <returns>The value of the queried key. </returns>
		public override String GetClearVRCoreParameter(String argKey) {
			if(!GetIsPlayerBusy())
				return "";
			jvalue[] parms = new jvalue[1];
			parms[0] = new jvalue();
			parms[0].l = AndroidJNI.NewStringUTF(argKey);
			string output = "";
			try {
				output = AndroidJNI.CallStringMethod(clearVRCoreWrapperGlobalRef, getParameterMethodId, parms);
			} catch (Exception e) { // catches JNI errors only.
				UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An exception was thrown while accessing key '{0}' from ClearVRCore. Error: {1}", argKey, e.ToString()));
			}
			return output;
		}

		public override String GetDeviceAppId() {
			if(!GetIsPlayerBusy())
				return "";
			return AndroidJNI.CallStringMethod(clearVRCoreWrapperGlobalRef, getDeviceAppIdMethodId, emptyJvalue);
		}

		/// <summary>
		/// This method is directly linked to ClearVRCoreWrapper.getParameter().
		/// See the ClearVRCore docs for details about valid keys and when they can be queried. Note that this will return an empty string in case you try to query a parameter
		/// when in an illegal state.
		/// </summary>
		/// <param name="argKey"></param> The key to query.
		/// <returns>The value of the queried key. </returns>
		public override String GetClearVRCoreArrayParameter(String argKey, int argIndex) {
			if(!GetIsPlayerBusy())
				return "";
			jvalue[] parms = new jvalue[2];
			parms[0] = new jvalue();
			parms[0].l = AndroidJNI.NewStringUTF(argKey);
			parms[1] = new jvalue();
			parms[1].i = argIndex;
			string output = "";
			try {
				output = AndroidJNI.CallStringMethod(clearVRCoreWrapperGlobalRef, getArrayParameterSafelyMethodId, parms);
			} catch (Exception e) { // catches JNI errors only.
				UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An exception was thrown while accessing key '{0}'[{1}] from ClearVRCore. Error: {2}", argKey, argIndex, e.ToString()));
			}
			return output;
		}


		/// <summary>
		///	This method is directly linked to ClearVRCoreWrapper.setParameter(). Return true is successful, false otherwise.
		/// See the ClearVRCore docs for details about valid keys and when they can be queried.
		/// </summary>
		/// <param name="argKey"> The key to set.</param>
		/// <param name="argValue">The value they key should be set to.</param>
		/// <returns>true if success, false otherwise.</returns>
		public override bool SetClearVRCoreParameter(String argKey, String argValue) {
			if(!GetIsPlayerBusy())
				return false;
			jvalue[] parms = new jvalue[2];
			parms[0] = new jvalue();
			parms[0].l = AndroidJNI.NewStringUTF(argKey);
			parms[1] = new jvalue();
			parms[1].l = AndroidJNI.NewStringUTF(argValue);
			bool isSuccess = false;
			try {
				isSuccess = AndroidJNI.CallBooleanMethod(clearVRCoreWrapperGlobalRef, setParameterMethodId, parms);
			} catch (Exception e) {
				UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An exception was thrown while setting key '{0}' to {1} on ClearVRCore. Error: {2}", argKey, argValue, e.ToString()));
			}
			return isSuccess;
		}

		/// <summary>
		/// Parse media info. This method is handled asynchronously since it involves network traffic and manifest parsing.
		/// You will have to wait for the ClearVREventTypes.MediaInfoParsed Event before it is safe to query
		/// audio/video parameters and select the audio track of you liking.
		/// </summary>
		/// <param name="argUrl"></param>
		public override ClearVRAsyncRequest _ParseMediaInfo(String argUrl, bool argIsCalledAsynchronous) {
			if(!GetIsPlayerBusy()) {
				return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot parse media info when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.ParseMediaInfo);
			}
			ClearVREvent clearVREvent = ClearVREvent.GetGenericOKEvent(ClearVREventTypes.ParsingMediaInfo);
			ScheduleClearVREvent(clearVREvent);
			jvalue[] parms = new jvalue[1];
			parms[0] = new jvalue();
			parms[0].l = AndroidJNI.NewStringUTF(argUrl);
			IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, parseMediaInfoMethodId, parms);
			return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);			
		}

		/*
		Get the current average bitrate in megabit per second. Notice that the lower level method returns this value in kilobit per second.
		*/
		public override float GetAverageBitrateInMbps() {
			if(!GetIsPlayerBusy())
				return 0f; // Cannot query this parameter in these states
			try {
				return (float) Math.Round((float)AndroidJNI.CallIntMethod(clearVRCoreWrapperGlobalRef, getAverageBitrateInKbpsMethodId, emptyJvalue) / 1024.0f, 1);
			} catch (Exception e) {
				UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An error was thrown while getting the average stream bitrate. Details: {0}.", e));
			}
			return 0.0f;
		}

		public override long GetCurrentContentTimeInMilliseconds() {
			if(!GetIsPlayerBusy())
				return 0;
			try {
				return (long) AndroidJNI.CallLongMethod(clearVRCoreWrapperGlobalRef, getCurrentContentTimeMethodId, emptyJvalue);
			} catch (Exception e) {
				UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An error was thrown while getting current content time. Details: {0}.", e));
			}
			return 0;
		}

		public override string GetHighestQualityResolutionAndFramerate() {
			try {
				return AndroidJNI.CallStringMethod(clearVRCoreWrapperGlobalRef, getHighestQualityResolutionAndFramerateMethodId, emptyJvalue);
			} catch (Exception e) {
				UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An error was thrown while getting highest quality content resolution. Details: {0}.", e));
			}
			return "";
		}

		public override string GetCurrentResolutionAndFramerate() {
			return GetClearVRCoreParameter("playback.current_quality");
		}

		public override ClearVRAsyncRequest _Seek(long argNewPositionInMilliseconds, bool argIsCalledAsynchronous) {
			if(!GetIsPlayerBusy()) {
				return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot seek when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.Seek);
			}
			jvalue[] parms = new jvalue[1];
			parms[0] = new jvalue();
			parms[0].j = argNewPositionInMilliseconds;
			IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, seekMethodId, parms);
			return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);			
		}

        public override bool SetMuteAudio(bool argIsMuted) {
			if(argIsMuted) {
				return AndroidJNI.CallBooleanMethod(clearVRCoreWrapperGlobalRef, muteAudioMethodId, emptyJvalue);
			} else {
				bool returnedValue = AndroidJNI.CallBooleanMethod(clearVRCoreWrapperGlobalRef, unmuteAudioMethodId, emptyJvalue);
				if(returnedValue) {
					// Successfully unmuted, the SetMuteAudio() API returns false in this case
					return false;
				}
				// Unable to unmute, we fall-through to returning false.
			}
			return false;
		}

		public override void SetAudioGain(float argGain) {
			jvalue[] parms = new jvalue[1];
			parms[0] = new jvalue();
			parms[0].f = argGain;
			AndroidJNI.CallVoidMethod(clearVRCoreWrapperGlobalRef, setAudioGainMethodId, parms);
		}

		public override float GetAudioGain() {
			if(!GetIsPlayerBusy()) {
				return 0;
			}
			return AndroidJNI.CallFloatMethod(clearVRCoreWrapperGlobalRef, getAudioGainMethodId, emptyJvalue);
		}

		public override bool GetIsAudioMuted() {
			if(!GetIsPlayerBusy()) {
				return false;
			}
			return AndroidJNI.CallBooleanMethod(clearVRCoreWrapperGlobalRef, getIsAudioMutedMethodId, emptyJvalue);
		}

		public override ClearVRAsyncRequest _SetAudioTrack(int argIndex, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, bool argIsCalledAsynchronous) {
			if(!GetIsPlayerBusy()) {
				return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot set audio track when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.SwitchAudioTrack);
			}
			jvalue[] parms = new jvalue[3];
			parms[0] = new jvalue();
			parms[0].i = argIndex;
			parms[1] = new jvalue();
			parms[1].i = (int) (argAudioDecoder != null ? argAudioDecoder.audioDecoderType : AudioDecoder.GetDefaultAudioDecoderForPlatform(_platformOptions.platform).audioDecoderType);
			parms[2] = new jvalue();
			parms[2].i = (int) (argAudioPlaybackEngine != null ? argAudioPlaybackEngine.audioPlaybackEngineType : AudioPlaybackEngine.GetDefaultAudioPlaybackEngineForPlatform(_platformOptions.platform).audioPlaybackEngineType);
			IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, setAudioTrackAndAudioDecoderAndAudioPlaybackEngineMethodId, parms);
			return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);			
		}

		public override ClearVRAsyncRequest _PrewarmCache(String argManifestUrl, int argInitialPositionInMilliseconds, long argFlags, bool argIsCalledAsynchronous) {
			if(!GetIsPlayerBusy()) {
				return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot prewarm cache when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.PrewarmCache);
			}
			jvalue[] parms = new jvalue[3];
			parms[0] = new jvalue();
			parms[0].l = AndroidJNI.NewStringUTF(argManifestUrl);
			parms[1] = new jvalue();
			parms[1].i = argInitialPositionInMilliseconds;
			parms[2] = new jvalue();
			parms[2].j = (long) argFlags;
			IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, prewarmCacheMethodId, parms);
			return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);			
		}

        public override int GetAudioTrack() {
			if(!GetIsPlayerBusy()) {
				return -1;
			}
			return AndroidJNI.CallIntMethod(clearVRCoreWrapperGlobalRef, getAudioTrackMethodId, emptyJvalue);
		}


		public override ClearVRAsyncRequest _SwitchContent(ContentItem argContentItem, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, bool argIsCalledAsynchronous) {
			if(!GetIsInitialized()) {
				return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot prewarm cache when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.SwitchContent);
			}
			UpdateOrientationInfo();
			float yaw = (float) -argContentItem.startYaw;
			float pitch = (float) argContentItem.startPitch;
			jvalue[] parms = new jvalue[8];
			parms[0] = new jvalue(); // manifest url (string)
			parms[0].l = AndroidJNI.NewStringUTF(argContentItem.manifestUrl);
			parms[1] = new jvalue();
			parms[1].f = yaw;
			parms[2] = new jvalue();
			parms[2].f =  pitch;
			parms[3] = new jvalue();
			parms[3].j = argContentItem.startPositionInMilliseconds; // start position in content in milliseconds (long)
			parms[4] = new jvalue();
			parms[4].i = 0; // The audio track index to select
			parms[5] = new jvalue();
			parms[5].i = (int) (argAudioDecoder != null ? argAudioDecoder.audioDecoderType : AudioDecoder.GetDefaultAudioDecoderForPlatform(_platformOptions.platform).audioDecoderType); // audio decoder type (int)
			parms[6] = new jvalue();
			parms[6].i = (int) (argAudioPlaybackEngine != null ? argAudioPlaybackEngine.audioPlaybackEngineType : AudioPlaybackEngine.GetDefaultAudioPlaybackEngineForPlatform(_platformOptions.platform).audioPlaybackEngineType); // audio playback engine type (int)
			parms[7] = new jvalue();
			parms[7].j = (long) 0L; // There are currently no SwitchContentFlags specified.
			switchContentContentItemQueue.Enqueue(argContentItem);
			IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, switchContentMethodId, parms);
			return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);
		}

        public override bool SetLoopContent(bool argIsContentLoopEnabled) {
			return SetClearVRCoreParameter("playback.loop_content", argIsContentLoopEnabled.ToString());
		}

		public override ClearVRAsyncRequest _SetStereoMode(bool argStereo, bool argIsCalledAsynchronous) {
			if(!GetIsPlayerBusy()) {
				return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot set stereo mode when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.ChangeStereoMode);
			}
			if(argStereo && ! GetIsContentFormatStereoscopic()){
				return InvokeClearVRCoreWrapperInvalidStateEvent("Cannot set stereo mode on monoscopic content.", argIsCalledAsynchronous, RequestTypes.ChangeStereoMode);
			}
			jvalue[] parms = new jvalue[1];
			parms[0] = new jvalue();
			parms[0].z = argStereo;
			IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, setStereoscopicModeMethodId, parms);
			return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);
		}

		public override ClearVRAsyncRequest _StartPlayout(bool argIsCalledAsynchronous) {
			if(!GetIsPlayerBusy()) {
				return InvokeClearVRCoreWrapperInvalidStateEvent("You can only call Play() once. If you would like to unpause, use the Unpause() API instead.", false, RequestTypes.Unknown);
			}
			IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, startPlayoutMethodId, emptyJvalue);
			return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);
		}

		public override ClearVRAsyncRequest _Pause(bool argIsCalledAsynchronous) {
			if(!GetIsPlayerBusy()) {
				return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot pause when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.Pause);
			}
			IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, pauseMethodId, emptyJvalue);
			return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);
		}

		public override ClearVRAsyncRequest _Unpause(bool argIsCalledAsynchronous) {
			if(!GetIsPlayerBusy()) {
				return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot unpause when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.Pause);
			}
			IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, unpauseMethodId, emptyJvalue);
			return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);
		}

		public override EventTypes GetEventType() {
			IntPtr eventTypeRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, getEventTypeMethodId, emptyJvalue);
			return clearVRAndroidJNIConverter.GetEventTypeFromRawObject(eventTypeRawObject);
		}

		/// <summary>
		/// Stop ClearVR, wait for CbClearVRCoreWrapperStopped() to know when this process has completed.
		/// </summary>
		public override ClearVRAsyncRequest _Stop(bool argStoppedAfterApplicationLostFocus, bool argIsCalledAsynchronous) {
			if(isPlayerShuttingDown) {
				// already stopping/shutting down
				return InvokeClearVRCoreWrapperInvalidStateEvent("Unable to stop, already stopping.", argIsCalledAsynchronous, RequestTypes.Stop);
			}
			isPlayerShuttingDown = true;
			if(argStoppedAfterApplicationLostFocus) {
				stoppedAfterApplicationLostFocusContentTimeInMilliseconds = GetCurrentContentTimeInMilliseconds();
			}
			if(_state != States.Uninitialized) {
				// This will trigger the shutdown on the render thread
				IssuePluginEvent(ClearVRNativeRendererPluginEvent.Destroy);
			}
			SetState(States.Stopping, ClearVRMessage.GetGenericOKMessage());
			if(clearVRCoreWrapperGlobalRef != IntPtr.Zero && stopClearVRCoreMethodId != IntPtr.Zero) { // This is true in case we created a ClearVRCoreWrapper object. If there is such an object, we can call its stop() method.
				// This will stop ClearVR. MUST be called before leaving the scene!
				IntPtr clearVRAsyncRequestRawObject = AndroidJNI.CallObjectMethod(clearVRCoreWrapperGlobalRef, stopClearVRCoreMethodId, emptyJvalue);
				return clearVRAndroidJNIConverter.ConvertClearVRAsyncRequestAsRawObjectToClearVRAsyncRequest(clearVRAsyncRequestRawObject);
			}// else no ClearVRCoreWrapper object is available. We will fake our Stop request and response.
			if(argIsCalledAsynchronous) {
				ClearVRAsyncRequest clearVRAsyncRequest = new ClearVRAsyncRequest(RequestTypes.Stop);
				ScheduleClearVREvent(new ClearVREvent(ClearVREventTypes.GenericMessage,
					new ClearVRAsyncRequestResponse(clearVRAsyncRequest.requestType, 
						clearVRAsyncRequest.requestId),
						ClearVRMessage.GetGenericOKMessage()
					)
				);
				// Remember that in MediaPlayerBase.CbClearVRCoreWrapperRequestCompleted() we trigger the state change to Stopped, so there is NO need to that here.
				return clearVRAsyncRequest;
			} else {
				ScheduleClearVREvent(ClearVREvent.GetGenericOKEvent(ClearVREventTypes.StateChangedStopped));
			}
			return new ClearVRAsyncRequest(RequestTypes.Stop);

		}

		/// <summary>
		/// Called to destroy the object
		/// </summary>
		public override void Destroy() {
			base.Destroy();
			if(!isPlayerShuttingDown) {
				Stop(false);
				return;
			}
		}
		
		public override void CleanUpAfterStopped() {
			if(!clearVRCoreWrapperRawClassGlobalRef.Equals(IntPtr.Zero))
				AndroidJNI.DeleteGlobalRef(clearVRCoreWrapperRawClassGlobalRef);
			clearVRCoreWrapperRawClassGlobalRef = IntPtr.Zero;
			if(!clearVRCoreWrapperGlobalRef.Equals(IntPtr.Zero))
				AndroidJNI.DeleteGlobalRef(clearVRCoreWrapperGlobalRef);
			clearVRCoreWrapperGlobalRef = IntPtr.Zero;
			if(!applicationContextRawObjectGlobalRef.Equals(IntPtr.Zero))
				AndroidJNI.DeleteGlobalRef(applicationContextRawObjectGlobalRef);
			applicationContextRawObjectGlobalRef = IntPtr.Zero;
			if(!clearVRCoreWrapperExternalInterfaceJavaProxyGlobalRef.Equals(IntPtr.Zero))
				AndroidJNI.DeleteGlobalRef(clearVRCoreWrapperExternalInterfaceJavaProxyGlobalRef);
			clearVRCoreWrapperExternalInterfaceJavaProxyGlobalRef = IntPtr.Zero;
			clearVRAndroidJNIConverter.Release();
		}
	}
}
#endif
