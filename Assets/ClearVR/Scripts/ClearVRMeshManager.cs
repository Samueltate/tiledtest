﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

namespace com.tiledmedia.clearvr {
    public class ClearVRMeshManager {
        protected List<ClearVRMeshBase> _clearVRMeshes = new List<ClearVRMeshBase>();
        protected int _currentClearVRMeshSignature = -1;
        protected Texture2D _nativeTexture = null;
        protected IntPtr _lastTextureId = IntPtr.Zero;
        public Texture2D nativeTexture {
            get {
                return _nativeTexture;
            }
        }

        public int currentClearVRMeshSignature {
            get {
                return _currentClearVRMeshSignature;
            }
        }
        protected int _currentClearVRMeshIndex = -1;
        public int currentClearVRMeshIndex {
            get {
                return _currentClearVRMeshIndex;
            }
        }
        protected bool _isVisible = true;
        public bool isVisible {
            get {
                return _isVisible;
            }
        }

        public int numberOfClearVRMeshes {
            get {
                return _clearVRMeshes.Count;
            }
        }

        internal void RemoveCurrentClearVRMesh() {
            if(_currentClearVRMeshSignature != -1) {
                int index = GetClearVRMeshIndexBySignature(_currentClearVRMeshSignature);
                _clearVRMeshes[index].SetNativeTexture(null); // explicitly unbind native texture
                UnityEngine.Object.DestroyImmediate(_clearVRMeshes[index].containerGameObject);
                _clearVRMeshes.Remove(_clearVRMeshes[index]);
                _currentClearVRMeshSignature = -1;
            }
        }

        internal void RecreateNativeTexture(int argNewIndex, 
                                            MeshTextureAvailableStruct argMeshTextureAvailableStruct,
                                            PlatformOptionsBase argPlatformOptions) {
            // Destroy the old texture
            if (_nativeTexture != null) {
                DestroyNativeTexture();
            }
            // Create external texture with correct dimensions
            // NOTE:
            // An "OPENGL NATIVE PLUG-IN ERROR: GL_INVALID_OPERATION: Operation illegal in current state" might be logged.
            // This bug is present since Unity 5.6.and has to do with a mismatch w.r.t. TextureFormat
            _nativeTexture = Texture2D.CreateExternalTexture(_clearVRMeshes[argNewIndex].frameWidth,
                                                            _clearVRMeshes[argNewIndex].frameHeight,
                                                            TextureFormat.RGBA32,
                                                            false,
                                                            false,
                                                            argMeshTextureAvailableStruct.textureId);
            // Set-up _MainTex shader variable to the correct texture.
            SetNativeTexture(nativeTexture, argNewIndex);
            #if UNITY_ANDROID && ! UNITY_EDITOR
                _clearVRMeshes[argNewIndex].EnableOrDisableOESFastPath(((PlatformOptionsAndroid) argPlatformOptions).isUseOESFastPathEnabled);
            #endif
            #if UNITY_IOS
                // Do not set OES parameter on shader, as it does not exist on iOS
            #endif
        }

        /// <summary>
        /// Updates the native texture id.
        /// </summary>
        /// <param name="argTextureId">Argument texture identifier.</param>
        internal void UpdateNativeTexture(int argTextureId) {
            UpdateNativeTexture((IntPtr) argTextureId);
        }

        /// <summary>
        /// Updates the native texture id.
        /// </summary>
        /// <param name="argTextureId">Argument texture identifier.</param>
        internal void UpdateNativeTexture(IntPtr argTextureId) {
            //System.Console.Write("[Unity] Setting texture id: " + argTextureId + "\n");
            if(_nativeTexture != null && argTextureId != IntPtr.Zero && _lastTextureId != argTextureId) {
                _lastTextureId = argTextureId;
                _nativeTexture.UpdateExternalTexture(argTextureId);
            }
        }
        internal void SetShader(Shader argClearVRMeshShader, int argOverrideIndex) {
            int index = -1;
            if (GetIndex(argOverrideIndex, out index)) {
                _clearVRMeshes[index].SetShader(argClearVRMeshShader);
            }
        }

        internal int GetClearVRMeshIndexBySignature(int argSignature) {
            for (int i = 0; i < _clearVRMeshes.Count; i++) {
                if (_clearVRMeshes[i].signature == argSignature) {
                    return i;
                }
            }
            return -1;
        }

        internal void DestroyClearVRMeshes() {
            _currentClearVRMeshIndex = -1;
            for (int i = 0; i < _clearVRMeshes.Count; i++) {
                UnityEngine.Object.Destroy(_clearVRMeshes[i].containerGameObject);
            }
            _clearVRMeshes.Clear();
        }

        internal bool SetMainColor(Color argNewColor) { 
            if (_currentClearVRMeshIndex != -1) {
                _clearVRMeshes[_currentClearVRMeshIndex].SetMainColor(argNewColor);
                return true;
            }
            return false;
        }

        internal GameObject GetActiveClearVRMesh() { 
			if (_currentClearVRMeshIndex != -1) {
				return _clearVRMeshes[_currentClearVRMeshIndex].containerGameObject;
			}
			return null;
        }

        internal ClearVRMeshTypes GetActiveClearVRMeshType() { 
			if (_currentClearVRMeshIndex != -1) {
				return _clearVRMeshes[_currentClearVRMeshIndex].clearVRMeshType;
			}
			return ClearVRMeshTypes.Unknown;
        }

        internal bool EnableOrDisableStereoscopicRendering(bool argSetMonoOrStereoMode) {
            if (_currentClearVRMeshIndex != -1) {
                _clearVRMeshes[_currentClearVRMeshIndex].EnableOrDisableStereoscopicRendering(argSetMonoOrStereoMode);
                return true;
            }
            return false;
        }

        internal void Rotate(Vector3 argNewEulerAngles) {
            if (_currentClearVRMeshIndex != -1) {
                _clearVRMeshes[_currentClearVRMeshIndex].Rotate(argNewEulerAngles);
            }
        }

        internal void Translate(Vector3 argTranslation, Space argRelativeTo) {
            if(_currentClearVRMeshIndex != -1) {
                _clearVRMeshes[_currentClearVRMeshIndex].Translate(argTranslation, argRelativeTo);
            }
        }

        internal void Translate(Vector3 argTranslation, Transform argRelativeTo) {
            if(_currentClearVRMeshIndex != -1) {
                _clearVRMeshes[_currentClearVRMeshIndex].Translate(argTranslation, argRelativeTo);
            }
        }

        internal void HideOrUnhide(bool argHideOrUnhide, int argOverrideIndex = -1) {
            int index = -1;
            if (GetIndex(argOverrideIndex, out index)) {
                _isVisible = argHideOrUnhide;
                _clearVRMeshes[index].containerGameObject.SetActive(argHideOrUnhide);
            }

        }

        internal void EnableOrDisableMeshRenderer(bool argEnabledOrDisabled, int argOverrideIndex = -1) {
            int index = -1;
            if (GetIndex(argOverrideIndex, out index)) {
                _clearVRMeshes[index].EnableOrDisableMeshRenderer(true);
            }
        }

        internal void SetNativeTexture(Texture2D argNativeTexture, int argOverrideIndex) {
            int index = -1;
            if (GetIndex(argOverrideIndex, out index)) {
                _clearVRMeshes[index].SetNativeTexture(argNativeTexture);
                _nativeTexture = argNativeTexture;
            }
        }

        internal void Recenter(Vector3 argNewOrientation){
            if(_currentClearVRMeshIndex != -1) {
                _clearVRMeshes[_currentClearVRMeshIndex].Rotate(argNewOrientation);
            }
        }

        internal void SetIsClearVRMeshReadyForDestruction(bool argValue, int argIndex) {
            _clearVRMeshes[argIndex].isReadyForDestruction = argValue;
        }

        internal bool GetIsReadyForDestruction(int argIndex) {
            return _clearVRMeshes[argIndex].isReadyForDestruction;
        }

        internal ClearVRMeshBase AddClearVRMeshBase(MeshCreatedStruct argMeshCreatedStruct, PlatformOptionsBase argPlatformOptions) {
            ClearVRMeshBase clearVRMesh;
            GameObject clearVRMeshGameObject = new GameObject("ClearVRMesh-GO-" + argMeshCreatedStruct.signature);
            switch (argMeshCreatedStruct.clearVRMeshType) {
                case ClearVRMeshTypes.Cubemap:
                    clearVRMesh = clearVRMeshGameObject.AddComponent<ClearVRMeshCubemap>();
                    break;
                case ClearVRMeshTypes.Cubemap180:
                    clearVRMesh = clearVRMeshGameObject.AddComponent<ClearVRMeshCubemap180>();
                    break;
                case ClearVRMeshTypes.Planar:
                    clearVRMesh = clearVRMeshGameObject.AddComponent<ClearVRMeshPlanar>();
                    break;
                default:
                    return null;
            }
            clearVRMeshGameObject.transform.SetParent(argPlatformOptions.parentGameObject.transform, false);
            clearVRMesh.SetContainerGameObject(clearVRMeshGameObject);
            _clearVRMeshes.Add(clearVRMesh);
            return clearVRMesh;
        }

        internal Vector3 GetEulerAngles(int argOverrideIndex = -1) {
            int index = -1;
            if (GetIndex(argOverrideIndex, out index)) {
                return _clearVRMeshes[index].eulerAngles;
            }
            return Vector3.zero;
        }

        internal int GetDecoderFrameWidth(int argOverrideIndex = -1) {
            int index = -1;
            if (GetIndex(argOverrideIndex, out index)) {
                if(_clearVRMeshes[index] != null) {
                    return _clearVRMeshes[index].frameWidth;
                }
            }
            return -1;
        }

        internal int GetDecoderFrameHeight(int argOverrideIndex = -1) {
            int index = -1;
            if (GetIndex(argOverrideIndex, out index)) {
                if(_clearVRMeshes[index] != null) {
                    return _clearVRMeshes[index].frameHeight;
                }
            }
            return -1;
        }

        private bool GetIndex(int argOverrideIndex, out int argIndex) {
            argIndex = (argOverrideIndex == -1) ? _currentClearVRMeshIndex : argOverrideIndex;
            return (argIndex != -1);
        }

        internal void SetActiveClearVRMeshSignature(int argSignature) {
            _currentClearVRMeshSignature = argSignature;
            _currentClearVRMeshIndex = GetClearVRMeshIndexBySignature(argSignature);
        }

        internal void DestroyNativeTexture() {
            if(_nativeTexture != null) {
                UnityEngine.GameObject.Destroy(_nativeTexture);
                _nativeTexture = null;
            }
        }
        public void Stop() {
            DestroyClearVRMeshes();
            DestroyNativeTexture();
        }
    }
}