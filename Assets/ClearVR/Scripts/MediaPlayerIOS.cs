﻿#if UNITY_IOS
/*
Tiledmedia Sample Player for Unity3D
(c) Tiledmedia, 2017 - 2018

Author: Arjen Veenhuizen
Contact: arjen@tiledmedia.com

*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Reflection;
using System;
using UnityEngine.XR;
using System.Threading;
using AOT;
using System.Runtime.CompilerServices;
namespace com.tiledmedia.clearvr {
    public class MediaPlayerIOS : MediaPlayerBase {
        public static class ClearVRCoreWrapperExternalInterface {
            /* The underlying SDK makes sure that this callback is always invoked from the main thread. Otherwise, random SEGFAULTS are to be expected! */
            private static MediaPlayerIOS mediaPlayer;

            public delegate void CbClearVRCoreWrapperRequestCompletedDelegate(ref ClearVRAsyncRequestResponseStruct argClearVRAsyncRequestResponseStruct);
            public delegate void CbClearVRCoreWrapperMessageDelegate(ref ClearVRMessageStruct argClearVRMessageStruct);
            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
            public struct ClearVRAsyncRequestStruct {
                public Int32 requestType;
                public UInt32 requestId;
            }

            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
            public struct ClearVRAsyncRequestResponseStruct {
                public Int32 requestType;
                public UInt32 requestId;
                public IntPtr clearVRMessageStructAsIntPtr;
            }

            [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
            public struct ClearVRMessageStruct {
                public Int32 messageType;
                public Int32 messageCode;
                public IntPtr messageAsIntPtr;
                public byte isSuccess;
            }

            public static void SetMediaPlayer(MediaPlayerIOS argMediaPlayer) {
                mediaPlayer = argMediaPlayer;
            }

            public static ClearVRMessage ConvertClearVRMessageStructToClearVRMessage(ClearVRMessageStruct argClearVRMessageStruct) {
                ClearVRMessage clearVRMessage = new ClearVRMessage(argClearVRMessageStruct.messageType,
                                                   argClearVRMessageStruct.messageCode,
                                                   MediaPlayerIOS.MarshalStringAndFree(argClearVRMessageStruct.messageAsIntPtr),
                                                   ClearVRMessage.ConvertBooleanToClearVRResult(argClearVRMessageStruct.isSuccess == 1));
                return clearVRMessage;
            }

            [MonoPInvokeCallback(typeof(CbClearVRCoreWrapperRequestCompletedDelegate))]
            /* The underlying SDK makes sure that this callback is always invoked from the main thread. Otherwise, random SEGFAULTS are to be expected! */
            public static void CbClearVRCoreWrapperRequestCompleted(ref ClearVRAsyncRequestResponseStruct argClearVRAsyncRequestResponseStruct) {
                ClearVRMessageStruct clearVRMessageStruct = (ClearVRMessageStruct)Marshal.PtrToStructure(argClearVRAsyncRequestResponseStruct.clearVRMessageStructAsIntPtr, typeof(ClearVRMessageStruct));
                ClearVRMessage clearVRMessage = ConvertClearVRMessageStructToClearVRMessage(clearVRMessageStruct);
                ClearVRAsyncRequestResponse clearVRAsyncRequestResponse = new ClearVRAsyncRequestResponse((RequestTypes)argClearVRAsyncRequestResponseStruct.requestType, (int)argClearVRAsyncRequestResponseStruct.requestId);
                mediaPlayer.CbClearVRCoreWrapperRequestCompleted(clearVRAsyncRequestResponse, clearVRMessage);
            }

            [MonoPInvokeCallback(typeof(CbClearVRCoreWrapperMessageDelegate))]
            /* The underlying SDK makes sure that this callback is always invoked from the main thread. Otherwise, random SEGFAULTS are to be expected! */
            public static void CbClearVRCoreWrapperMessage(ref ClearVRMessageStruct argClearVRMessageStruct) {
                mediaPlayer.CbClearVRCoreWrapperMessage(ConvertClearVRMessageStructToClearVRMessage(argClearVRMessageStruct));
            }
        }

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern void _bootstrap();
        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern void _destroy();
        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]

        private static extern IntPtr _initialize(IntPtr argLicenseFileData, 
            Int32 argLicenseFileDataSize, 
            String argDeviceAppId, 
            Int16 argScreenWidth, 
            Int16 argScreenHeight, 
            Int16 argDeviceType, 
            Int64 argInitializeFlags, 
            /* HTTP proxy */
            String argHttpProxyHost,
            Int32 argHttpProxyPort,
            String argHttpProxyUsername,
            String argHttpProxyPassword,
            /* HTTPS proxy */
            String argHttpsProxyHost,
            Int32 argHttpsProxyPort,
            String argHttpsProxyUsername,
            String argHttpsProxyPassword);
        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _parseMediaInfo(String argManifestUrl);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _stop();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _prepareContentForPlayout(String argManifestUrl,
                                                   double argStartYaw,
                                                   double argStartPitch,
                                                   Int32 argStartPositionInMilliseconds,
                                                   Int32 argAudioDecoderType,
                                                   Int32 argAudioPlaybackEngineType,
                                                   Int32 argStartClearVRCoreTimeoutInMilliseconds);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _startPlayout();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _switchContent(String argManifestUrl,
                                           double argStartYaw,
                                           double argStartPitch,
                                           Int32 argStartPositionInMilliseconds,
                                           Int32 argAudioDecoderType,
                                           Int32 argAudioPlaybackEngineType,
                                           Int64 argFlags);


        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _getClearVRCoreVersion();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern void _setCbClearVRCoreWrapperMessageDelegate(ClearVRCoreWrapperExternalInterface.CbClearVRCoreWrapperMessageDelegate callback);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern void _setCbClearVRCoreWrapperRequestCompletedDelegate(ClearVRCoreWrapperExternalInterface.CbClearVRCoreWrapperRequestCompletedDelegate callback);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern void _setCbClearVRCoreWrapperRendererFrameAvailableDelegate(IntPtr callback);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern void _freeChar(IntPtr argCharPointerToFree);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 _getCurrentContentTime();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 _getContentDuration();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 _getAudioTrack();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 _getClearVREventType();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 _getAverageBitrateInKbps();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern Int32 _getNumberOfAudioTracks();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _getParameterSafely(String argKey);
        
        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern byte _setParameterSafely(String argKey, String argValue);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _getArrayParameterSafely(String argKey, int argIndex);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _getGetParameterSafelyFunction();
        
        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _getDeviceAppId();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _getHighestQualityResolutionAndFramerate();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _pause();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _unpause();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _seek(Int32 argPositionInMilliseconds, Int64 argFlags);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _setStereoscopicMode(byte /* it's a 1-byte bool actually */ argMonoscopicOrStereoscopic);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _setAudioTrack(Int32 argAudioTrackIndex, Int32 argAudioDecoderType, Int32 AudioPlaybackEngineType);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _prewarmCache(String argManifestUrl, Int32 argInitialPositionInMilliseconds, Int64 argFlags);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern void _sendSensorData(double argYaw, double argPitch, double argRoll, double argYawVelocity, double argPitchVelocity, double argRollVelocity, double argYawAcceleration, double argPitchAcceleration, double argRollAcceleration);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr _getUpdateTextureFunction();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern byte _muteAudio();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern byte _unmuteAudio();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern byte _getIsAudioMuted();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern void _setAudioGain(float argGain);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        private static extern float _getAudioGain();

        /* Native Renderer Plugin imports */
        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CVR_NRP_Load();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CVR_NRP_GetCbRendererFrameAsByteArrayAvailable();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CVR_NRP_SetGetParameterSafelyFunctionPointer(IntPtr argGetParameterSafelyFunctionPointer);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CVR_NRP_SetUpdateTextureFunctionPointer(IntPtr argUpdateTextureFunctionPointer);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CVR_NRP_GetRenderEventFunc();

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CVR_NRP_SetOESFastPathEnabled(bool argValue);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CVR_NRP_SetMeshBuffersFromUnity(IntPtr argVertexBuffer,
            IntPtr argIndexBuffer,
            int argSignature);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CVR_NRP_SetSphereRadius(float argSphereRadius);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CVR_NRP_SetCallbackMeshCreated(CbMeshCreatedDelegate argCbMeshCreatedDelegate);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CVR_NRP_SetCallbackMeshTextureAvailable(CbMeshTextureAvailableDelegate argCbMeshTextureAvailableDelegate);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern void CVR_NRP_SetCallbackMeshDestroyed(CbMeshDestroyedDelegate argCbMeshDestroyedDelegate);

        [DllImport("__Internal", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr CVR_NRP_GetTextureId();


        private int isInitializeCalled = 0;
        public enum ClearVRNativeRendererPluginEvent {
            Unknown = 0,
            Initialize = 1,
            Update = 2,
            Destroy = 999
        }

        public static void IssuePluginEvent(ClearVRNativeRendererPluginEvent argType, long optionalOffset = 0) {
            if (CVRNRPRenderEventFunctionPointer.Equals(IntPtr.Zero)) {
                throw new Exception("[ClearVR] You have to call GetRenderEventFunc() in Start() first!");
            }
            GL.InvalidateState(); // Essential for proper operation in OpenGLES 3.x
            GL.IssuePluginEvent(CVRNRPRenderEventFunctionPointer, (int)argType + (int)optionalOffset);
        }

        public static IntPtr CVRNRPRenderEventFunctionPointer = IntPtr.Zero;

        private double[] sendSensorDataParams = new double[9]; // yaw, pitch, roll (absolute, angular velocity and angular acceleration)

        /// <summary>
        /// Class constructor
        /// </summary>
        /// <param name="argPlatformOptions">The options to use when constructing the class</param>
        /// <param name="argCbClearVREvent">The app-level ClearVREvent callback to trigger.</param>
        public MediaPlayerIOS(PlatformOptionsBase argPlatformOptions, UnityEngine.Events.UnityAction<MediaPlayerBase,ClearVREvent> argCbClearVREvent) : base(argPlatformOptions, argCbClearVREvent) {
            SetPlatformOptions(argPlatformOptions);
            if(!LoadShader()){
                // error has been handled in LoadShader().
                return;
            }
            if(!InitializePlatformBindings()) {
                ScheduleClearVREvent(new ClearVREvent(ClearVREventTypes.UnableToInitializePlayer, ClearVRMessageTypes.FatalError, ClearVRMessageCodes.GenericFatalError, "An error occured while initializing iOS-specific platform bindings. Cannot continue.", ClearVRResult.Failure));
                return;
            }
            if(_state != States.Initializing) {
                throw new Exception("[ClearVR] You must call InitializePlatformBindings() before you can instantiate this class.");
            }
            SetState(States.Initialized, ClearVRMessage.GetGenericOKMessage());
        }

        private bool InitializePlatformBindings() {
            SetState(States.Initializing, ClearVRMessage.GetGenericOKMessage());
            //// This is called to merely load the native plugin as soon as possible. It is doing *nothing* under the hood
            //// but it is essential to call this (at least once) in your code.
            CVR_NRP_Load();
            //// Next, we register our callbacks *before* we initialize the plugin on the GL render thread through IssuePluginEvent()
            CVR_NRP_SetCallbackMeshCreated(new CbMeshCreatedDelegate(CbMeshCreated));
            CVR_NRP_SetCallbackMeshDestroyed(new CbMeshDestroyedDelegate(CbMeshDestroyed));
            CVR_NRP_SetCallbackMeshTextureAvailable(new CbMeshTextureAvailableDelegate(CbMeshTextureAvailable));
            CVR_NRP_SetSphereRadius(_platformOptions.sphereRadius);
            CVRNRPRenderEventFunctionPointer = CVR_NRP_GetRenderEventFunc();
            // Must enable OES Fast Path before calling NRP.initialize on the rendering thread. By default, it will be disabled.
            CVR_NRP_SetOESFastPathEnabled(_isUseOESFastPathEnabled);
            for(int i = 0; i < sendSensorDataParams.Length; i++) {
                sendSensorDataParams[i] = 0.0d; // default value when values are not available (0...2 = yaw/pitch/roll, 3...5 = angular velocity, 6...8 = angular acceleration)
            }
            GetClearVRCoreVersion();
            return true;
        }

        /// <summary>
        /// Apply platform specific options
        /// </summary>
        /// <param name="argPlatformOptions"></param>
        private void SetPlatformOptions(PlatformOptionsBase argPlatformOptions) {
            _platformOptions = argPlatformOptions;
            _platformOptions.platform = RuntimePlatform.Android;
            if(_platformOptions.audioPlaybackEngine == null) {
                _platformOptions.audioPlaybackEngine = AudioPlaybackEngine.GetDefaultAudioPlaybackEngineForPlatform(_platformOptions.platform);
            }
            if(_platformOptions.audioDecoder == null) {
                _platformOptions.audioDecoder = AudioDecoder.GetDefaultAudioDecoderForPlatform(_platformOptions.platform);
            }
            #if UNITY_5_6_0 || UNITY_5_6_1
                if(_isUseOESFastPathEnabled)
                    throw new Exception("[ClearVR] Unity 5.6.0 and 5.6.1 do not support the Fast OES rendering path, see Unity bug #899502 for details. Please switch to a newer version of Unity, e.g. 2018.3.0f2.");
            #endif
        }
        // Allocate on the stack to make sure that it doesn't get GC-ed.
        IntPtr cbRendererFrameAsByteArrayAvailable;

        /// <summary>
        /// Called to prepare the underlying libraries for playout.
        /// This will yield a single frame that is used to construct the ClearVRMesh.
        /// </summary>
        public override ClearVRAsyncRequest _PrepareCore(bool argIsCalledAsynchronous) {
            if(!_state.Equals(States.Initialized))
                return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot prepare core when in {0} state.", GetCurrentStateAsString()), false, RequestTypes.Initialize);
            SetState(States.PreparingCore, ClearVRMessage.GetGenericOKMessage());
            // Bootstrap the C++ -> objective-C -> Swift bridge.
            _bootstrap();
            // This can only be called after _boostrap() has completed.
            CVR_NRP_SetGetParameterSafelyFunctionPointer(_getGetParameterSafelyFunction());
            CVR_NRP_SetUpdateTextureFunctionPointer(_getUpdateTextureFunction());
            ClearVRCoreWrapperExternalInterface.SetMediaPlayer(this);
            // GEt and register delegates.
            _setCbClearVRCoreWrapperRequestCompletedDelegate(ClearVRCoreWrapperExternalInterface.CbClearVRCoreWrapperRequestCompleted);
            _setCbClearVRCoreWrapperMessageDelegate(ClearVRCoreWrapperExternalInterface.CbClearVRCoreWrapperMessage);
            // Marshall license file bytes as to make sure that they can be properly relayed through our bridges.
            int size = Marshal.SizeOf(_platformOptions.licenseFileBytes[0]) * _platformOptions.licenseFileBytes.Length;
            IntPtr licenseFileDataAsIntPtr = Marshal.AllocHGlobal(size);
            try {
                Marshal.Copy(_platformOptions.licenseFileBytes, 0, licenseFileDataAsIntPtr, _platformOptions.licenseFileBytes.Length);
            } finally {
                // Free the unmanaged memory.
                // Marshal.FreeHGlobal(pnt);
            }
            ClearVRAsyncRequest clearVRAsyncRequest = ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(
                _initialize(
                    licenseFileDataAsIntPtr,
                    _platformOptions.licenseFileBytes.Length,
                    "", 
                    _platformOptions.screenWidth, 
                    _platformOptions.screenHeight, 
                    (Int16)_platformOptions.deviceType, 
                    _platformOptions.initializeFlags,
                    _platformOptions.httpProxyParameters.host,
                    _platformOptions.httpProxyParameters.port,
                    _platformOptions.httpProxyParameters.username,
                    _platformOptions.httpProxyParameters.password,
                    _platformOptions.httpsProxyParameters.host,
                    _platformOptions.httpsProxyParameters.port,
                    _platformOptions.httpsProxyParameters.username,
                    _platformOptions.httpsProxyParameters.password
                )
            );
            // FIXME: Can we already free the license file bytes at this point? We are only going to use it lateron.
            Marshal.FreeHGlobal(licenseFileDataAsIntPtr);
            cbRendererFrameAsByteArrayAvailable = CVR_NRP_GetCbRendererFrameAsByteArrayAvailable();
            _setCbClearVRCoreWrapperRendererFrameAvailableDelegate(cbRendererFrameAsByteArrayAvailable);
            // TODO: Port to iOS
            ///* Initialize statistics */
            //_clearVRCoreWrapperStatistics = new StatisticsAndroid(clearVRCoreWrapperRawClassGlobalRef, clearVRCoreWrapperGlobalRef);
            return clearVRAsyncRequest;
        }

        public static string GetClearVRCoreVersion() {
            if(_clearVRCoreVersionString.Equals("Unknown")) {
                _clearVRCoreVersionString = MarshalStringAndFree(_getClearVRCoreVersion());
            }
            return _clearVRCoreVersionString;
        }

        public override void Render() {
            /* Intentionally left blank for now */
        }

        public override void Update() {
            //System.Console.Write("[Unity] Update\n");
            base.UpdateClearVREventsToInvokeQueue();
            // As we cannot create a texture cache in the NRP right after app launch (Unity crashes mysteriously), we have to wait for a couple of frames to do that.
            // Two frames seems to be a safe number, we pick four to be "sure" it doesn't fail
            if(isInitializeCalled < 4) {
                isInitializeCalled++;
                if(isInitializeCalled == 4)
                    //// Initialize the native plugin.
                    //// Note that this asynchronous call can take one or two frames to complete.
                    IssuePluginEvent(ClearVRNativeRendererPluginEvent.Initialize);
            }
            if(SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.Metal) {
                _clearVRMeshManager.UpdateNativeTexture(CVR_NRP_GetTextureId());
            }
            if(SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.Metal) {
                //System.Console.Write(String.Format("[Unity] Issuing plugin event {0}\n", (countt + 2)));
                IssuePluginEvent(ClearVRNativeRendererPluginEvent.Update);// + countt);
                //countt++;
            }
        }
        //int countt = 1001;
        public override void EndOfFrame() {
            //System.Console.Write("[Unity] EndOfFrame\n");
            // We issue our plugin event after the current frame is rendered. That way, we know for sure that the next video frame will be available at the next (Late)Update()
            if (!isFirstFrame) {
                if(SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.OpenGLES3 || SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.OpenGLES2) {
                    IssuePluginEvent(ClearVRNativeRendererPluginEvent.Update);
                }
            }
        }

        public override void LateUpdate() {
            //System.Console.Write("[Unity] LateUpdate (UpdateTExture)\n");
            /* sending sensor data is only allowed after the first frame has been rendered. */
            if (!isFirstFrame) {
                if(_isViewportTrackingEnabled) {
                    SendSensorInfo();
                }
                // For OpenGL
                if(SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.OpenGLES3 || SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.OpenGLES2) {
                    _clearVRMeshManager.UpdateNativeTexture(CVR_NRP_GetTextureId());
                }
            }
        }

        public override ClearVRMeshBase ConstructClearVRMesh(MeshCreatedStruct argMeshCreatedStruct) {
            ClearVRMeshBase clearVRMesh = base.ConstructClearVRMesh(argMeshCreatedStruct);
            CVR_NRP_SetMeshBuffersFromUnity(clearVRMesh.mesh.GetNativeVertexBufferPtr(0),
                clearVRMesh.mesh.GetNativeIndexBufferPtr(),
                argMeshCreatedStruct.signature);
            return clearVRMesh;
        }

        public override void SendSensorInfo() {
            base.UpdateOrientationInfo();
            sendSensorDataParams[0] = orientationData[0];
            sendSensorDataParams[1] = orientationData[1];
            sendSensorDataParams[2] = orientationData[2];
            _sendSensorData(sendSensorDataParams[0], sendSensorDataParams[1], sendSensorDataParams[2],
                sendSensorDataParams[3], sendSensorDataParams[4], sendSensorDataParams[5],
                sendSensorDataParams[6], sendSensorDataParams[7], sendSensorDataParams[8]); 
        }

        /// <summary>
        /// Query the content duration. Can only be queried after at least parseMediaInfo() returned.
        /// </summary>
        /// <returns>The content duration in milliseconds</returns>
        public override long GetContentDurationInMilliseconds() {
            long contentDurationInMilliseconds = 0;
            if(GetIsPlayerBusy()) {
                if(GetIsMediaInfoParsed()) {
                    contentDurationInMilliseconds = (long) _getContentDuration();
                }
            }
            return contentDurationInMilliseconds;
        }

        /// <summary>
        /// Convenience method to return the number of audio tracks.
        /// </summary>
        /// <returns>The number of audio tracks, or -1 if this information is not (yet) available.</returns>
        public override int GetNumberOfAudioTracks() {
            int numberOfAudioTracks = -1;
            if(GetIsPlayerBusy()) {
                if(GetIsMediaInfoParsed()) {
                    return (int) _getNumberOfAudioTracks();
                }
            }
            return numberOfAudioTracks;
        }

        /// <summary>
        /// Actually load and start playout of the content item
        /// </summary>
        /// <param name="argContentItem">The content item to prepare</param>
        public override ClearVRAsyncRequest _PrepareContentForPlayout(ContentItem argContentItem, bool argIsCalledAsynchronous) {
            if(_state != States.CorePrepared) {
                return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot prepare content for playout when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.PrepareContentForPlayout);
            }
            /* Media info must have been parsed prior to preparing content for playout */
            if(!GetIsMediaInfoParsed()) {
                return InvokeClearVRCoreWrapperInvalidStateEvent("Cannot prepare content for playout in current state. Parse media info first!", argIsCalledAsynchronous, RequestTypes.PrepareContentForPlayout);
            }
            contentItem = argContentItem;
            SetState(States.PreparingContentForPlayout, ClearVRMessage.GetGenericOKMessage());
            SetLoopContent(_platformOptions.loopContent);

            if(_platformOptions.clearVRCoreLogToFile != "") {
				SetClearVRCoreParameter("config.verbosity", _platformOptions.clearVRCoreVerbosity.ToString());
                SetClearVRCoreParameter("config.filelog", _platformOptions.clearVRCoreLogToFile);
            }
            if(!_platformOptions.isVRDevicePresent) {
                SetClearVRCoreParameter("config.force_mono", "true");
                SetRenderMode(RenderModes.Monoscopic);
            }
            UpdateOrientationInfo();
			double yaw = orientationData[0] + contentItem.startYaw;
			double pitch = orientationData[1] + argContentItem.startPitch;
            return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(_prepareContentForPlayout(contentItem.manifestUrl,
                                                   yaw,
                                                   pitch,
                                                   (Int32) contentItem.startPositionInMilliseconds,
                                                   (Int32) _platformOptions.audioDecoder.audioDecoderType,
                                                   (Int32) _platformOptions.audioPlaybackEngine.audioPlaybackEngineType,
                                                   platformOptions.prepareContentForPlayoutTimeoutInMilliseconds));
        }

        public override String GetDeviceAppId() {
            return MarshalStringAndFree(_getDeviceAppId());
        }

        /// <summary>
        /// This method is directly linked to ClearVRCoreWrapper.getParameter().
        /// See the ClearVRCore docs for details about valid keys and when they can be queried. Note that this will return an empty string in case you try to query a parameter
        /// when in an illegal state.
        /// </summary>
        /// <param name="argKey"></param> The key to query.
        /// <returns>The value of the queried key. </returns>
        public override String GetClearVRCoreParameter(String argKey) {
            if(!GetIsPlayerBusy())
                return "";
            return MarshalStringAndFree(_getParameterSafely(argKey));
        }

        /// <summary>
        /// This method is directly linked to ClearVRCoreWrapper.getParameter().
        /// See the ClearVRCore docs for details about valid keys and when they can be queried. Note that this will return an empty string in case you try to query a parameter
        /// when in an illegal state.
        /// </summary>
        /// <param name="argKey"></param> The key to query.
        /// <returns>The value of the queried key. </returns>
        public override String GetClearVRCoreArrayParameter(String argKey, int argIndex) {
            if(!GetIsPlayerBusy())
                return "";
            return MarshalStringAndFree(_getArrayParameterSafely(argKey, argIndex));
        }


        /// <summary>
        /// This method is directly linked to ClearVRCoreWrapper.setParameter(). Return true is successful, false otherwise.
        /// See the ClearVRCore docs for details about valid keys and when they can be queried.
        /// </summary>
        /// <param name="argKey"> The key to set.</param>
        /// <param name="argValue">The value they key should be set to.</param>
        /// <returns>true if success, false otherwise.</returns>
        public override bool SetClearVRCoreParameter(String argKey, String argValue) {
            if(!GetIsPlayerBusy())
                return false;
            bool isSuccess = (_setParameterSafely(argKey, argValue) == 1);

            if(!isSuccess) { 
                UnityEngine.Debug.LogWarning(String.Format("[ClearVR] An exception was thrown while setting key '{0}' to {1} on ClearVRCore.", argKey, argValue));
            }
            return isSuccess;
        }

        /// <summary>
        /// Parse media info. This method is handled asynchronously since it involves network traffic and manifest parsing.
        /// You will have to wait for the ClearVREventTypes.MediaInfoParsed Event before it is safe to query
        /// audio/video parameters and select the audio track of you liking.
        /// </summary>
        /// <param name="argUrl"></param>
        public override ClearVRAsyncRequest _ParseMediaInfo(String argUrl, bool argIsCalledAsynchronous) {
            if(!GetIsPlayerBusy()) {
                return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot parse media info when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.ParseMediaInfo);
            }
            return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(_parseMediaInfo(argUrl));
        }

        /*
        Get the current average bitrate in megabit per second. Notice that the lower level method returns this value in kilobit per second.
        */
        public override float GetAverageBitrateInMbps() {
            if(!GetIsPlayerBusy())
                return 0f; // Cannot query this parameter in these states
            return (float)Math.Round((float) _getAverageBitrateInKbps() / 1024f, 1);
        }

        public override long GetCurrentContentTimeInMilliseconds() {
            if(!GetIsPlayerBusy())
                return 0;
            return _getCurrentContentTime();
        }

        public override string GetHighestQualityResolutionAndFramerate() {
            return MarshalStringAndFree(_getHighestQualityResolutionAndFramerate());
        }

        public override string GetCurrentResolutionAndFramerate() {
            return GetClearVRCoreParameter("playback.current_quality");
        }

        public override ClearVRAsyncRequest _Seek(long argNewPositionInMilliseconds, bool argIsCalledAsynchronous) {
            if(!GetIsPlayerBusy()) {
                return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot seek when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.Seek);
            }
            return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(_seek((Int32)argNewPositionInMilliseconds, 0));
        }

        public override bool SetMuteAudio(bool argIsMuted) {
            if (argIsMuted) {
               return (_muteAudio() == 1); // returns a bool in byte disguise
            } else {
                bool returnedValue = (_unmuteAudio() == 1);
				if(returnedValue) {
					// Successfully unmuted, the SetMuteAudio() API returns false in this case
					return false;
				}
				// Unable to unmute, we fall-through to returning false.
            }
            return false;
        }

        public override void SetAudioGain(float argGain) {
            _setAudioGain(argGain);
        }

        public override float GetAudioGain() {
            if(!GetIsPlayerBusy()) {
                return 0;
            }
            return _getAudioGain();
        }

        public override bool GetIsAudioMuted() {
            if(!GetIsPlayerBusy()) {
                return false;
            }
            return (_getIsAudioMuted() == 1);
        }

        public override ClearVRAsyncRequest _SetAudioTrack(int argIndex, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, bool argIsCalledAsynchronous) {
            if(!GetIsPlayerBusy()) {
                return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot set audio track when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.SwitchAudioTrack);
            }
            return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(_setAudioTrack(argIndex, (int) (argAudioDecoder != null ? argAudioDecoder.audioDecoderType : AudioDecoder.GetDefaultAudioDecoderForPlatform(_platformOptions.platform).audioDecoderType), (int) (argAudioPlaybackEngine != null ? argAudioPlaybackEngine.audioPlaybackEngineType : AudioPlaybackEngine.GetDefaultAudioPlaybackEngineForPlatform(_platformOptions.platform).audioPlaybackEngineType)));
        }

        public override ClearVRAsyncRequest _PrewarmCache(String argManifestUrl, int argInitialPositionInMilliseconds, long argFlags, bool argIsCalledAsynchronous) {
            if(!GetIsPlayerBusy()) {
                return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot prewarm cache when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.PrewarmCache);
            }
            return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(_prewarmCache(argManifestUrl, argInitialPositionInMilliseconds, argFlags));
        }

        public override int GetAudioTrack() {
            if(!GetIsPlayerBusy()) {
                return -1;
            }
            return _getAudioTrack();
        }

        public override ClearVRAsyncRequest _SwitchContent(ContentItem argContentItem, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, bool argIsCalledAsynchronous) {
            if(!GetIsInitialized()) {
                return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot prewarm cache when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.SwitchContent);
            }
            switchContentContentItemQueue.Enqueue(argContentItem);
			UpdateOrientationInfo();
			float yaw = (float) -argContentItem.startYaw;
			float pitch = (float) argContentItem.startPitch;
            return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(_switchContent(argContentItem.manifestUrl,
                                           yaw,
                                           pitch,
                                           (Int32) argContentItem.startPositionInMilliseconds,
                                           (Int32)(argAudioDecoder != null ? argAudioDecoder.audioDecoderType : AudioDecoder.GetDefaultAudioDecoderForPlatform(_platformOptions.platform).audioDecoderType),
                                           (Int32)(argAudioPlaybackEngine != null ? argAudioPlaybackEngine.audioPlaybackEngineType : AudioPlaybackEngine.GetDefaultAudioPlaybackEngineForPlatform(_platformOptions.platform).audioPlaybackEngineType),
                                           0));
           }

        public override bool SetLoopContent(bool argIsContentLoopEnabled) {
            return SetClearVRCoreParameter("playback.loop_content", argIsContentLoopEnabled.ToString());
        }

        public override ClearVRAsyncRequest _SetStereoMode(bool argStereo, bool argIsCalledAsynchronous) {
            if(!GetIsPlayerBusy()) {
                return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot set stereo mode when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.ChangeStereoMode);
            }
            if(argStereo && ! GetIsContentFormatStereoscopic()){
                return InvokeClearVRCoreWrapperInvalidStateEvent("Cannot set stereo mode on monoscopic content.", argIsCalledAsynchronous, RequestTypes.ChangeStereoMode);
            }
            return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(_setStereoscopicMode(argStereo ? (byte) 1 : (byte) 0));
        }

        public override ClearVRAsyncRequest _StartPlayout(bool argIsCalledAsynchronous) {
            if(!GetIsPlayerBusy()) {
                return InvokeClearVRCoreWrapperInvalidStateEvent("You can only call Play() once. If you would like to unpause, use the Unpause() API instead.", false, RequestTypes.Unknown);
            }
            return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(_startPlayout());
        }

        public override ClearVRAsyncRequest _Pause(bool argIsCalledAsynchronous) {
            if(!GetIsPlayerBusy()) {
                return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot pause when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.Pause);
            }
            return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(_pause());
        }

        public override ClearVRAsyncRequest _Unpause(bool argIsCalledAsynchronous) {
            if(!GetIsPlayerBusy()) {
                return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot unpause when in {0} state.", GetCurrentStateAsString()), argIsCalledAsynchronous, RequestTypes.Pause);
            }
            return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(_unpause());
        }

        public override EventTypes GetEventType() {
            if (!GetIsPlayerBusy()) {
                return EventTypes.Unknown;
            }
            Int32 clearVREventType = _getClearVREventType();
            switch (clearVREventType) {
                case 0:
                    return EventTypes.Unknown;
                case 1:
                    return EventTypes.Vod;
                case 2:
                    return EventTypes.Live;
                default:
                    throw new Exception(String.Format("Unsupported event type: {0}", clearVREventType));
            }
#pragma warning disable CS0162 // Unreachable code
            return EventTypes.Unknown;
#pragma warning restore CS0162 // Unreachable code
        }

        protected ClearVRAsyncRequest ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(IntPtr argClearVRAsyncRequestStructIntPtr) {
            ClearVRCoreWrapperExternalInterface.ClearVRAsyncRequestStruct clearVRAsyncRequestStruct = (ClearVRCoreWrapperExternalInterface.ClearVRAsyncRequestStruct)Marshal.PtrToStructure(argClearVRAsyncRequestStructIntPtr, typeof(ClearVRCoreWrapperExternalInterface.ClearVRAsyncRequestStruct));
            ClearVRAsyncRequest clearVRAsyncRequest = new ClearVRAsyncRequest((RequestTypes)clearVRAsyncRequestStruct.requestType, Convert.ToInt32(clearVRAsyncRequestStruct.requestId));
            return clearVRAsyncRequest;
        }

        /// <summary>
        /// Stop ClearVR, wait for CbClearVRCoreWrapperStopped() to know when this process has completed.
        /// </summary>
        public override ClearVRAsyncRequest _Stop(bool argStoppedAfterApplicationLostFocus, bool argIsCalledAsynchronous) {
            if (isPlayerShuttingDown) {
                // already stopping/shutting down
                return InvokeClearVRCoreWrapperInvalidStateEvent("Unable to stop, already stopping.", argIsCalledAsynchronous, RequestTypes.Stop);
            }
            isPlayerShuttingDown = true;
            if(argStoppedAfterApplicationLostFocus) {
                stoppedAfterApplicationLostFocusContentTimeInMilliseconds = GetCurrentContentTimeInMilliseconds();
            }
            if(_state != States.Uninitialized) {
                // This will trigger the shutdown on the render thread
                IssuePluginEvent(ClearVRNativeRendererPluginEvent.Destroy);
            }
            SetState(States.Stopping, ClearVRMessage.GetGenericOKMessage());
            // This will stop ClearVR. MUST be called before leaving the scene!
            IntPtr clearVRAsyncRequestMaybe = _stop(); // Might return nil is _bootstrap() was never called. This is unlikely but not impossible.
            if(clearVRAsyncRequestMaybe != IntPtr.Zero) {
                return ConvertClearVRAsyncRequestStructIntPtrToClearVRAsyncRequest(clearVRAsyncRequestMaybe);
            }
            // _stop() returned a null pointer, so we fake a state change to Stopped as we will never get one from the MediaFlow.
            if(argIsCalledAsynchronous) {
                ClearVRAsyncRequest clearVRAsyncRequest = new ClearVRAsyncRequest(RequestTypes.Stop);
                ScheduleClearVREvent(new ClearVREvent(ClearVREventTypes.GenericMessage,
                                                        new ClearVRAsyncRequestResponse(clearVRAsyncRequest.requestType, 
                                                    clearVRAsyncRequest.requestId),
                                                        ClearVRMessage.GetGenericOKMessage()
                                                        )
                                    );
				// Remember that in MediaPlayerBase.CbClearVRCoreWrapperRequestCompleted() we trigger the state change to Stopped, so there is NO need to that here.
                return clearVRAsyncRequest;
            } else {
                ScheduleClearVREvent(ClearVREvent.GetGenericOKEvent(ClearVREventTypes.StateChangedStopped));
            }
            // We create our own StopRequest
            return new ClearVRAsyncRequest(RequestTypes.Stop);
        }

        protected static String MarshalStringAndFree(IntPtr argStringAsIntPtr)
        {
            String marshaledString = Marshal.PtrToStringAnsi(argStringAsIntPtr);
            //_freeChar(argStringAsIntPtr);
            return marshaledString;
        }

        /// <summary>
        /// Called to destroy the object
        /// </summary>
        public override void Destroy() {
            base.Destroy();
            if(!isPlayerShuttingDown) {
                Stop(false);
                return;
            }
        }

        public override void CleanUpAfterStopped() {
            _destroy();
        }

    }
}
#endif
