﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Globalization;
using UnityEngine;

namespace com.tiledmedia.clearvr {
    /// <summary>
    /// This object is used to load an initial clip and to switch to a new clip.
    /// </summary>
    [Serializable]
    public class ContentItem {
        /// <summary>
        /// The horizontal field of view offset that defines the "front" of the 360 degree panorama.
        /// </summary>
        [Obsolete("The hfovOffset field (to specify the front of a 360 video) has been deprecated. Using the DEFAULT_HFOV_OFFSET_360_CONTENT constant does not yield any effect.", false)]
        public const int DEFAULT_HFOV_OFFSET_360_CONTENT = 0;
        /// <summary>
        /// The manifest URL that should be loaded
        /// </summary>
        public string manifestUrl;
        /// <summary>
        /// The horizontal offset in the content which allows you to specify the "forward looking" orientation.
        /// Currently, we support 90 and 270 degrees only.
        /// </summary>
        [Obsolete("The hfovOffset field (to specify the front of a 360 video) has been deprecated. One should not be using this field anymore. It will be removed on July 31st, 2019.", false)]
        public int hfovOffset;
        /// <summary>
        /// Approximate start position in the clip. Note that due to the nature of spatio-temporal video streaming, seeking accuracy is limited to GOP boundaries only.
        /// If no start position is provided, 0 is assumed when switching to a new clip, or the current position if switching to the exact same clip.
        /// </summary>
        public long startPositionInMilliseconds;
        /// <summary>
        /// Since v3.1
        /// 
        /// Add an additional offset (on top of hfovOffset) to the yaw orientation .
        /// </summary>
        public float startYaw;
        /// <summary>
        /// Specify a pitch offset.
        /// </summary>
        public float startPitch;
        /// <summary>
        /// Since v4.1.2
        /// Serializes a ContentItem to a semicolon separated string.
        /// 
        /// Notes:
        /// You are discouraged to use this API. It is primarily for internal purposes only.
        /// </summary>
        /// <returns>The serialized ContentItem</returns>
        public String Serialize() {
            String escapedManifestUrl = manifestUrl.Replace(";", "%3B");
            return String.Format("{0};{1};{2};{3}", escapedManifestUrl, startPositionInMilliseconds, startYaw.ToString(CultureInfo.InvariantCulture), startPitch.ToString(CultureInfo.InvariantCulture));
        }
        /// <summary>
        /// Since v4.1.2
        /// Deserializes a serialized ContentItem.
        /// 
        /// Notes:
        /// You are discouraged to use this API. It is primarily for internal purposes only.
        /// <param name="argSerializedContentItem">The serialized ContentItem string to deserialize.</param>
        /// <param name="argContentItem">The target ContentItem object.</param>
        /// </summary>
        /// <returns>True in case of success, False otherwise.</returns>
        public static bool Deserialize(String argSerializedContentItem, ref ContentItem argContentItem) {
            String[] splittedString = argSerializedContentItem.Split(';');
			if(splittedString.Length == 4) {
                argContentItem.manifestUrl = splittedString[0].Replace("%3B", ";");
                argContentItem.startPositionInMilliseconds = Convert.ToInt64(splittedString[1]);
                argContentItem.startYaw = float.Parse(splittedString[2], CultureInfo.InvariantCulture);
                argContentItem.startPitch = float.Parse(splittedString[3], CultureInfo.InvariantCulture);
    			#pragma warning disable 0618
                argContentItem.hfovOffset = 0; // Will be deprecated.
    			#pragma warning restore 0618
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// ContentItem constructor
        /// </summary>
        /// <param name="argManifestUrl">The manifest url pointing to the clip that needs to be loaded.</param>
        /// <param name="argStartPositionInMilliseconds">The approximate initial position in the video playback should start from in milliseconds. Default value: 0</param>
        /// <param name="argStartYaw">The start yaw in degree. Default value: 0</param>
        /// <param name="argStartPitch">The start pitch in degree. Default value: 0.</param>
        /// <param name="argHfovOffset">DEPRECATED! Specifies the "front"-face of the VR video in degree. Default value: 0. </param>
        [Obsolete("The hfovOffset field (to specify the front of a video) has been deprecated. Setting its value won't yield any effect. Please use the new ContentItem(String, long, floar, float) API instead. This API will be removed on July 31st, 2019.", false)]
        public ContentItem(String argManifestUrl, long argStartPositionInMilliseconds = 0, float argStartYaw = 0f, float argStartPitch = 0f, int argHfovOffset = DEFAULT_HFOV_OFFSET_360_CONTENT) {
            manifestUrl = argManifestUrl;
			#pragma warning disable 0618
            hfovOffset = 0;
			#pragma warning restore 0618
            startPositionInMilliseconds = argStartPositionInMilliseconds;
            startYaw = argStartYaw;
            startPitch = argStartPitch;
        }

        /// <summary>
        /// ContentItem constructor
        /// </summary>
        /// <param name="argManifestUrl">The manifest url pointing to the clip that needs to be loaded.</param>
        /// <param name="argStartPositionInMilliseconds">The approximate initial position in the video playback should start from in milliseconds. Default value: 0</param>
        /// <param name="argStartYaw">The start yaw in degree. Default value: 0</param>
        /// <param name="argStartPitch">The start pitch in degree. Default value: 0.</param>
        public ContentItem(String argManifestUrl, long argStartPositionInMilliseconds = 0, float argStartYaw = 0f, float argStartPitch = 0f) {
            manifestUrl = argManifestUrl;
            startPositionInMilliseconds = argStartPositionInMilliseconds;
            startYaw = argStartYaw;
            startPitch = argStartPitch;
			#pragma warning disable 0618
            hfovOffset = 0;
			#pragma warning restore 0618
        }

        /// <summary>
        /// Empty ContentItem constructor that sets all its fields to default values. Note that this also sets the hfovOffset field to -90 degree.
        /// </summary>
        [Obsolete("ContentItem() is deprecated, please use ContentItem(string, long, float, float, int) instead.")]
        public ContentItem() {
            manifestUrl = "";
			#pragma warning disable 0618
            hfovOffset = 0;
			#pragma warning restore 0618
            startPositionInMilliseconds = 0;
            startYaw = 0;
            startPitch = 0;
        }
    }
    /// <summary>
    /// Convenience array of ContentItems
    /// </summary>
    [Serializable]
    public class ContentItemList {
        public com.tiledmedia.clearvr.ContentItem[] content_items;
    }
}