﻿using System;   
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Reflection;

namespace com.tiledmedia.clearvr {
	public static class Utils {
		private static bool isVrDevicePresent; // Do not set a value yet, assume "default"
        private static DeviceTypes deviceType = DeviceTypes.Unknown;
        private static bool isInited = false;

        private static bool GetIsHeadsetDevice() {
            return (deviceType == DeviceTypes.Cardboard || 
                deviceType == DeviceTypes.Daydream ||
                deviceType == DeviceTypes.GearVR ||
                deviceType == DeviceTypes.OculusGo ||
                deviceType == DeviceTypes.Quest ||
                deviceType == DeviceTypes.WaveVR ||
                deviceType == DeviceTypes.PicoVR);
        }

        // This function will be used to detect the WaveVR SDK.
        // Note that the WaveVR SDK does not expose itself through UnityEngine.XR
        [DllImport("wvrunity", CallingConvention = CallingConvention.Cdecl)]
        public static extern System.IntPtr GetRenderEventFunc();

		/* Dummy load a simple function from the OVRPlugin. This is used lateron to verify whether the OVRPlugin is at all available or not. */
		[DllImport("OVRPlugin", CallingConvention = CallingConvention.Cdecl)]
		public static extern bool ovrp_GetInitialized();

        public static bool GetIsVrDevicePresent() {
            if(!isInited) {
                DetectDeviceType();
            }
            return isVrDevicePresent;
        }

        public static DeviceTypes GetDeviceType() {
            if(!isInited) {
                DetectDeviceType();
            }
            return deviceType;
        }

        internal static Type GetType(string argTypeName){
            // Try Type.GetType() first. This will work with types defined
            // by the Mono runtime, in the same assembly as the caller, etc.
            var type = Type.GetType(argTypeName);
            // If it worked, then we're done here
            if(type != null) {
                return type;
            }
            // If the argTypeName is a full name, then we can try loading the defining assembly directly
            if(argTypeName.Contains(".")) {
                // Get the name of the assembly (Assumption is that we are using 
                // fully-qualified type names)
                var assemblyName = argTypeName.Substring(0, argTypeName.IndexOf('.'));
                // Attempt to load the indicated Assembly
                try { 
                    Assembly assembly = Assembly.Load(assemblyName); // can throw an Exception
                    if(assembly != null) {
                        // Ask that assembly to return the proper Type
                        try {
                            type = assembly.GetType(argTypeName); // can throw an Exception
                            if(type != null) {
                                return type;
                            }
                        } catch {
                            // fallthrough
                        }
                    } else {
                        // fallthrough
                    }
                } catch {
                    // fallthrough
                }        
            }
            // If we still haven't found the proper type, we can enumerate all of the 
            // loaded assemblies and see if any of them define the type
            var currentAssembly = Assembly.GetExecutingAssembly();
            var referencedAssemblies = currentAssembly.GetReferencedAssemblies();
            foreach(var assemblyName in referencedAssemblies) {
                Assembly assembly = null;
                // Load the referenced assembly
                try {
                    assembly = Assembly.Load(assemblyName);
                } catch {
                    continue; // Failed for whatever reason, let's try the next one
                }
                if(assembly != null) {
                    // See if that assembly defines the named type
                    try {
                        type = assembly.GetType(argTypeName);
                        if(type != null) {
                            return type;
                        }
                    } catch {
                        continue;
                    }
                }
            }
        
            // The type just couldn't be found...
            return null;
        }
        
        /// <summary>
        /// Redetect the device type. Call this API if you switched to a different mode (e.g. from traditional flat to cardboard). Use Utils.GetDeviceType() to get the newly detected device type.
        /// Notes:
        /// 1. This is a SLOW and EXPENSIVE call. We highly recommend you to query once and cache your result afterwards.
        /// 2. This does NOT return the same value as UnityEngine.XR.XRSettings.loadedDeviceName or UnityEngine.XR.XRDevice.system. Some VR platforms, notably WaveVR, do not expose any usable values on these Unity specific APIs.
        /// 3. This could theoreticaly change on run-time, for example if you would switch from flat (None) to cardboard (Cardboard) mode.
        /// </summary>
        public static void RedetectDeviceType() {
            DetectDeviceType();
        }
        
        internal static void DetectDeviceType() {
            deviceType = DeviceTypes.Unknown;
#if UNITY_2018_1_OR_NEWER
            String loadedDeviceName = UnityEngine.XR.XRSettings.loadedDeviceName;
#else
            String loadedDeviceName = UnityEngine.VR.VRSettings.loadedDeviceName;
#endif
            switch(loadedDeviceName.ToLower()) {
                case "daydream":
                    deviceType = DeviceTypes.Daydream;
                    break;
                case "cardboard":
                    deviceType = DeviceTypes.Cardboard;
                    break;
                case "oculus":
                    // needs magic detection
                    break;
                // Note that we cannot check for empty string "" or "none" as that will also be set in case of Wave VR
                default:
                    break; // Let's try some other mechanisms.
            }
            // Check for Oculus Headsets, we will use introspection to figure out the exact headset using OVRPlugin.GetSystemHeadsetType().
			Type ovrPluginType = GetType("OVRPlugin");
			if(ovrPluginType != null) { // Good, the OVRDisplay object is available
				bool isOVRPluginLibraryFound = false;
				try {
					ovrp_GetInitialized();
					isOVRPluginLibraryFound = true;
				} catch (DllNotFoundException) {
					// OVR source code is there, but the OVR plugin cannot be loaded.
				}
				if(isOVRPluginLibraryFound) {
					try {
						MethodInfo getSystemHeadsetTypeMethodInfo = ovrPluginType.GetMethod("GetSystemHeadsetType", BindingFlags.Public | BindingFlags.Static);
                        if(getSystemHeadsetTypeMethodInfo != null) {
                            // Note that the System.Type of the HeadsetType enum is "OVRPlugin+SystemHeadset"
                            String headsetType = getSystemHeadsetTypeMethodInfo.Invoke(null /* static */, null /* no args */).ToString();
                            // fuzzy match on "GearVR", as there are various GearVR models
                            if(headsetType.IndexOf("GearVR") != -1) {
                                deviceType = DeviceTypes.GearVR;
                            } else if(headsetType.Equals("Oculus_Go")) {
                                deviceType = DeviceTypes.OculusGo;
                            } else if(headsetType.Equals("Oculus_Quest")) {
                                deviceType = DeviceTypes.Quest;
                            }
                        }
					} catch (Exception e) { // explicit fallthrough as we do not care for the exception in this case
						UnityEngine.Debug.LogError("[ClearVR] An error was thrown while detecting headset type. Details: " + e);
					}
				} else {
					// no OVRPlugin.so present in application
				}
			}
            if(deviceType == DeviceTypes.Unknown) {
                Type waveVRType = GetType("WaveVR");
                if(waveVRType != null) {
                    try {
                        GetRenderEventFunc();
                        deviceType = DeviceTypes.WaveVR;
                    } catch {
                        // Assume that there is no WaveVR library active.
                    }
                }
            }
            if(deviceType == DeviceTypes.Unknown) {
                Type picoVRType = GetType("Pvr_UnitySDKAPI.Render");
                if(picoVRType != null) {
                    try {
                        MethodInfo upvr_GetSupportHMDTypesMethodInfo = picoVRType.GetMethod("UPvr_GetSupportHMDTypes", BindingFlags.Public | BindingFlags.Static);
                        if(upvr_GetSupportHMDTypesMethodInfo != null) {
                            String supportedHMDType = upvr_GetSupportHMDTypesMethodInfo.Invoke(null /* static */, null /* no args */).ToString();
                            if(!String.IsNullOrEmpty(supportedHMDType)) {
                                deviceType = DeviceTypes.PicoVR;
                            }
                        }
                    } catch {
                        // Assume that there is no PicoVR library active.
                    }
                }
            }
            if(deviceType == DeviceTypes.Unknown) {
                deviceType = DeviceTypes.Flat;
            }
            isVrDevicePresent = GetIsHeadsetDevice();
            isInited = true;
        }
	}
}
