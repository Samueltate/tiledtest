﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Linq;

namespace com.tiledmedia.clearvr {
	public class ClearVREvent {
		private ClearVREventTypes _type;
		private ClearVRMessage _message = null;
		private ClearVRAsyncRequestResponse _clearVRAsyncRequestResponse = null;

		public ClearVREventTypes type {
			get {
				return _type;
			}
			set {
				_type = value;
			}
		}

		/// <summary>
		/// The message that this event holds.
		/// </summary>
		/// <value>The ClearVRMessage</value>
		public ClearVRMessage message {
			get {
				return _message;
			}
			set {
				_message = value;
			}
		}

		public ClearVRAsyncRequestResponse clearVRAsyncRequestResponse {
			get {
				return _clearVRAsyncRequestResponse;
			}
			private set {
				_clearVRAsyncRequestResponse = value;
			}
		}

		public static ClearVREvent GetGenericOKEvent(ClearVREventTypes argType) {
			return new ClearVREvent(argType, ClearVRMessage.GetGenericOKMessage());
		}

		public static ClearVREvent GetGenericWarningEvent(ClearVREventTypes argType) {
			return new ClearVREvent(argType, ClearVRMessage.GetGenericWarningMessage());
		}

		public static ClearVREvent GetGenericFatalErrorEvent(ClearVREventTypes argType) {
			return new ClearVREvent(argType, ClearVRMessage.GetGenericFatalErrorMessage());
		}

		public ClearVREvent(ClearVREventTypes argEventType, ClearVRMessageTypes argMessageType, ClearVRMessageCodes argCode, String argMessage, ClearVRResult argClearVRResult) {
			_type = argEventType;
			_message = new ClearVRMessage(argMessageType, argCode, argMessage, argClearVRResult);
		}

		public ClearVREvent(ClearVREventTypes argEventType, int argMessageType, int argCode, String argMessage, ClearVRResult argClearVRResult) {
			_type = argEventType;
			_message = new ClearVRMessage(argMessageType, argCode, argMessage, argClearVRResult);
		}

		public ClearVREvent(ClearVREventTypes argEventType, ClearVRMessage argMessage) {
			_type = argEventType;
			_message = argMessage;
		}

		public ClearVREvent(ClearVREventTypes argEventType, ClearVRAsyncRequestResponse argClearVRAsyncRequestResponse, ClearVRMessage argClearVRMessage) {
			_type = argEventType;
			_message = argClearVRMessage;
			_clearVRAsyncRequestResponse = argClearVRAsyncRequestResponse;
		}

		public bool HasWarningOrFatalErrorMessage() {
			return !(message.GetIsInfo());
		}

		public bool GetIsStateChangedEvent() {
			return true ? (_type.ToString().IndexOf("StateChanged") == 0) : false;
		}

		public void Print() {
			UnityEngine.Debug.Log(String.Format("Event type: {0}, message type: {1}, message code: {2}, result: {3}, message string: {4}", _type, message.type, message.code, message.result, message.message));
		}

		[Obsolete("ClearVREvent.ParseAudioTrackSwitchedEvent() is deprecated. Use ClearVREvent.message.ParseAudioTrackSwitched() instead.", true)]
		public bool ParseAudioTrackSwitchedEvent(	out int argAudioTrackIndex, 
													out AudioDecoder argAudioDecoder, 
													out AudioPlaybackEngine argAudioPlaybackEngine) {
			argAudioTrackIndex = -1;
			argAudioDecoder = new AudioDecoder(AudioDecoderTypes.Unknown);
			argAudioPlaybackEngine = new AudioPlaybackEngine(AudioPlaybackEngineTypes.Unknown);
			if(!_type.Equals(ClearVREventTypes.AudioTrackSwitched)){
				return false;
			}
			return message.ParseAudioTrackSwitched(out argAudioTrackIndex, out argAudioDecoder, out argAudioPlaybackEngine);
		}
		
		public bool ParseClearVRCoreWrapperStateChangedStoppedEvent(ref ContentItem argContentItem) {
			if(!_type.Equals(ClearVREventTypes.StateChangedStopped)) {
				return false;
			}
			return message.ParseClearVRCoreWrapperStateChangedStopped(ref argContentItem);
		}
	}

	public class ClearVRMessage {
		private ClearVRMessageTypes _type;
		private int _code;
		private String _message;
		private ClearVRResult _result;

		public ClearVRMessageTypes type {
			get {
				return _type;
			}
			set {
				_type = value;
			}
		}

		public bool GetIsClearVRCoreErrorCode() {
			return Enum.IsDefined(typeof(ClearVRCoreErrorCodes), _code);
		}

		public ClearVRCoreErrorCodes GetClearVRCoreErrorCode() {
			if(GetIsClearVRCoreErrorCode()) {
				return (ClearVRCoreErrorCodes) _code;
			}
			return ClearVRCoreErrorCodes.UnknownError;
		}

		public ClearVRMessageCodes GetClearVRMessageCode() {
			if(!GetIsClearVRCoreErrorCode()) {
				return (ClearVRMessageCodes) _code;
			}
			return ClearVRMessageCodes.Unknown;
		}

		public String GetCode() {
			if(GetIsClearVRCoreErrorCode()) {
				return ((ClearVRCoreErrorCodes) _code).ToString();
			}
			return ((ClearVRMessageCodes) _code).ToString();
		}

		public int code {
			get {
				return _code;
			}
			set {
				_code = value;
			}
		}

		public String message {
			get{
				return _message;
			}
			set{
				_message = value;
			}
		}

		public ClearVRResult result {
			get {
				return _result;
			}
			set{
				_result = value;
			}
		}

		public static ClearVRMessage GetGenericOKMessage() {
			return new ClearVRMessage(ClearVRMessageTypes.Info, ClearVRMessageCodes.GenericOK, "", ClearVRResult.Success);
		}

		public static ClearVRMessage GetGenericWarningMessage() {
			return new ClearVRMessage(ClearVRMessageTypes.Warning, ClearVRMessageCodes.GenericWarning, "An unexpected warning occured.", ClearVRResult.Failure);
		}

		public static ClearVRMessage GetGenericFatalErrorMessage() {
			return new ClearVRMessage(ClearVRMessageTypes.FatalError, ClearVRMessageCodes.GenericWarning, "An unexpected fatal error occured.", ClearVRResult.Failure);
		}
		
		public ClearVRMessage(ClearVRMessageTypes argType){
			_type = argType;
			_code = (int) ClearVRMessageCodes.GenericOK;
			_message = "";
			_result = ClearVRResult.Success;
		}

		public ClearVRMessage(int argType){
			_type = (ClearVRMessageTypes) argType;
			_code = (int) ClearVRMessageCodes.GenericOK;
			_message = "";
			_result = ClearVRResult.Success;
		}

		public ClearVRMessage(ClearVRMessageTypes argType, ClearVRMessageCodes argCode, String argMessage, ClearVRResult argClearVRResult){
			_type = argType;
			_code = (int) argCode;
			_message = argMessage;
			_result = argClearVRResult;
		}

		public ClearVRMessage(int argType, int argCode, String argMessage, ClearVRResult argClearVRResult){
			_type = (ClearVRMessageTypes) argType;
			_code = argCode;
			_message = argMessage;
			_result = argClearVRResult;
		}

		public void Update(ClearVRMessageTypes argType, ClearVRMessageCodes argCode, String argMessage, ClearVRResult argClearVRResult){
			_type = argType;
			_code = (int) argCode;
			_message = argMessage;			
			_result = argClearVRResult;
		}

		public void Update(int argType, int argCode, String argMessage, ClearVRResult argClearVRResult){
			_type = (ClearVRMessageTypes) argType;
			_code = argCode;
			_message = argMessage;			
			_result = argClearVRResult;
		}		

		public bool GetIsFatalError(){
			return _type == ClearVRMessageTypes.FatalError;
		}
		
		public bool GetIsWarning(){
			return _type == ClearVRMessageTypes.Warning;
		}

		public bool GetIsInfo(){
			return _type == ClearVRMessageTypes.Info;
		}

		public bool GetIsSuccess() {
			return (_result == ClearVRResult.Success);
		}
		
		public String GetFullMessage() {
			return String.Format("Type: {0}, code: {1} ({2}), message: {3}. Result {4}", _type, GetCode(), _code, _message.Trim('.'), _result);
		}

		public static ClearVRResult ConvertBooleanToClearVRResult(bool argValue){
			return argValue ? ClearVRResult.Success : ClearVRResult.Failure;
		}

		[Obsolete("ClearVRMessage.ParseAudioTrackSwitched() is deprecated. Use ClearVREvent.message.ParseAudioTrackChanged() instead.", true)]
		public bool ParseAudioTrackSwitched(out int argAudioTrackIndex, 
				out AudioDecoder argAudioDecoder, 
				out AudioPlaybackEngine argAudioPlaybackEngine) {
			argAudioTrackIndex = -1;
			argAudioDecoder = new AudioDecoder(AudioDecoderTypes.Unknown);
			argAudioPlaybackEngine = new AudioPlaybackEngine(AudioPlaybackEngineTypes.Unknown);
			return ParseAudioTrackChanged(out argAudioTrackIndex, out argAudioDecoder, out argAudioPlaybackEngine);
		}
		
		public bool ParseAudioTrackChanged(out int argAudioTrackIndex, 
				out AudioDecoder argAudioDecoder, 
				out AudioPlaybackEngine argAudioPlaybackEngine) {
			argAudioTrackIndex = -1;
			argAudioDecoder = new AudioDecoder(AudioDecoderTypes.Unknown);
			argAudioPlaybackEngine = new AudioPlaybackEngine(AudioPlaybackEngineTypes.Unknown);
			String[] splittedString = message.Split(';');
			if(splittedString.Length == 1) { // Support for legacy SDKs 
				splittedString = message.Split(',');
			}
			if(splittedString.Length == 3){
				try {
					argAudioTrackIndex = Convert.ToInt32(splittedString[0]);
					argAudioDecoder = new AudioDecoder((AudioDecoderTypes) Convert.ToInt32(splittedString[1]));
					argAudioPlaybackEngine = new AudioPlaybackEngine((AudioPlaybackEngineTypes) Convert.ToInt32(splittedString[1]));
					return true;
				} catch { // Ignore any exception
					return false;
				}
			}
			return false;
		}

		public bool ParseClearVRCoreWrapperVideoDecoderCapabilities(out VideoDecoderCapabilities argVideoDecoderCapabilities) {
			if(GetClearVRMessageCode() == ClearVRMessageCodes.ClearVRCoreWrapperVideoDecoderCapabilities) {
				return VideoDecoderCapabilities.Deserialize(this.message, out argVideoDecoderCapabilities);
			}
			argVideoDecoderCapabilities = null;
			return false;
		}

		public bool ParseClearVRCoreWrapperStateChangedStopped(ref ContentItem argContentItem) {
			String workingMessage = message;
			String[] splittedString = workingMessage.Split(';');
			if(splittedString.Length != 5 /* 4 + 1 */ ) {
				return false;
			}
			// We would expect this, as the first item in the splitted string list contains the Stopped event message (which might be a zero length string)
			workingMessage = string.Join(";", splittedString.Skip(1).ToArray());
			return ContentItem.Deserialize(workingMessage, ref argContentItem);
		}
	}


		public class VideoDecoderCapabilities {
			String _videoDecoderName;
			String _mimetype;
			String _decoderMaximumCodecLevelSupported;
			String _clearVRSdkMaximumCodecLevelSupported;
			String _maximumCodecLevelInUse;
			public String videoDecoderName { 
				get {
					return _videoDecoderName;
				}
			}

			public String mimetype {
				get {
					return _mimetype;
				}
			}

			public String decoderMaximumCodecLevelSupported {
				get {
					return _decoderMaximumCodecLevelSupported;
				}
			}

			public String clearVRSdkMaximumCodecLevelSupported {
				get {
					return _clearVRSdkMaximumCodecLevelSupported;
				}
			}

			public String maximumCodecLevelInUse {
				get {
					return _maximumCodecLevelInUse;
				}
			}

			protected VideoDecoderCapabilities(String argVideoDecoderName, String argMimetype, String argDecoderMaximumCodecLevelSupported, String argClearVRSdkMaximumCodecLevelSupported, String argMaximumCodecLevelInUse) {
				_videoDecoderName = argVideoDecoderName;
				_mimetype = argMimetype;
				_decoderMaximumCodecLevelSupported = argDecoderMaximumCodecLevelSupported;
				_clearVRSdkMaximumCodecLevelSupported = argClearVRSdkMaximumCodecLevelSupported;
				_maximumCodecLevelInUse = argMaximumCodecLevelInUse;
			}

			public String Serialize() {
				return String.Format("{0};{1};{2};{3};{4}", _videoDecoderName, _mimetype, _decoderMaximumCodecLevelSupported, _clearVRSdkMaximumCodecLevelSupported, _maximumCodecLevelInUse);
			}

			public static bool Deserialize(String argSerializedString, out VideoDecoderCapabilities argVideoDecoderCapabilities) {
				argVideoDecoderCapabilities = null;
				String[] splittedString = argSerializedString.Split(';');
				if(splittedString.Length == 5) { // Support for legacy SDKs 
					try {
						argVideoDecoderCapabilities = new VideoDecoderCapabilities(splittedString[0], splittedString[1], splittedString[2], splittedString[3], splittedString[4]);
						return true;
					} catch { // ignore any exception
						return false;
					}
				}
				return false;
			}
		}

	public enum ClearVRResult {
		Unspecified,
		Success,
		Failure
	}

	public enum ClearVRMessageTypes {
		FatalError,
		Warning,
		Info
	}

	public enum ApplicationFocusAndPauseHandlingTypes {
		Legacy,
		Recommended,
		Disabled
	}

	public enum ClearVREventTypes {
		None,
		StateChangedUninitialized,
		StateChangedInitializing,
		StateChangedInitialized,
		StateChangedPreparingCore,
		StateChangedCorePrepared,
		StateChangedPreparingContentForPlayout,
		StateChangedContentPreparedForPlayout,
		StateChangedBuffering,
		StateChangedPlaying,
		StateChangedPausing,
		StateChangedPaused,
		StateChangedSeeking,
		StateChangedSwitchingContent,
		StateChangedFinished, /* end of clip reached */
		StateChangedStopping, /* tearing down core */
		StateChangedStopped, /* core tear down complete, fully stopped */

		ParsingMediaInfo,
		MediaInfoParsed,
		RenderModeChanged,
		FirstFrameRendered,
		AudioTrackSwitched,
		ContentSwitched,
		UnableToInitializePlayer,
		ResumingPlaybackAfterApplicationPaused,
		StereoModeSwitched,
		// Since v4.0
		ContentFormatChanged,
		ABRSwitch,
		PrewarmCacheCompleted,

		GenericMessage
	}

	public enum Projections {
		Spherical
	}

	public enum RenderModes {
		Native,
		Monoscopic,
		Stereoscopic
	}


	public enum EventTypes {
		Unknown,
		Vod,
		Live
	}

	public enum ClearVRMessageCodes {
		Unknown = 0,
		/* Message codes reserved for Unity */
		GenericOK = -2000,
		GenericWarning = -2001,
		GenericFatalError = -2002,
		SetRenderModeFailed = -2003,
		APINotSupportedOnThisPlatform = -2004,

		/* ClearVRCoreWrapper related errors */

		/* -1000 .. -1099 are fatal ERROR messages */
		// -1000 - -1009 reserved for ClearVRCore initialization errors
		ClearVRCoreWrapperUnspecifiedFatalError = -1000,
		ClearVRCoreWrapperAlreadyInitialized = -1001,
		ClearVRCoreWrapperFatalException = -1002,
		ClearVRCoreWrapperNotProperlyInitialized = -1003,
		ClearVRCoreWrapperUnableToParseMediaInfo = -1004,
		ClearVRCoreWrapperCannotGenerateDeviceAppId = -1005,
		ClearVRCoreWrapperContentLoadingTimeout = -1006,
		ClearVRCoreWrapperInitializationTimeout = -1007,
		ClearVRCoreWrapperInvalidOrUnsupportedProxySettings = -1008,

		// 1010 - 1019 reserved for video decoder related errors
		ClearVRCoreWrapperVideoDecoderNotInitialized = -1010,
		ClearVRCoreWrapperVideoDecoderDecodingFailure = -1011,
		ClearVRCoreWrapperNoHardwareVideoDecoderAvailable = -1012,
		ClearVRCoreWrapperCannotDecodeFrame = -1013,
		ClearVRCoreWrapperVideoDecoderDoesNotSupportProfileOrLevel = -1014,
		ClearVRCoreWrapperVideoMimetypeNotSupportBySDK = -1015,
		ClearVRCoreWrapperVideoDecoderCannotConfigureDecoder = -1016,

		/* Audio decoder related fatal errors */
		ClearVRCoreWrapperAudioDecoderNotInitialized = -1020,
		ClearVRCoreWrapperAudioDecoderDecodingFailure = -1021,
		ClearVRCoreWrapperAudioDecoderCannotDecodeSample = -1022,
		ClearVRCoreWrapperAudioDecoderFormatNotSupported = -1023,
		/* 1080 - 1099 reserved for generic errors */

		/* -1100 .. -1199 are non-fatal WARNING _messages */
		ClearVRCoreWrapperUnspecifiedWarning = -1100,
		ClearVRCoreWrapperNonFatalClearVRCoreException = -1101,
		ClearVRCoreWrapperSpatialAudioNotSupported = -1102,
		ClearVRCoreWrapperVideoDecoderInputOverflow = -1103,
		ClearVRCoreWrapperVideoDecoderOutputOverflow = -1104,
		ClearVRCoreWrapperInvalidState = -1105,
		ClearVRCoreWrapperContentDoesNotSupportSeek = -1106,
		ClearVRCoreWrapperCannotSwitchAudioTrack = -1107,
		ClearVRCoreWrapperCannotSwitchContent = -1108,
		ClearVRCoreWrapperVideoDecoderFrameDropped = -1109,
		ClearVRCoreWrapperRequestCancelled = -1110,
		/// <summary>
		/// A message with this code indicates that the video decoder cannot keep up with the video framerate. For example, you are trying to play back a 60 fps clip on a device that is only rated for 30 fps at the given resolution.
		/// As it is virtually impossible to know what a decoder is truely capable off a priori (especially on the Android ecosystem), and as the video resolution we feed into the decoder varies from clip to clip, we will try to 
		/// playback video as good as possible. If the decoder struggles or is failing miserably to keep up, you will receive this warning message and it is up to you to decide what to do with it. 
		/// 
		/// Note that this is a "simplified" version of the "ClearVRCoreWrapperVideoDecoderInputOverflow" message which gives you a slightly more detailed message (its message value contains how much the decoder input queue overflowed during the last 10 seconds).
		/// </summary>
		ClearVRCoreWrapperVideoDecoderTooSlow = -1111,
		/// <summary>
		/// There is the possibility that the video decoder threw an error during configure-state. We will retry this a couple of times (in varying configurations) before we give up.
		/// Note that when you receive this message, you should treat it as a warning. There is still the chance that the configure will be succesful at the next attempt.
		/// </summary>
		ClearVRCoreWrapperVideoDecoderConfigureDecoderThrewWarning = -1112,
		/// <summary>
		/// There is the remote possibility that a decoded frame has no metadata associated with it. In that case, the video frame must be dropped. 
		/// Notes:
		/// 1. It is extremely unlikely that this event is triggered. Please contact us if you see this event passing by.
		/// </summary>
		ClearVRCoreWrapperVideoDecoderFrameWithoutMetadataDropped = -1113,
		
		/* -1200 .. -1299 are INFO messages */
		ClearVRCoreWrapperGenericOK = -1200,
		ClearVRCoreWrapperGenericInfo = -1201,
		ClearVRCorWrapperOpenGLVersionInfo = -1202,
		ClearVRCoreWrapperVideoDecoderCapabilities = -1203,
		ClearVRCoreWrapperAudioTrackChanged = -1204,
		ClearVRCoreWrapperStereoscopicModeChanged = -1205,

        /* -1300 .. -1399 are state change messages */
        ClearVRCoreWrapperClearVRCoreStateUninitialized = -1300,
        ClearVRCoreWrapperClearVRCoreStateInitializing = -1301,
        ClearVRCoreWrapperClearVRCoreStateInitialized = -1302,
        ClearVRCoreWrapperClearVRCoreStateRunning = -1303,
        ClearVRCoreWrapperClearVRCoreStatePausing = -1304,
        ClearVRCoreWrapperClearVRCoreStatePaused = -1305,
        ClearVRCoreWrapperClearVRCoreStateBuffering = -1306,
        ClearVRCoreWrapperClearVRCoreStateSeeking = -1307,
        ClearVRCoreWrapperClearVRCoreStateSwitchingContent = -1308,
        ClearVRCoreWrapperClearVRCoreStateFinished = -1309,
        ClearVRCoreWrapperClearVRCoreStateStopped = -1310,
    }
	
	public enum RequestTypes {
		/* A list of requests types that can be performed on the ClearVRCoreWrapper */
		Unknown = 0,
		Initialize = 1,
		ParseMediaInfo = 2,
		PrepareContentForPlayout = 3,
		Start = 4,
		Pause = 5,
		Unpause = 6,
		Seek = 7,
		Stop = 8,
		SwitchAudioTrack = 9,
		SwitchContent= 10,
		ChangeStereoMode = 11,
		PrewarmCache = 12,
		ClearVRPlayerInitialize = 999 /* This is a magic RequestType that codes for a request to initialize a (Unity) ClearVRPlayer class */ 
	}

	public enum InitializeFlags {
		None = 0,
		NoCachePrewarming = 1,
		LongBuffer = 2
	}

	public enum PrewarmCacheFlags {
		None = 0
	} 

	public enum DeviceTypes {
		// This list must be in sync with the list in core/External.go. As the core does not specify an Unknown/default value, we start at -1 in our case.
		Unknown    = -1,
		Flat       = 0,
		GenericHmd = 1,
		OculusGo   = 2,
		GearVR     = 3,
		Quest      = 4,
		WaveVR     = 5,
		Daydream   = 6,
		Cardboard  = 7,
		PicoVR     = 8
	}

	static class DeviceTypesMethods {
		public static bool GetIsAndroidOculusDevice(this DeviceTypes argDeviceType) {
			return (argDeviceType == DeviceTypes.GearVR || 
				argDeviceType == DeviceTypes.OculusGo ||
				argDeviceType == DeviceTypes.Quest);
		}
	}
	public class ProxyParameters {
		public enum ProxyTypes {
			Unknown = 0,
			Http = 1,
			Https = 2
		}
		private ProxyTypes _proxyType = ProxyTypes.Unknown;
		private String _host = "<auto>";
		private int _port = -1;
		private String _username = "";
		private String _password = "";
		public ProxyTypes proxyType {
			get {
				return _proxyType;
			}
		}
		public String host {
			get {
				return _host;
			}
		}
		public int port {
			get {
				return _port;
			}
		}
		public String username {
			get {
				return _username;
			}
		}
		public String password {
			get {
				return _password;
			}
		}
		public ProxyParameters(ProxyTypes argProxyType) {
			_proxyType = argProxyType;
		}

		/// <summary>
		/// Set the proxy settings. At minimal, host and port need to specified if a proxy is active. You can either opt for auto-detection (setting them to "<auto>" and -1 respectively or disable proxy detection completely (by setting them to "" and 0 respectively). Optionally a username and password can be set. 
		/// Leave username and password empty (e.g. an empty string) in case an anonymous proxy is used. 
		/// <param name="argHost">The proxy host. Set to its default value "<auto>" for auto-detection. Set to an empty string to display the proxy.</param>
		/// <param name="argPort">The proxy port. Set to its default value -1 for auto-detection, set to 0 for disable the proxy.</param>
		/// <param name="argUsername">The proxy username. Default value: "". Note that we cannot auto-detect the username, it must always be provided in case the proxy does not allow anonymous access.</param>
		/// <param name="argPassword">The proxy password. Default value: "". Note that we cannot auto-detect the password, it must always be provided in case the proxy does not allow anonymous access.</param>
		/// </summary>
		public void SetProxyParameters(String argHost, int argPort, String argUsername = "<auto>", String argPassword = "") {
			_host = argHost;
			_port = argPort;
			_username = argUsername;
			_password = argPassword;
		}
	}

	public class PlatformOptionsBase : PlatformOptionsInterface {
		public RuntimePlatform platform = Application.platform;
		public byte[] licenseFileBytes;
		public bool isVRDevicePresent = false;
		public GameObject parentGameObject;
		/// <summary>
		/// Deprecated, this field will be removed on 2018-12-31. Superseded by the trackingTransform field.
		/// This transform of the camera field is used to track the orientation of the viewport.
		/// </summary>
		[Obsolete("The camera field is deprecated. Use the trackingTransform field instead.", true)]
		public Camera camera = null;
		public RenderModes preferredRenderMode = RenderModes.Native;
		public bool autoPlay = true;
		public bool loopContent = true;
		public ContentItem autoPrepareContentItem = null;
		public AudioPlaybackEngine audioPlaybackEngine = null;
		public AudioDecoder audioDecoder = null;
		public float sphereRadius = 15f;
		public int sphereLayer = 0;
		public ApplicationFocusAndPauseHandlingTypes applicationFocusAndPauseHandling = ApplicationFocusAndPauseHandlingTypes.Recommended;
		/// <summary>
		/// If set to true, the Render mode will keep track of whether the core is rendering in mono or stereoscopic mode. This could change, e.g. due to ABR. If set to false, the application implementor is tasked with doing the switching himself. See ClearVRPlayer.cs for ane xample on how ClearVREventTypes.StereoModeSwitched can be parsed.
		/// </summary>
		public bool enableAutomaticRenderModeSwitching = true; 
		public short screenWidth = 0;
		public short screenHeight = 0;
		/// <summary>
		/// The device type. For optimal performance it is essential to set the correct device type. Set/keep at its default value DeviceTypes.Unknown for auto-detection. If you fail to set the correct value, poor performance, poor visual quality or stereoscopic playback may break.
		/// </summary>
		public DeviceTypes deviceType = DeviceTypes.Unknown;
		/// <summary>
		/// ClearVRCore verbosity level. Please keep at 0 at all times.
		/// </summary>
		public int clearVRCoreVerbosity = 0;
		public String clearVRCoreLogToFile = "";
		[Obsolete("enableAutomaticRenderModeSwitchingOnContentFormatChanged has been deprecated and is no longer useful. Please remove any reference to it from your code", false)]
		public bool enableAutomaticRenderModeSwitchingOnContentFormatChanged = true;
		public long initializeFlags = (long) InitializeFlags.None;
		/// <summary>
		/// Any value <= 0 will result in the default timeout to be used. This default value is currently 30000 milliseconds.
		/// This timeout will only be triggered if loading a specific content item took longer than the specified amount of time. In this case, one will receive a ClearVRCoreWrapperContentLoadingTimeout message.
		/// </summary>
		public int prepareContentForPlayoutTimeoutInMilliseconds = 0; 
		/// <summary>
		/// Setting this to true (default: false) allows the implementor to change the transparency of the sphere by using the mediaPlayer.SetMainColor() api.
		/// For common performance reasons not specifically related to ClearVR streaming, you should carefully assess the potential negative performance impact of using transparency-enabled shaders on mobile devices.
		/// Please note that you need to manually include the required shaders into your project (via Edit -> Project Settings -> Graphics -> Always Included Shaders).
		/// </summary>
		[Obsolete("You can no longer explicitly enable or disable support for transparency. Setting an alpha component != 1 using the SetMainColor() API now automatically enables transparency support.", true)]
		public bool isTransparencySupportEnabled = false;
		/// <summary>
		/// Since v4.1.2
		/// Supersedes the deprecated camera field. This transform is used to track the highquality viewport. Typically, one would set this field to the activeCamera.transform field.
		/// One could also use the transform of for example a random gameobject and let the viewport track the orientation of it.
		/// </summary>
		public Transform trackingTransform = null;
		/// <summary>
		/// Since v5.1
		/// Specify the HTTPS proxy settings. Note that the lower-level SDK will attempt to detect proxy host and port automatically if host and port are at their default values ("<auto>" and -1 respectively).
		/// Due to platform security constraints, we cannot detect username and password automatically. We must rely on the application to provide those.
		/// </summary>
		public readonly ProxyParameters httpProxyParameters = new ProxyParameters(ProxyParameters.ProxyTypes.Http);
		/// <summary>
		/// Since v5.1
		/// Specify the HTTPS proxy settings. Note that the lower-level SDK will attempt to detect proxy host and port automatically if host and port are at their default values ("<auto>" and -1 respectively).
		/// Due to platform security constraints, we cannot detect username and password automatically. We must rely on the application to provide those.
		/// </summary>
		public readonly ProxyParameters httpsProxyParameters = new ProxyParameters(ProxyParameters.ProxyTypes.Https);
		/// <summary>
		/// An experimenteal feature that toggles whether headset orientation acceleration and velocity is used to tune streaming performance or not.
		/// Please do not change.
		/// </summary>
		[Obsolete("This option is obsolete and will be removed no later than 2019-12-31", false)]
		public bool useHeadsetAccelerationAndVelocityIfAvailable = ((int) ((new System.Random(Guid.NewGuid().GetHashCode())).Next(100)) <= 1);
		public virtual bool Verify() {
			// Each platform specific options class should call this base.Verify() method, see e.g. PlatformOptionsAndroid.Verify()
			if(trackingTransform == null) {
				UnityEngine.Debug.LogError("[ClearVR] No tracking transform set.");
				return false;
			}
			if(screenHeight == 0 || screenWidth == 0) {
				return false;
			}
			if(licenseFileBytes == null) {
				UnityEngine.Debug.LogError("[ClearVR] No license file data provided.");
				return false;
			}
			return true;
		}
	}
	
	public class PlatformOptionsAndroid : PlatformOptionsBase {
		public new RuntimePlatform platform = RuntimePlatform.Android;
		/// <summary>
		/// Enables OES Fast Path mode. Video frames are directly streamed from the decoder onto the sphere, skipping one texture copy. An important limitation is that this mode does not support picture-in-picture. 
		/// </summary>
		public bool isUseOESFastPathEnabled = false;
		/// <summary>
		/// Verifies if Android platform specific options are valid or not.
		/// </summary>
		/// <returns>true if everything is OK, false otherwise.</returns>
		public override bool Verify() {
			bool isSuccess = base.Verify();
			if(isSuccess){
				return true;
			}
			return false;
		}
	}

    public class PlatformOptionsIOS : PlatformOptionsBase {
        public new RuntimePlatform platform = RuntimePlatform.IPhonePlayer;
        public bool isUseOESFastPathEnabled = true; // default true, do not change as non OES Fast Path mode is not supported.

        /// <summary>
        /// Verifies if IOs platform specific options are valid or not.
        /// </summary>
        /// <returns>true if everything is OK, false otherwise.</returns>
        public override bool Verify() {
            bool isSuccess = base.Verify();
			if(!isUseOESFastPathEnabled) {
				UnityEngine.Debug.LogError("[ClearVR] non OES Fast Path is currently not supported on iOS. Please set platformOptions.isUseOESFastPathEnabled = true");
				isSuccess = false;				
			}
            if(isSuccess){
                return true;
            }
            return false;
        }
    }

	public enum ContentFormat {
		Unknown,
		Monoscopic180,
		Monoscopic360,
		Stereoscopic180,
		Stereoscopic360,
		Planar
	}


	public class AudioPlaybackEngine {
		private AudioPlaybackEngineTypes _audioPlaybackEngineType;
		public AudioPlaybackEngineTypes audioPlaybackEngineType {
			get{
				return _audioPlaybackEngineType;
			}
		}
		
		public AudioPlaybackEngine(AudioPlaybackEngineTypes argType) {
			_audioPlaybackEngineType = argType;
		}

		public bool supportsSpatialAudio { 
			get { 
				switch (_audioPlaybackEngineType) {
					case AudioPlaybackEngineTypes.AndroidDefault:
						return false;
                    case AudioPlaybackEngineTypes.IOSDefault:
                        return false;
                    default:
						return false; 
				}
			}
		}

		public static AudioPlaybackEngine GetDefaultAudioPlaybackEngineForPlatform(RuntimePlatform argPlatform) {
			switch (argPlatform){
				case RuntimePlatform.Android:
					return new AudioPlaybackEngine(AudioPlaybackEngineTypes.AndroidDefault);
                case RuntimePlatform.IPhonePlayer:
                    return new AudioPlaybackEngine(AudioPlaybackEngineTypes.IOSDefault);
                default:
					return new AudioPlaybackEngine(AudioPlaybackEngineTypes.Unknown);
			}
		}
	}

	public enum AudioPlaybackEngineTypes {
		Unknown = -1,
		AndroidDefault = 0,
		AndroidAudioTrack = 1,
		AndroidOpenSL = 2,
        IOSDefault = 100,
    }

	public class AudioDecoder {
		private AudioDecoderTypes _audioDecoderType;
		public AudioDecoderTypes audioDecoderType {
			get{
				return _audioDecoderType;
			}
		}
		public AudioDecoder(AudioDecoderTypes argType){
			_audioDecoderType = argType;
		}

		public static AudioDecoder GetDefaultAudioDecoderForPlatform(RuntimePlatform argPlatform) {
			switch (argPlatform) {
				case RuntimePlatform.Android:
					return new AudioDecoder(AudioDecoderTypes.AndroidDefault);
                case RuntimePlatform.IPhonePlayer:
                    return new AudioDecoder(AudioDecoderTypes.IOSDefault);
                default:
					return new AudioDecoder(AudioDecoderTypes.Unknown);
			}
		}
	}

	public class ClearVRAsyncRequest {
		public static int CLEAR_VR_REQUEST_ID_NOT_SET = 1;
		static System.Random randomNumberGenerator = new System.Random();
		public RequestTypes requestType;
		public int requestId;
		public object[] optionalArguments;
		public Action<ClearVREvent, ClearVRPlayer> cbClearVRAsyncResponseReceived;
		public ClearVRAsyncRequest(int argRequestType, int argRequestId, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			requestType = (RequestTypes) argRequestType;
			requestId = argRequestId;
			optionalArguments = argOptionalArguments;
			cbClearVRAsyncResponseReceived = argCbClearVRAsyncRequestResponseReceived;
		}
		public ClearVRAsyncRequest(int argRequestType, int argRequestId) : this((RequestTypes) argRequestType, argRequestId) {
		}

		public ClearVRAsyncRequest(RequestTypes argRequestType, int argRequestId) {
			requestType = argRequestType;
			requestId = argRequestId;
		}

		public void UpdateActionAndOptionalArguments(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) { 
			cbClearVRAsyncResponseReceived = argCbClearVRAsyncRequestResponseReceived;
			optionalArguments = argOptionalArguments;
		}

		public ClearVRAsyncRequest(RequestTypes argRequestType) : this(argRequestType, randomNumberGenerator.Next(1000, Int32.MaxValue)) {
		}
	}

	public class ClearVRAsyncRequestResponse {
        public RequestTypes requestType;
        public int requestId;
		public object[] optionalArguments;
		public ClearVRAsyncRequestResponse(RequestTypes argRequestType, int argRequestId, params object[] argOptionalArguments) {
			requestType = argRequestType;
			requestId = argRequestId;
			optionalArguments = argOptionalArguments;
		}
	}

	public enum AudioDecoderTypes {
		Unknown = -1,
		AndroidDefault = 0,
		AndroidMediaCodec = 1,
        IOSDefault = 0,
    }

	// Unmanaged --> Managed callback structss
	[Serializable]
	public struct MeshCreatedStruct {
		public int vertexCount;
		public int indexCount;
		public float sphereRadius;
		public int frameWidth;
		public int frameHeight;
		public int signature;
		public int overlapX;
		public int overlapY;
		public FallbackLayoutIdentifier fallbackLayoutIdentifier;
		public float boundsX;
		public float boundsY;
		public float boundsZ;
		public ClearVRMeshTypes clearVRMeshType;
		// We unmarshal the pointer to a List<FallbackCubefaceInfoStruct> manually
		public IntPtr fallbackCubefaceInfoStructsIntPtr;
	}

	[Serializable]
	public struct MeshDestroyedStruct {
		public int signature;
	}


	public enum FallbackCubefaceIdentifier {
		Unknown = 0,
		LeftEyeLeft = 1,
		LeftEyeRight = 2,
		LeftEyeTop = 3,
		LeftEyeBottom = 4,
		LeftEyeFront = 5,
		LeftEyeBack = 6,
		/* right eye is currently not signalled */
		RightEyeLeft = 101,
		RightEyeRight = 102,
		RightEyeTop = 103,
		RightEyeBottom = 104,
		RightEyeFront = 105,
		RightEyeBack = 106
	};

	[Serializable]
		public struct FallbackCubefaceInfoStruct {
			public FallbackCubefaceIdentifier fallbackCubefaceIdentifier;
			public int topLeftX;
			public int topLeftY;
			public int width;
			public int height;
		};


	[Serializable]
	public struct MeshTextureAvailableStruct {
		public int signature;
		public IntPtr textureId;
	}

	public enum FallbackLayoutIdentifier {
		Unknown,
		CubemapTwoByOne,
		CubemapThreeByTwo
	}

	public class FallbackLayout {
		public FallbackLayoutIdentifier fallbackLayoutIdentifier;
		public int overlapX;
		public int overlapY;
		public List<FallbackCubefaceInfoStruct> fallbackCubefaceInfoStructs = new List<FallbackCubefaceInfoStruct>();
	}

    public enum ClearVRMeshTypes {
        Unknown = 0,
        Cubemap = 1,
		Planar = 2,
		Cubemap180 = 3,
    }
}
