﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Rendering;
using System;
using System.Runtime.InteropServices;
using UnityEngine.XR;
using AOT;
using System.Runtime.CompilerServices;

namespace com.tiledmedia.clearvr {
	public abstract class MediaPlayerBase : InternalInterface, MediaPlayerInterface, MediaInfoInterface, MediaControllerInterface, PerformanceInterface, DebugInterface {
		/* InternalInterface */
		public abstract void Update();
		public abstract void LateUpdate();
		public abstract void Render();
		public abstract void CleanUpAfterStopped();
		public abstract void SendSensorInfo();
		public abstract ClearVRAsyncRequest _StartPlayout(bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _PrepareCore(bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _Pause(bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _Unpause(bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _ParseMediaInfo(String argUrl, bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _Seek(long argNewPositionInMilliseconds, bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _SetStereoMode(bool argStereo, bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _PrewarmCache(String arManifestUrl, int argInitialPositionInMilliseconds, long argFlags, bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _SetAudioTrack(int argIndex, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _SwitchContent(ContentItem argContentItem, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _PrepareContentForPlayout(ContentItem argContentItem, bool argIsCalledAsynchronous);
		public abstract ClearVRAsyncRequest _Stop(bool argStoppedAfterApplicationLostFocus, bool argIsCalledAsynchronous);

		/* MediaPlayerInterface */
		public abstract String GetClearVRCoreParameter(String argKey);
		public abstract String GetClearVRCoreArrayParameter(String argKey, int argIndex);
        public abstract bool SetClearVRCoreParameter(String argKey, String argValue);
		
		/* MediaControllerInterface */
        public abstract bool SetLoopContent(bool argIsContentLoopEnabled);
		public abstract long GetCurrentContentTimeInMilliseconds();
		public abstract bool SetMuteAudio(bool argIsMuted);
		public abstract void SetAudioGain(float argGain);
		public abstract float GetAudioGain();
		public abstract bool GetIsAudioMuted();
        public abstract int GetAudioTrack();

		/* MediaInfoInterface */
		public abstract long GetContentDurationInMilliseconds();
		public abstract String GetHighestQualityResolutionAndFramerate();
		public abstract int GetNumberOfAudioTracks();
		public abstract String GetCurrentResolutionAndFramerate();
		public abstract EventTypes GetEventType();
		
		/* PerformanceInterface */
		public abstract float GetAverageBitrateInMbps();
		protected StatisticsBase _clearVRCoreWrapperStatistics = null;

		/* DebugInterface */
		// All debug interface methods are declared as virtual at the end of this class. Please do not use them as they can change without notice.

		/* Other */
		protected ClearVREvents _clearVREvents = new ClearVREvents();
        protected bool isFirstFrame = true;
        protected bool isPlayerShuttingDown = false; // Set to true in StopClearVR() when shutting down the application.
		protected Vector3 trackingTransformOrientation;
		protected Vector3 trackingTransformPosition;
		protected float[] orientationData = new float[9]; // 0 = yaw, 1 = pitch, 2 = roll, 3 - 8 is unused for now

		/* Delegates and magic (static) strings signaling mesh created/texture created information from the NRP to Unity. */
		public delegate void CbMeshCreatedDelegate (ref MeshCreatedStruct argMeshCreatedStruct);
		public delegate void CbMeshDestroyedDelegate (ref MeshDestroyedStruct argMeshDestroyedStruct);
		public delegate void CbMeshTextureAvailableDelegate (ref MeshTextureAvailableStruct argMeshTextureAvailableStruct);

		/* These compiler flags are required for IL2CPP, note that the method must be static */
        [MonoPInvokeCallback(typeof(CbMeshCreatedDelegate))]
        [MethodImpl(MethodImplOptions.Synchronized)]
		public static void CbMeshCreated(ref MeshCreatedStruct argMeshCreatedStruct) {
			asyncMeshOperationsQueue.Enqueue(new AsyncMeshOperation<MeshCreatedStruct>(AsyncMeshOperationTypes.MeshCreated, argMeshCreatedStruct));
		}
        
		[MonoPInvokeCallback(typeof(CbMeshDestroyedDelegate))]
        [MethodImpl(MethodImplOptions.Synchronized)]
		public static void CbMeshDestroyed(ref MeshDestroyedStruct argMeshDestroyedStruct) {
			asyncMeshOperationsQueue.Enqueue(new AsyncMeshOperation<MeshDestroyedStruct>(AsyncMeshOperationTypes.MeshDestroyed, argMeshDestroyedStruct));
		}

        [MonoPInvokeCallback(typeof(CbMeshTextureAvailableDelegate))]
        [MethodImpl(MethodImplOptions.Synchronized)]
		public static void CbMeshTextureAvailable(ref MeshTextureAvailableStruct argMeshTextureAvailableStruct) {
			asyncMeshOperationsQueue.Enqueue(new AsyncMeshOperation<MeshTextureAvailableStruct>(AsyncMeshOperationTypes.MeshTextureAvailable, argMeshTextureAvailableStruct));
		}

        internal ClearVRMeshManager _clearVRMeshManager = new ClearVRMeshManager();

		/* This UnityEvent channel is used with the MediaPlayer* classes and the ClearVRPlayer monobehavior script. */
		protected class ClearVREvents : UnityEngine.Events.UnityEvent<MediaPlayerBase, ClearVREvent> {
		}

		protected PlatformOptionsBase _platformOptions;
		public PlatformOptionsBase platformOptions {
			get {return _platformOptions; }
		}

		protected RenderModes _renderMode = RenderModes.Native;
		protected long frameCount = 0;
		protected static string _clearVRCoreVersionString = "Unknown";
		protected Queue<ClearVREvent> clearVREventToInvokeQueue = new Queue<ClearVREvent>();
		/// <summary>
		/// Creates a Queue that is effectively used as a pre-allocated byte array queue to prevent random crashes when trying to allocate memory on the non-Unity Main thread
		/// </summary>
	 	protected Shader clearVRMeshShader = null;
		protected ContentItem contentItem;
		protected Queue<ContentItem> switchContentContentItemQueue = new Queue<ContentItem>();
		protected bool isMediaInfoParsed = false;
		protected bool isRenderingInStereoscopicMode = false; /* helper boolean because we need to know this every frame */
		protected List<ClearVRAsyncRequest> _clearVRAsyncRequests = new List<ClearVRAsyncRequest>();
		protected List<ClearVRAsyncRequest> _clearVRAsyncRequestsWithoutCallback = new List<ClearVRAsyncRequest>();
		public List<ClearVRAsyncRequest> clearVRAsyncRequests {
			get {
				return _clearVRAsyncRequests;
			}
		}
		public List<ClearVRAsyncRequest> clearVRAsyncRequestsWithoutCallback {
			get {
				return _clearVRAsyncRequestsWithoutCallback;
			}
		}

		protected enum States {
			Undefined,
			Uninitialized,
			Initializing,
			Initialized,
			PreparingCore,
			CorePrepared,
			PreparingContentForPlayout,
			ContentPreparedForPlayout,
			Buffering,
			Playing,
			Pausing,
			Paused,
			Seeking,
			SwitchingContent,
			Finished,
			Stopping,
			Stopped,
		}
		/* Holds a local reference to our native texture */
		protected FallbackLayout fallbackLayout = new FallbackLayout();

        [MethodImpl(MethodImplOptions.NoInlining)]
        public string GetCurrentMethod() {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);
            return sf.GetMethod().Name;
        }


        protected String GetCurrentStateAsString() {
			return _state.ToString();
		}

		public bool GetIsMediaInfoParsed() {
			return isMediaInfoParsed && GetIsInitialized();
		}

		public bool GetIsInitialized() {
			return (_state != States.Uninitialized &&
					_state != States.Initializing &&
					_state != States.Stopping && 
					_state != States.Stopped);
		}

		public bool GetIsInPlayingState() {
			return (_state == States.Playing);
		}

		public bool GetIsInPausedState() {
			return (_state == States.Paused);
		}
		
		public bool GetIsInFinishedState() {
		        return (_state == States.Finished);
		}
		public bool GetIsInStoppedState() {
			return (_state == States.Stopped);
		}

		/* The player is regarded busy if it has started preparing the core */
		public bool GetIsPlayerBusy() {
			return (GetIsInitialized() && 
					_state != States.PreparingCore);
		}

		public bool GetIsInPausingOrPausedState() {
			return (_state == States.Pausing || 
					GetIsInPausedState());
		}

		public StatisticsBase clearVRCoreWrapperStatistics {
			get {return _clearVRCoreWrapperStatistics; }
		}

        /// <summary>
        /// These are asynchronous operation types on a ClearVRMesh, requested by the native renderer plugin from C++.
        /// </summary>
        protected enum AsyncMeshOperationTypes {
			MeshCreated,
			MeshDestroyed,
			MeshTextureAvailable

		}
		/// <summary>
		/// Generic AsyncMeshOperation class.
		/// </summary>		
		protected abstract class AsyncMeshOperation {
			public AsyncMeshOperationTypes asyncMeshOperationType;
		}

		/// <summary>
		/// An AsyncMeshOperation object with a Type T specific payload. The payload is typically a struct.
		/// </summary>
		/// <typeparam name="T">The payload to carry, typically a struct.</typeparam>
		protected class AsyncMeshOperation<T> : AsyncMeshOperation {
			public T genericPayload;
			public AsyncMeshOperation(AsyncMeshOperationTypes argAsyncMeshOperationType, T argPayload) {
				asyncMeshOperationType = argAsyncMeshOperationType;
				genericPayload = argPayload;
			}
		}
		/// <summary>
		/// This queue is pop-ed one by one on the main renderer thread.
		/// Note that this must be a static Queue as it will be accessed from a C++ callback/delegate. 
		/// For now, we safely assume that this queue will be drained of any pending actions while shutting down a mediaPlayer instance. To be sure, the queue is flushed during initialization of a mediaPLayer as well.
		/// </summary>
		/// <typeparam name="AsyncMeshOperation"></typeparam>
		/// <returns></returns>
		protected static Queue<AsyncMeshOperation> asyncMeshOperationsQueue = new Queue<AsyncMeshOperation>();

		protected States _state = States.Uninitialized;
		protected RenderModes renderMode = RenderModes.Native;
		protected byte[] licenseFileBytes;
		protected ContentFormat _contentFormat = ContentFormat.Unknown;
		protected long stoppedAfterApplicationLostFocusContentTimeInMilliseconds = -1;
		protected bool _isViewportTrackingEnabled = true;
	
		public MediaPlayerBase(PlatformOptionsBase argPlatformOptions, UnityEngine.Events.UnityAction<MediaPlayerBase,ClearVREvent> argCbClearVREvent) {
			/* Attach ClearVREvents event listener. This will relay any event to the ClearVRPlayer parent which in turn can relay any message to the base constructor class. */
			asyncMeshOperationsQueue.Clear();
			if(argCbClearVREvent != null) {
				_clearVREvents.AddListener(argCbClearVREvent);
			}
		}

		protected bool GetIsRenderingInStereoscopicMode() {
			return isRenderingInStereoscopicMode;
		}

		protected void UpdateClearVREventsToInvokeQueue() {
			frameCount++;
			if(clearVREventToInvokeQueue.Count > 0) {
				ClearVREvent clearVREvent = clearVREventToInvokeQueue.Dequeue();
				InvokeEvent(clearVREvent);
			}
            if (asyncMeshOperationsQueue.Count > 0) {
                HandleAsyncMeshOperationsQueue();
            }
		}

		public virtual RenderModes GetRenderMode() {
			return _renderMode;
		}

		public virtual bool SetMainColor(Color argNewColor) {
            return _clearVRMeshManager.SetMainColor(argNewColor);
		}

		public void EnableOrDisableViewportTracking(bool argIsEnabledOrDisabled) {
			_isViewportTrackingEnabled = argIsEnabledOrDisabled;
		}

		protected ClearVRAsyncRequest InvokeClearVRCoreWrapperInvalidStateEvent(String argMessage, bool argIsCalledAsynchronous, RequestTypes argRequestType) {
			ClearVRMessage clearVRMessage = new ClearVRMessage(ClearVRMessageTypes.Warning, 
						ClearVRMessageCodes.ClearVRCoreWrapperInvalidState, 
						argMessage, 
					ClearVRResult.Failure);
			
			if(argIsCalledAsynchronous) {
				ClearVRAsyncRequest clearVRAsyncRequest = new ClearVRAsyncRequest(argRequestType);
				ScheduleClearVREvent(new ClearVREvent(ClearVREventTypes.GenericMessage,
					new ClearVRAsyncRequestResponse(clearVRAsyncRequest.requestType, 
						clearVRAsyncRequest.requestId), 
						clearVRMessage
					)
				);
				return clearVRAsyncRequest;
			} else {
				ScheduleClearVREvent(new ClearVREvent(ClearVREventTypes.GenericMessage, 
					clearVRMessage));
			}
			return new ClearVRAsyncRequest(argRequestType);
		}

		protected ClearVRAsyncRequest InvokeAPINotSupportedOnThisPlatformEvent(String argMessage, bool argIsCalledAsynchronous, RequestTypes argRequestType) {
			ClearVRMessage clearVRMessage = new ClearVRMessage(ClearVRMessageTypes.FatalError, 
						ClearVRMessageCodes.APINotSupportedOnThisPlatform, 
						argMessage, 
					ClearVRResult.Failure);
			
			if(argIsCalledAsynchronous) {
				ClearVRAsyncRequest clearVRAsyncRequest = new ClearVRAsyncRequest(argRequestType);
				ScheduleClearVREvent(new ClearVREvent(ClearVREventTypes.GenericMessage,
					new ClearVRAsyncRequestResponse(clearVRAsyncRequest.requestType, 
						clearVRAsyncRequest.requestId),
						clearVRMessage
					)
				);
				return clearVRAsyncRequest;
			} else {
				ScheduleClearVREvent(new ClearVREvent(ClearVREventTypes.GenericMessage, 
					clearVRMessage));
			}
			return new ClearVRAsyncRequest(argRequestType);
		}

		public virtual PlatformOptionsBase GetPlatformOptions() {
			return _platformOptions;
		}

		public virtual void SetRenderMode(RenderModes argNewRenderMode) {
			bool setMonoOrStereoMode = false; // false = set mono, true = set stereo
			ClearVREvent clearVREvent = ClearVREvent.GetGenericOKEvent(ClearVREventTypes.RenderModeChanged);
			if(GetIsContentFormatStereoscopic()) {
				if(_platformOptions.isVRDevicePresent) {
					if(argNewRenderMode == RenderModes.Monoscopic) {
						setMonoOrStereoMode = false;
					} else if(argNewRenderMode == RenderModes.Stereoscopic) {
						setMonoOrStereoMode = true;
					} else if(argNewRenderMode == RenderModes.Native) {
						setMonoOrStereoMode = true;
					}
				} else {
					if(argNewRenderMode == RenderModes.Monoscopic) {
						setMonoOrStereoMode = false;
					} else if(argNewRenderMode == RenderModes.Stereoscopic) {
						// cannot comply
						clearVREvent.message.Update(ClearVRMessageTypes.Warning, ClearVRMessageCodes.SetRenderModeFailed, "Cannot switch to stereoscopic rendering, no headset detected.", ClearVRResult.Failure);
						setMonoOrStereoMode = false;						
					} else if(argNewRenderMode == RenderModes.Native) {
						setMonoOrStereoMode = false;
					}
				}
			} else {
				if(argNewRenderMode == RenderModes.Monoscopic) {
					setMonoOrStereoMode = false;
				} else if(argNewRenderMode == RenderModes.Stereoscopic) {
					// cannot comply
					clearVREvent.message.Update(ClearVRMessageTypes.Warning, ClearVRMessageCodes.SetRenderModeFailed, "Cannot switch to stereoscopic rendering, content is monoscopic.", ClearVRResult.Failure);
					setMonoOrStereoMode = false;						
				}
			}
            _clearVRMeshManager.EnableOrDisableStereoscopicRendering(setMonoOrStereoMode);
			if(setMonoOrStereoMode) {
				_renderMode = RenderModes.Stereoscopic;
			} else {
				_renderMode = RenderModes.Monoscopic;
			}
			isRenderingInStereoscopicMode = setMonoOrStereoMode;
			ScheduleClearVREvent(clearVREvent);
		}

		/// <summary>
		/// Request content format at ClearVRCore. Note that ContentFormat is something different then RenderMode
		/// </summary>
		public virtual void UpdateContentFormat() {
			String mediaProjectionType = GetClearVRCoreParameter("media.projection_type");
			bool stereoscopicModeActive = (GetClearVRCoreParameter("playback.stereoscopic_mode_active") == "true"); // To fix #2140
			bool oldIsStereoscopic =  GetIsContentFormatStereoscopic();
			ContentFormat oldContentFormat = _contentFormat; // ContentFormat.Unknown by default.
			switch(mediaProjectionType) {
				case "360-cubemap-mono":
					_contentFormat = ContentFormat.Monoscopic360;
					break;
				case "360-cubemap-stereo":
					_contentFormat = ContentFormat.Stereoscopic360;
					break;
				case "180-cubemap-mono":
					_contentFormat = ContentFormat.Monoscopic180;
					break;
				case "180-cubemap-stereo":
					_contentFormat = ContentFormat.Stereoscopic180;
					break;
				case "planar":
					_contentFormat = ContentFormat.Planar;
					break;
				default:
					_contentFormat = ContentFormat.Unknown;
					break;
			}
			bool newIsStereoscopic = GetIsContentFormatStereoscopic() && stereoscopicModeActive;
			if(_contentFormat != oldContentFormat && platformOptions.enableAutomaticRenderModeSwitching) {
				// Force synchronous render mode change. If we would've waited for the callback we would end-up with a blank right eye for one video frame
				if(oldContentFormat == ContentFormat.Unknown) {
					if(newIsStereoscopic) {
						if(platformOptions.isVRDevicePresent) {
							SetRenderMode(RenderModes.Stereoscopic);
						} else {
							// Cannot happen as we force the core into monoscopic mode. Force to monoscopic just in case
							SetRenderMode(RenderModes.Monoscopic);
						}
					} else {
						SetRenderMode(RenderModes.Monoscopic);
					}
				} else {
					// Force synchronous render mode change. If we would've waited for the callback we would end-up with a blank right eye for one video frame
					if(oldIsStereoscopic && !newIsStereoscopic) {
						// We always have to explicitly switch to monoscopic rendering, otherwise the right eye will break down.
						SetRenderMode(RenderModes.Monoscopic);
					} else if (!oldIsStereoscopic && newIsStereoscopic) {
						// FIXME. wait for #1113 to be fixed.
						SetRenderMode(RenderModes.Stereoscopic);
					}
				}
				InvokeEvent(ClearVREvent.GetGenericOKEvent(ClearVREventTypes.ContentFormatChanged));
			}
		}
		public virtual void Rotate(Vector3 argNewEulerAngles) {
            _clearVRMeshManager.Rotate(argNewEulerAngles);
		}

		public virtual void Translate(Vector3 argTranslation, Space argRelativeTo = Space.Self) {
            _clearVRMeshManager.Translate(argTranslation, argRelativeTo);
		}

        public virtual void Translate(Vector3 argTranslation, Transform argRelativeTo) {
            _clearVRMeshManager.Translate(argTranslation, argRelativeTo);
        }

		public virtual GameObject GetActiveClearVRMesh() {
			return _clearVRMeshManager.GetActiveClearVRMesh();
		}

        public virtual void HideOrUnhide(bool argHideOrUnhide) {
            _clearVRMeshManager.HideOrUnhide(argHideOrUnhide);
		}

		public virtual bool GetIsContentFormatStereoscopic() {
			return (_contentFormat == ContentFormat.Stereoscopic180 || _contentFormat == ContentFormat.Stereoscopic360);
		}

		public virtual ContentFormat GetContentFormat() {
			return _contentFormat;
		}

		public virtual void Recenter() {
            Vector3 newOrientation = _platformOptions.trackingTransform.eulerAngles;
            newOrientation.x = 0;
            newOrientation.y = -_platformOptions.trackingTransform.eulerAngles.y;
            newOrientation.z = 0;
            _clearVRMeshManager.Recenter(newOrientation);
		}

		public virtual FallbackLayout GetFallbackLayout() {
			return fallbackLayout;
		}

		public virtual Texture2D GetTexture() {
            return _clearVRMeshManager.nativeTexture;
		}

        Vector3 clearVRMeshEulerAngles;
		/// <summary>
		/// Send camera position to the core library.
		/// Yaw, pitch and roll are defined as specified in https://developer.oculus.com/documentation/pcsdk/latest/concepts/dg-sensor/
		/// Do not forget that x = pitch and y = yaw (instead of x = yaw and y = pitch)
		/// </summary>
		public void UpdateOrientationInfo() {
			switch(_clearVRMeshManager.GetActiveClearVRMeshType()) {
				case ClearVRMeshTypes.Planar:
					// Just send raw, uncompensated position
					trackingTransformPosition = _platformOptions.trackingTransform.position;
					orientationData[0] = trackingTransformPosition.y; // yaw [-180 ... 180]
					orientationData[1] = trackingTransformPosition.x; // pitch [-90 ... 90]
					orientationData[2] = trackingTransformPosition.z; // roll [0]
					break;
				case ClearVRMeshTypes.Cubemap:
				case ClearVRMeshTypes.Cubemap180:
					trackingTransformOrientation = _platformOptions.trackingTransform.eulerAngles;
					clearVRMeshEulerAngles = _clearVRMeshManager.GetEulerAngles();
					orientationData[0] = -(trackingTransformOrientation.y + clearVRMeshEulerAngles.y);
					orientationData[1] = (trackingTransformOrientation.x + clearVRMeshEulerAngles.x);
					orientationData[2] = (trackingTransformOrientation.z + clearVRMeshEulerAngles.z);
					break;
				case ClearVRMeshTypes.Unknown:
				default:
					// This can happen during the first two or three frames when we are still constructing the ClearVRMesh
					// In this case, we do not know the rotation of the ClearVRMesh and we thus cannot calculate the correct sensor orientation.
					// Sending this to the Core would cause it to flush it cores.
					trackingTransformOrientation = _platformOptions.trackingTransform.eulerAngles;
					orientationData[0] = -trackingTransformOrientation.y;
					orientationData[1] = trackingTransformOrientation.x;
					orientationData[2] = trackingTransformOrientation.z;
					break;
			}
		}

        protected bool _isUseOESFastPathEnabled {
            get {
                if (_platformOptions != null) {
                    #if UNITY_ANDROID && !UNITY_EDITOR
                        return ((PlatformOptionsAndroid)_platformOptions).isUseOESFastPathEnabled;
                    #endif
                    #if UNITY_IOS && !UNITY_EDITOR
                        return ((PlatformOptionsIOS)_platformOptions).isUseOESFastPathEnabled;
                    #endif
                    return false;
                }
                return false;
            } set {
                if (_platformOptions != null) {
                    #if UNITY_ANDROID && !UNITY_EDITOR
                        ((PlatformOptionsAndroid)_platformOptions).isUseOESFastPathEnabled = value;
                    #endif
                    #if UNITY_IOS && !UNITY_EDITOR
                        ((PlatformOptionsIOS)_platformOptions).isUseOESFastPathEnabled = value;
                    #endif
                }
            }
        }

        /// <summary>
        /// Load the appropriate shader based on whether Single pass Stereoscopic Rendering (SPSR) has been enabled or not.
        /// Please note that if SPSR is detected and isUseOESFastPathEnabled == true, the latter will be disabled automatically as OES fast path is not supported in SPSR due to limitations in the CG shader language
        /// </summary>
        /// <returns>true if the appropriate shader was found and loaded, false otherwise.</returns>
        protected bool LoadShader() {
            // Load suitable shader
            String shaderName = "ClearVR/ClearVRShader";
            try
            {
                clearVRMeshShader = Shader.Find(shaderName);
                if(clearVRMeshShader == null){
                    throw new Exception(String.Format("[ClearVR] Shader {0} missing from project.", shaderName));
                }
            } catch (Exception e) {
                ScheduleClearVREvent(new ClearVREvent(ClearVREventTypes.UnableToInitializePlayer, ClearVRMessageTypes.FatalError, ClearVRMessageCodes.GenericFatalError, String.Format("Unable to load shader {0}. Not included? Error: {1}", shaderName, e), ClearVRResult.Failure));
                return false;
            }
            return true;
        }


        protected void HandleAsyncMeshOperationsQueue() {
            // First, grab the generic implementation of the AsyncMeshOperation to determine its type.
            // Subsequently, we have to cast to the appropriate AsyncMeshOperation<T>.
            if(asyncMeshOperationsQueue.Count == 0) {
                return;
            }
            AsyncMeshOperation asyncMeshOperationGeneric = asyncMeshOperationsQueue.Dequeue();
            switch(asyncMeshOperationGeneric.asyncMeshOperationType) {
                case AsyncMeshOperationTypes.MeshCreated:
                    // Create a new ClearVRMesh
                    ConstructClearVRMesh((MeshCreatedStruct) ((AsyncMeshOperation<MeshCreatedStruct>) (Convert.ChangeType(asyncMeshOperationGeneric, typeof(AsyncMeshOperation<MeshCreatedStruct>)))).genericPayload);
                    break;
                case AsyncMeshOperationTypes.MeshDestroyed:
                    // Mark the ClearVRMesh mesh as ready to be destroyed.
                    MeshDestroyedStruct meshDestroyedStruct = (MeshDestroyedStruct) ((AsyncMeshOperation<MeshDestroyedStruct>) (Convert.ChangeType(asyncMeshOperationGeneric, typeof(AsyncMeshOperation<MeshDestroyedStruct>)))).genericPayload;
                    int index = _clearVRMeshManager.GetClearVRMeshIndexBySignature(meshDestroyedStruct.signature);
                    bool areAllClearVRMeshesReadyForDestruction = true;
                    if(index >= 0) {
                        _clearVRMeshManager.SetIsClearVRMeshReadyForDestruction(true, index);
                        for(int i = 0; i < _clearVRMeshManager.numberOfClearVRMeshes; i++) {
                            if(!_clearVRMeshManager.GetIsReadyForDestruction(i)) {
                                areAllClearVRMeshesReadyForDestruction = false;
                                break;
                            }
                        }
                    } else { 
                        areAllClearVRMeshesReadyForDestruction = true;
                    }
                    // This must ONLY be called for when we are destroying the mediaPlayer. 
                    // If a TCS is marked ready for destruction because of a switchContent(), then destruction is taken care of elsewhere.
                    if(areAllClearVRMeshesReadyForDestruction && isPlayerShuttingDown) { 
						_clearVRMeshManager.Stop();
                    }
                    break;
                case AsyncMeshOperationTypes.MeshTextureAvailable:
                    // Setup ClearVRMesh texture!
                    SetupClearVRMeshTexture((MeshTextureAvailableStruct) ((AsyncMeshOperation<MeshTextureAvailableStruct>) (Convert.ChangeType(asyncMeshOperationGeneric, typeof(AsyncMeshOperation<MeshTextureAvailableStruct>)))).genericPayload);
                    break;
                default:
                    throw new Exception(String.Format("[ClearVR] Async Mesh Operation Type {0} Not implemented.", asyncMeshOperationGeneric.asyncMeshOperationType));
            }
        }

        /// <summary>
        /// Construct ClearVRMesh
        /// This mesh will hold the video texture and its UVs will be updated for every frame.
        /// Note that the actual mesh is calculated in the ClearVRNativeRendererPlugin, the Mesh is mainly
        /// used as a simple interface to use this procedurally generated mesh.
        /// </summary>
        public virtual ClearVRMeshBase ConstructClearVRMesh(MeshCreatedStruct argMeshCreatedStruct) {
			// System.Console.Write("[Unity] ConstructClearVRMesh and NOT YET show it. TCSID: " +  argMeshCreatedStruct.signature + "\n");
            ClearVRMeshBase clearVRMesh = _clearVRMeshManager.AddClearVRMeshBase(argMeshCreatedStruct, _platformOptions);
			if(clearVRMesh == null) {
				return null;
			}
            fallbackLayout.fallbackLayoutIdentifier = argMeshCreatedStruct.fallbackLayoutIdentifier;
            fallbackLayout.overlapX = argMeshCreatedStruct.overlapX;
            fallbackLayout.overlapY = argMeshCreatedStruct.overlapY;
            fallbackLayout.fallbackCubefaceInfoStructs = new List<FallbackCubefaceInfoStruct>();
            long fallbackCubefaceInfoStructsIntPtrLong = argMeshCreatedStruct.fallbackCubefaceInfoStructsIntPtr.ToInt64();
            for (int i = 0 ; i < 6 ; ++i) {
                fallbackLayout.fallbackCubefaceInfoStructs.Add ((FallbackCubefaceInfoStruct)Marshal.PtrToStructure (new IntPtr(fallbackCubefaceInfoStructsIntPtrLong), typeof (FallbackCubefaceInfoStruct)));
                fallbackCubefaceInfoStructsIntPtrLong += Marshal.SizeOf (typeof (FallbackCubefaceInfoStruct)) ;
            }

            clearVRMesh.Initialize(new Vector3(0, 0, 0),
                                new Vector3(0, 0, 0),
                                _platformOptions,
                                argMeshCreatedStruct,
                                null);

            isFirstFrame = false;
            return clearVRMesh;
        }

        private void SetupClearVRMeshTexture(MeshTextureAvailableStruct argMeshTextureAvailableStruct) {
			//  System.Console.Write("[Unity] SetupClearVRMeshTexture and show it, texture id: "+  argMeshTextureAvailableStruct.textureId + ", TCSID: " +  argMeshTextureAvailableStruct.signature + "\n");
            _clearVRMeshManager.RemoveCurrentClearVRMesh();

            int newIndex = _clearVRMeshManager.GetClearVRMeshIndexBySignature(argMeshTextureAvailableStruct.signature);
            _clearVRMeshManager.SetActiveClearVRMeshSignature(argMeshTextureAvailableStruct.signature);
            // Attach shader to the mesh renderer's material and set its texture
            _clearVRMeshManager.SetShader(clearVRMeshShader, newIndex);

            _clearVRMeshManager.RecreateNativeTexture(newIndex, argMeshTextureAvailableStruct, _platformOptions);

            // Enable MeshRenderer (if it has not explicitly been hidden) because by now our texture is available and our shader properly set-up.
            _clearVRMeshManager.EnableOrDisableMeshRenderer(true, newIndex);
            _clearVRMeshManager.HideOrUnhide(_clearVRMeshManager.isVisible, newIndex);
            // Since v4.0, we re-determine the content format after a successful content switch as it is now possible to dynamically switch between stereoscopic and monoscopic content.
            // If (and only if) the ContentFormat changed, a ContentFormatChanged event is fired notifying the application.
            UpdateContentFormat();
            InvokeEvent(ClearVREvent.GetGenericOKEvent(ClearVREventTypes.FirstFrameRendered));
        }

		public virtual String GetDeviceAppId() {
			return "";
		}


        /// <summary>
        /// Request the frame width of the decoded video frame. Note that this is NOT the same as the effective video resolution!
        /// </summary>
        /// <returns>The decoder video frame width in pixels or -1 if not yet known</returns>
        public virtual int GetDecoderVideoFrameWidth() {
            return _clearVRMeshManager.GetDecoderFrameWidth();
        }

        /// <summary>
        /// Request the frame height of the decoded video frame. Note that this is NOT the same as the effective video resolution!
        /// </summary>
        /// <returns>The decoder video frame height in pixels or -1 if not yet known</returns>
        public virtual int GetDecoderVideoFrameHeight() {
            return _clearVRMeshManager.GetDecoderFrameHeight();
        }

		public virtual void CbClearVRCoreWrapperRequestCompleted(ClearVRAsyncRequestResponse argClearVRAsyncRequestResponse, ClearVRMessage argClearVRMessage) {
			ClearVREvent clearVREvent = null;
			bool isSuccess = (argClearVRMessage.result == ClearVRResult.Success);
			switch(argClearVRAsyncRequestResponse.requestType) {
				case RequestTypes.Unknown:
					break;
				case RequestTypes.Initialize:
					// Note that we ONLY set the state in case of success, otherwise our state-model would get desynchronized.
					if(isSuccess) {
						// Only by this point we are *really* initialized
						SetState(States.CorePrepared, argClearVRAsyncRequestResponse, argClearVRMessage);
					} else {
						// This means that we have to forward a custom event that signals that we were unable to initialize our player.
						clearVREvent = new ClearVREvent(ClearVREventTypes.UnableToInitializePlayer, argClearVRAsyncRequestResponse, argClearVRMessage);
					}
					break;
				case RequestTypes.ParseMediaInfo:
					clearVREvent = new ClearVREvent(ClearVREventTypes.MediaInfoParsed, argClearVRAsyncRequestResponse, argClearVRMessage);
					isMediaInfoParsed = isSuccess;
					break;
				case RequestTypes.PrepareContentForPlayout:
					// Note that we ONLY set the state in case of success, otherwise our state-model would get desynchronized.
					if(isSuccess) {
						SetState(States.ContentPreparedForPlayout, argClearVRAsyncRequestResponse, argClearVRMessage);
					} else {
						// This means that we have to forward a custom event that signals that we were unable to initialize our player.
						clearVREvent = new ClearVREvent(ClearVREventTypes.UnableToInitializePlayer, argClearVRAsyncRequestResponse, argClearVRMessage);
					}
					break;
				case RequestTypes.Start:
					clearVREvent = new ClearVREvent(ClearVREventTypes.GenericMessage, argClearVRAsyncRequestResponse, argClearVRMessage);
					break;
				case RequestTypes.Pause:
					clearVREvent = new ClearVREvent(ClearVREventTypes.GenericMessage, argClearVRAsyncRequestResponse, argClearVRMessage);
					break;
				case RequestTypes.Unpause:
					clearVREvent = new ClearVREvent(ClearVREventTypes.GenericMessage, argClearVRAsyncRequestResponse, argClearVRMessage);
					break;
				case RequestTypes.Seek:
					clearVREvent = new ClearVREvent(ClearVREventTypes.GenericMessage, argClearVRAsyncRequestResponse, argClearVRMessage);
					break;
				case RequestTypes.Stop:
                    if (stoppedAfterApplicationLostFocusContentTimeInMilliseconds != -1 && contentItem != null) {
						contentItem.startPositionInMilliseconds = stoppedAfterApplicationLostFocusContentTimeInMilliseconds;
						argClearVRMessage.message = String.Format("{0};{1}", argClearVRMessage.message, contentItem.Serialize());
					}
					// End of playback, we are done shutting down the core libraries
					if(isSuccess) {
						// all is well
					} else {
						// Note that we internally take care to handle clean-up correctly. 
						// This merely signals that something unexpectedly went wrong.
						if(argClearVRMessage.GetIsFatalError()) {
							UnityEngine.Debug.LogError("[ClearVR] A fatal error was reported while shutting down, but it is safe to continue.");
						} else {
							// Harmless warning.
						}
					}
					// Clean-up last data
					CleanUpAfterStopped();
					// Notice how we relay the response to a request through a state change rather than a dedicated event.
					SetState(States.Stopped, argClearVRAsyncRequestResponse, argClearVRMessage);

					break;
				case RequestTypes.SwitchAudioTrack:
					clearVREvent = new ClearVREvent(ClearVREventTypes.AudioTrackSwitched, argClearVRAsyncRequestResponse, argClearVRMessage);
					break;
				case RequestTypes.SwitchContent:
					if(isSuccess) {
						contentItem = switchContentContentItemQueue.Dequeue();
					} else{
						switchContentContentItemQueue.Dequeue();
					}
					clearVREvent = new ClearVREvent(ClearVREventTypes.ContentSwitched, argClearVRAsyncRequestResponse, argClearVRMessage);
					break;
				case RequestTypes.ChangeStereoMode:
					clearVREvent = new ClearVREvent(ClearVREventTypes.StereoModeSwitched, argClearVRAsyncRequestResponse, argClearVRMessage);
					break;
				case RequestTypes.PrewarmCache:
					clearVREvent = new ClearVREvent(ClearVREventTypes.PrewarmCacheCompleted, argClearVRAsyncRequestResponse, argClearVRMessage);
					break;
				default:
					break;
			}

			if(clearVREvent != null)
				ScheduleClearVREvent(clearVREvent);
		}

		public virtual void CbClearVRCoreWrapperMessage(ClearVRMessage argClearVRMessage) { 
			ClearVREvent clearVREvent = new ClearVREvent(ClearVREventTypes.GenericMessage, argClearVRMessage);
			bool isSuccess = (argClearVRMessage.result == ClearVRResult.Success);
			switch(clearVREvent.message.type) {
				case ClearVRMessageTypes.FatalError:
					break;
				case ClearVRMessageTypes.Warning:
					break;
				case ClearVRMessageTypes.Info:
                    // First, see if this is a state changed message. These messages need special handling.
                    switch (argClearVRMessage.code) {
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateUninitialized:
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateInitializing:
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateInitialized:
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateRunning:
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStatePausing:
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStatePaused:
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateBuffering:
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateSeeking:
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateSwitchingContent:
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateFinished:
                        case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateStopped:
                            ClearVRCoreWrapperStateChanged(argClearVRMessage);
                            return; // make sure we do not emit an event, that is being taken care of by ClearVRCoreWrapperStateChanged() 
                        default:
                            break;
                    }

                    /* Parse harmless info messages */
                    switch (argClearVRMessage.code) {
						case (int) ClearVRMessageCodes.ClearVRCoreWrapperGenericInfo:
							break;
						case (int) ClearVRMessageCodes.ClearVRCorWrapperOpenGLVersionInfo:
							break;
						case (int) ClearVRMessageCodes.ClearVRCoreWrapperVideoDecoderCapabilities:
							break;
						case (int) ClearVRMessageCodes.GenericOK:
							break;
						case (int) ClearVRMessageCodes.ClearVRCoreWrapperAudioTrackChanged:
							if(isSuccess) {
								/* Update currently active audio decoder and audio playback engine */
								int newAudioTrackIndex = 0;
								AudioDecoder newAudioDecoder;
								AudioPlaybackEngine newAudioPlaybackEngine;
								if(argClearVRMessage.ParseAudioTrackChanged(out newAudioTrackIndex, out newAudioDecoder, out newAudioPlaybackEngine)) {
									_platformOptions.audioDecoder = newAudioDecoder;
									_platformOptions.audioPlaybackEngine = newAudioPlaybackEngine;
								}
							}
							break;
						case (int) ClearVRMessageCodes.ClearVRCoreWrapperStereoscopicModeChanged:
							// This involves rendering specific operations (a call to SetRenderMode()) which needs to be performaned from the Unity main thread.
							// ClearVRPlayer will take care of this.
							break;
						default:
							break;
					}
				break;
			}

			ScheduleClearVREvent(clearVREvent);
		}

		public virtual void ClearVRCoreWrapperStateChanged(ClearVRMessage argClearVRMessage) {
			States newState = States.Undefined;
			/* Please note that these are ClearVRCore states, there are more events in this Unity SDK which are NOT handled by this callback. */
			switch (argClearVRMessage.code) {
				/* These state changes in the Core have their own dedicated callback handlers and MUST be ignored here */
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateUninitialized:
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateInitializing:
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateInitialized:
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateStopped:
					return;
				/* These _state changes in the Core MUST be handled here */
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateRunning:
					newState = States.Playing;
					break;
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStatePausing:
					newState = States.Pausing;
					break;
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStatePaused:
					newState = States.Paused;
					break;
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateBuffering:
					newState = States.Buffering;
					break;
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateSeeking:
					newState = States.Seeking;
					break;
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateSwitchingContent:
					newState = States.SwitchingContent;
					break;
				case (int)ClearVRMessageCodes.ClearVRCoreWrapperClearVRCoreStateFinished:
					newState = States.Finished;
					break;
				default:
					// This should've covered all Core States
					return;
			}
			SetState(newState, ClearVRMessage.GetGenericOKMessage());					
		}

		public void ScheduleClearVRAsyncRequest(ClearVRAsyncRequest argClearVRAsyncRequest, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			argClearVRAsyncRequest.UpdateActionAndOptionalArguments(argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
			_clearVRAsyncRequests.Add(argClearVRAsyncRequest);
		}

		public ClearVRAsyncRequest ScheduleClearVRAsyncRequestWithoutCallback(ClearVRAsyncRequest argClearVRAsyncRequest) {
			if(argClearVRAsyncRequest != null)
				_clearVRAsyncRequestsWithoutCallback.Add(argClearVRAsyncRequest);
			return argClearVRAsyncRequest;
		}
		
		protected virtual void SetState(States argNewState, ClearVRAsyncRequestResponse argClearVRAsyncRequestResponse, ClearVRMessage argClearVRMessage) {
			ScheduleClearVREvent(new ClearVREvent(ConvertStateToClearVREventType(argNewState), argClearVRAsyncRequestResponse, argClearVRMessage));
		}

		protected virtual void SetState(States argNewState, ClearVRMessage argClearVRMessage) {
			ScheduleClearVREvent(new ClearVREvent(ConvertStateToClearVREventType(argNewState), argClearVRMessage));
		}

		/* Prepare core */
		public virtual ClearVRAsyncRequest PrepareCore() {
			return ScheduleClearVRAsyncRequestWithoutCallback(_PrepareCore(false));
		}

		/* StartPlayout */
		public virtual ClearVRAsyncRequest StartPlayout() {
			return ScheduleClearVRAsyncRequestWithoutCallback(_StartPlayout(false));
		}

		public void StartPlayout(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_StartPlayout(true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

		/* Pause */
		public virtual ClearVRAsyncRequest Pause() {
			return ScheduleClearVRAsyncRequestWithoutCallback(_Pause(false));
		}
		public void Pause(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_Pause(true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}
		
		/* Unpause */
		public virtual ClearVRAsyncRequest Unpause() {
			return ScheduleClearVRAsyncRequestWithoutCallback(_Unpause(false));
		}
		public void Unpause(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_Unpause(true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

        public virtual ClearVRAsyncRequest _TogglePause(bool argIsCalledAsynchronous) {
            if(_state == States.Paused) {
                return Unpause();
            }else if(_state == States.Playing || _state == States.Pausing) {
                return Pause();
            }
            return InvokeClearVRCoreWrapperInvalidStateEvent(String.Format("Cannot {0} in state {1}", _state == States.Paused ? "Unpause" : "Pause", GetCurrentStateAsString()), argIsCalledAsynchronous, _state == States.Paused ? RequestTypes.Pause : RequestTypes.Unpause);
        }

		/* Toggle Pause/Unpause */
		public virtual ClearVRAsyncRequest TogglePause() {
			return ScheduleClearVRAsyncRequestWithoutCallback(_TogglePause(false));
		}

		public void TogglePause(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_TogglePause(true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

		/* Parse media info */
		public virtual ClearVRAsyncRequest ParseMediaInfo(String argManifestUrl) {
			return ScheduleClearVRAsyncRequestWithoutCallback(_ParseMediaInfo(argManifestUrl, false));
		}
		public void ParseMediaInfo(String argManifestUrl, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_ParseMediaInfo(argManifestUrl, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

		/* Set stereo mode */
		public virtual ClearVRAsyncRequest SetStereoMode(bool argStereo) {
			return ScheduleClearVRAsyncRequestWithoutCallback(_SetStereoMode(argStereo, false));
		}
		public void SetStereoMode(bool argStereo, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_SetStereoMode(argStereo, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}
		/* Set stereo mode */
		public virtual ClearVRAsyncRequest PrewarmCache(String argManifestUrl, int argInitialPositionInMilliseconds, long argFlags) {
			return ScheduleClearVRAsyncRequestWithoutCallback(_PrewarmCache(argManifestUrl, argInitialPositionInMilliseconds, argFlags, false));
		}
		public void PrewarmCache(String argManifestUrl, int argInitialPositionInMilliseconds, long argFlags, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_PrewarmCache(argManifestUrl, argInitialPositionInMilliseconds, argFlags, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}
		/* Set audio track mode */
        public virtual ClearVRAsyncRequest SetAudioTrack(int argIndex) {
			return ScheduleClearVRAsyncRequestWithoutCallback(_SetAudioTrack(argIndex, null, null, false));
		}
		public virtual ClearVRAsyncRequest SetAudioTrack(int argIndex, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine) {
			return ScheduleClearVRAsyncRequestWithoutCallback(_SetAudioTrack(argIndex, argAudioDecoder, argAudioPlaybackEngine, false));
		}

        public virtual void SetAudioTrack(int argIndex, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_SetAudioTrack(argIndex, null, null, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

        public virtual void SetAudioTrack(int argIndex, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_SetAudioTrack(argIndex, argAudioDecoder, argAudioPlaybackEngine, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

		/* Switch content */

        public virtual ClearVRAsyncRequest SwitchContent(ContentItem argContentItem) {
			return ScheduleClearVRAsyncRequestWithoutCallback(_SwitchContent(argContentItem, null, null, false));
		}

		public virtual ClearVRAsyncRequest SwitchContent(ContentItem argContentItem, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine) {
			return ScheduleClearVRAsyncRequestWithoutCallback(_SwitchContent(argContentItem, argAudioDecoder, argAudioPlaybackEngine, false));
		}

        public virtual void SwitchContent(ContentItem argContentItem, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_SwitchContent(argContentItem, null, null, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

        public virtual void SwitchContent(ContentItem argContentItem, AudioDecoder argAudioDecoder, AudioPlaybackEngine argAudioPlaybackEngine, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_SwitchContent(argContentItem, argAudioDecoder, argAudioPlaybackEngine, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

		/* Prepare content for playout */
		public virtual ClearVRAsyncRequest PrepareContentForPlayout(ContentItem argContentItem) {
			return ScheduleClearVRAsyncRequestWithoutCallback(_PrepareContentForPlayout(argContentItem, false));
		}

		public void PrepareContentForPlayout(ContentItem argContentItem, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_PrepareContentForPlayout(argContentItem, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

		/* Seek */
		public virtual ClearVRAsyncRequest Seek(long argNewPositionInMilliseconds) {
			return ScheduleClearVRAsyncRequestWithoutCallback(_Seek(argNewPositionInMilliseconds, false));
		}

		public void Seek(long argNewPositionInMilliseconds, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_Seek(argNewPositionInMilliseconds, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

		/* Stop */
		public virtual ClearVRAsyncRequest Stop() {
			return ScheduleClearVRAsyncRequestWithoutCallback(_Stop(false, false));
		}
		
        public void Stop(Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_Stop(false, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}

		public virtual ClearVRAsyncRequest Stop(bool argStoppedAfterApplicationLostFocus) {
			return ScheduleClearVRAsyncRequestWithoutCallback(_Stop(argStoppedAfterApplicationLostFocus, false));
		}
		
        public void Stop(bool argStoppedAfterApplicationLostFocus, Action<ClearVREvent, ClearVRPlayer> argCbClearVRAsyncRequestResponseReceived, params object[] argOptionalArguments) {
			ScheduleClearVRAsyncRequest(_Stop(argStoppedAfterApplicationLostFocus, true), argCbClearVRAsyncRequestResponseReceived, argOptionalArguments);
		}
		
        protected ClearVREventTypes ConvertStateToClearVREventType(States argNewState) {
			_state = argNewState;
			ClearVREventTypes clearVREventType = ClearVREventTypes.None;
			switch(argNewState) {
				case States.Undefined:
					break;
				case States.Uninitialized:
					clearVREventType = ClearVREventTypes.StateChangedUninitialized;
					break;
				case States.Initializing:
					clearVREventType = ClearVREventTypes.StateChangedInitializing;
					break;
				case States.Initialized:
					clearVREventType = ClearVREventTypes.StateChangedInitialized;
					break;
				case States.PreparingCore:
					clearVREventType = ClearVREventTypes.StateChangedPreparingCore;
					break;
				case States.CorePrepared:
					clearVREventType = ClearVREventTypes.StateChangedCorePrepared;
					break;
				case States.PreparingContentForPlayout:
					clearVREventType = ClearVREventTypes.StateChangedPreparingContentForPlayout;
					break;
				case States.ContentPreparedForPlayout:
					clearVREventType = ClearVREventTypes.StateChangedContentPreparedForPlayout;
					break;
				case States.Buffering:
					clearVREventType = ClearVREventTypes.StateChangedBuffering;
					break;
				case States.Playing:
					clearVREventType = ClearVREventTypes.StateChangedPlaying;
					break;
				case States.Pausing:
					clearVREventType = ClearVREventTypes.StateChangedPausing;
					break;
				case States.Paused:
					clearVREventType = ClearVREventTypes.StateChangedPaused;
					break;
				case States.Seeking:
					clearVREventType = ClearVREventTypes.StateChangedSeeking;
					break;
				case States.SwitchingContent:
					clearVREventType = ClearVREventTypes.StateChangedSwitchingContent;
					break;
				case States.Finished:
					clearVREventType = ClearVREventTypes.StateChangedFinished;
					break;
				case States.Stopping:
					clearVREventType = ClearVREventTypes.StateChangedStopping;
					break;
				case States.Stopped:
					clearVREventType = ClearVREventTypes.StateChangedStopped;
					break;
			}
			if(clearVREventType.Equals(ClearVREventTypes.None))
				throw new Exception(String.Format("[ClearVR] state -> ClearVREvent conversion not implemented for state {0}.", argNewState));
			return clearVREventType;
		}

		public void ScheduleClearVREvent(ClearVREvent argClearVREvent) {
			clearVREventToInvokeQueue.Enqueue(argClearVREvent);
		}

		protected void InvokeEvent(ClearVREvent argClearVREvent) {
			_clearVREvents.Invoke(this, argClearVREvent);
		}

        public virtual void EndOfFrame() {
            // intentionally left empty
        }

        public virtual void Destroy() {
			if(_clearVRCoreWrapperStatistics != null) {
				_clearVRCoreWrapperStatistics.Destroy();
				_clearVRCoreWrapperStatistics = null;
			}
		}

	}
}