﻿//
// A special thanks goes out to Joey from Stage for his invaluable contribution to this shader.
//

Shader "ClearVR/ClearVRShader" {
    Properties {
        _MainTex ("Texture", 2D) = "black" {}
		_Color("Main Color", Color) = (1,1,1,1)
		[Toggle(STEREO_CUSTOM_UV)] Stereo("Stereo Mode", Float) = 0
		[Toggle(USE_OES_FAST_PATH)] Stereo("OES Fast Path", Float) = 0
        [HideInInspector] _SrcBlend ("__src", Float) = 1.0
        [HideInInspector] _DstBlend ("__dst", Float) = 0.0
        [HideInInspector] _ZWrite ("__zw", Float) = 1.0
    }
    SubShader {
        Tags { "RenderType" = "Opaque" "Queue" = "Background" "IgnoreProjector" = "True"  }
		LOD 100
		Lighting Off
		Cull Off
        // These values are toggled from code.
		ZWrite [_ZWrite]
        Blend [_SrcBlend] [_DstBlend]
		Pass {
            GLSLPROGRAM
                #version 310 es
                #include "UnityCG.glslinc"
                #pragma only_renderers gles gles3
				#pragma multi_compile STEREO_CUSTOM_UV_OFF STEREO_CUSTOM_UV_ON
				#pragma multi_compile USE_OES_FAST_PATH_OFF USE_OES_FAST_PATH_ON
                
                #extension GL_OES_EGL_image_external       : enable
                // Full specification:
                // https://www.khronos.org/registry/OpenGL/extensions/OES/OES_EGL_image_external_essl3.txt
                // Note that #version, #extension, and some others are passed straight through (ignoring any preprocessor defines). See glcpp
                // Source: https://github.com/aras-p/glsl-optimizer/tree/master/src/glsl
                // This means that we cannot "require" this extension as it would break backwards compatibility with OpenGLES 2. For now, "enable" should suffice.
                #extension GL_OES_EGL_image_external_essl3 : enable

                // OVR_multiview + OVR_multiview2
                // Full specifications:
                // https://www.khronos.org/registry/OpenGL/extensions/OVR/OVR_multiview.txt
                // https://www.khronos.org/registry/OpenGL/extensions/OVR/OVR_multiview2.txt
                // Multiview2 relaxes the constraints of only gl_Position being able to be influenced by gl_ViewID_OVR
                // Multiview2 implicitly enabled multiview but here we are being explicit

                #extension GL_OVR_multiview                : enable
                #extension GL_OVR_multiview2               : enable

                // Force high precision on our shader to prevent rounding errors from creeping up
                precision highp float;

                #ifdef VERTEX
                    #if __VERSION__ >= 300
                        /// Unity Stereo uniforms
                        layout(std140) uniform UnityStereoGlobals {
                            mat4 unity_StereoMatrixP[2];
                            mat4 unity_StereoMatrixV[2];
                            mat4 unity_StereoMatrixInvV[2];
                            mat4 unity_StereoMatrixVP[2];
                            mat4 unity_StereoCameraProjection[2];
                            mat4 unity_StereoCameraInvProjection[2];
                            mat4 unity_StereoWorldToCamera[2];
                            mat4 unity_StereoCameraToWorld[2];
                            vec3 unity_StereoWorldSpaceCameraPos[2];
                            vec4 unity_StereoScaleOffset[2];
                        };
                        
                        layout(std140) uniform UnityStereoEyeIndices {
                            vec4 unity_StereoEyeIndices[2];
                        };
                        
                        #if defined(STEREO_MULTIVIEW_ON)
                            // For GL_OVR_multiview we use gl_ViewID_OVR to get the current view index
                            layout(num_views = 2) in; 
                        #endif
                    #endif

                    #define SHADERLAB_GLSL

                    varying vec2 uvs;
                    uniform vec4 _MainTex_ST;
                    #if defined (STEREO_CUSTOM_UV_ON)
                        varying vec2 texcoord[2];
	          			uniform int unity_StereoEyeIndex;
                    #endif

                    vec2 transformTex(vec2 texCoord, vec4 texST) {
                        return (texCoord.xy * texST.xy + texST.zw);
                    }

                    void main() {
                        #if defined(STEREO_MULTIVIEW_ON)
                            gl_Position = unity_StereoMatrixVP[gl_ViewID_OVR] * unity_ObjectToWorld * gl_Vertex;
                        #else
                            gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
                        #endif

                        #if defined (STEREO_CUSTOM_UV_ON)
                            texcoord[0] = gl_MultiTexCoord0.xy;
                            texcoord[1] = gl_MultiTexCoord1.xy;
                            #ifdef STEREO_MULTIVIEW_ON 
                                uvs = transformTex(texcoord[gl_ViewID_OVR], _MainTex_ST);
                            #else
                                uvs = transformTex(texcoord[unity_StereoEyeIndex], _MainTex_ST);
                            #endif
                        #else
                            uvs = transformTex(gl_MultiTexCoord0.xy, _MainTex_ST);
                        #endif
                    }
                #endif  

                #ifdef FRAGMENT
                    varying vec2 uvs;
                    #if defined (USE_OES_FAST_PATH_ON)
                        uniform samplerExternalOES _MainTex;
                    #else
                        uniform sampler2D _MainTex;
                    #endif
                    uniform vec4 _Color;
                    void main() {
                        #if defined(SHADER_API_GLES3) || defined(SHADER_API_GLES) || defined(SHADER_API_METAL)
                            #if __VERSION__ >= 300
                                gl_FragColor = texture(_MainTex, uvs.xy) * _Color;
                            #else
                                gl_FragColor = texture2D(_MainTex, uvs.xy) * _Color;
                            #endif
                        #else
                            // Not supported
                            gl_FragColor = vec4(1.0, 1.0, 0.0, 1.0) * _Color;;
                        #endif
                    }
                #endif
            ENDGLSL
        }
    }
	SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Background" "IgnoreProjector" = "True"  }
		LOD 100
		Lighting Off
		Cull Off
		ZWrite [_ZWrite]
        Blend [_SrcBlend] [_DstBlend]
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma only_renderers metal
			#pragma multi_compile STEREO_CUSTOM_UV_OFF STEREO_CUSTOM_UV_ON
			#pragma multi_compile USE_OES_FAST_PATH_OFF USE_OES_FAST_PATH_ON

			#include "UnityCG.cginc"

			struct appdata {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
#if STEREO_CUSTOM_UV
				float2 uv2 : TEXCOORD1;
#endif
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			uniform sampler2D _MainTex;
			uniform float4 _MainTex_ST;
			uniform float3 _cameraPosition;
			uniform fixed4 _Color;

			v2f vert (appdata appData) {
				v2f output;

				output.vertex = UnityObjectToClipPos(appData.vertex);
				output.uv.xy = TRANSFORM_TEX(appData.uv, _MainTex);

#if STEREO_CUSTOM_UV
				if (!IsStereoEyeLeft(_cameraPosition, UNITY_MATRIX_V[0].xyz))
				{
					output.uv.xy = TRANSFORM_TEX(appData.uv2, _MainTex);
				}
#endif
				return output;
			}
			
			fixed4 frag (v2f input) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, input.uv.xy);
				col *= _Color;
				return fixed4(col.rgb, 1.0);
			}
			ENDCG
		}
	}    
}