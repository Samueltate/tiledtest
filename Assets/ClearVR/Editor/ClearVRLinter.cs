using UnityEditor;
using UnityEngine;
using System;
#if UNITY_EDITOR
namespace com.tiledmedia.clearvr {

    [InitializeOnLoad]
	public class ClearVRLinter {
		private static bool isUnityRunningInBatchmode = false;
		private static readonly string clearVRWelcomeNoticeShownKey = "clear_vr_is_welcome_notice_shown";
		private static bool clearVRIsWelcomeNoticeShown {
			get {
				return PlayerPrefs.GetInt(clearVRWelcomeNoticeShownKey, 1) == 1;
			}

			set {
				PlayerPrefs.SetInt(clearVRWelcomeNoticeShownKey, value ? 1 : 0);
			}
		}

		static ClearVRLinter() {
			EditorApplication.delayCall += OnDelayedCall;
		}

		static void OnDelayedCall() {
			if (System.Environment.CommandLine.Contains("-batchmode")) {
				isUnityRunningInBatchmode = true;
			}
#if UNITY_ANDROID
			if(!clearVRIsWelcomeNoticeShown && !isUnityRunningInBatchmode) {
				EditorUtility.DisplayDialog("ClearVR",
						"ClearVR requires a custom activity to be set in your AndroidManifest.xml\n" + 
						"Use the ClearVR menu item to change your AndroidManifest.xml according to\n" + 
						"the platform (e.g. flat/cardboard/oculus/PicoVR) you target." + 
						"Ok",
						"");
				clearVRIsWelcomeNoticeShown = true;
			}
#endif
		}

		static String ANDROID_MANIFEST_XML_FILE_FULL_PATH = Application.dataPath + "/Plugins/Android/AndroidManifest.xml";

        [MenuItem("ClearVR/Android/Setup Manifest XML/All other targets")]
		public static void SetupAndroidManifestForAllOtherTargets() {
			XMLHelpers.UpdateAndroidManifestXml(ANDROID_MANIFEST_XML_FILE_FULL_PATH, "/manifest/application/activity", "android:name", "com.tiledmedia.clearvrforunityandroid.ClearVRForUnityAndroidActivity", true);
		}

        [MenuItem("ClearVR/Android/Setup Manifest XML/WaveVR")]
		public static void SetupAndroidManifestForWaveVR() {
			XMLHelpers.UpdateAndroidManifestXml(ANDROID_MANIFEST_XML_FILE_FULL_PATH, "/manifest/application/activity", "android:name", "com.tiledmedia.clearvrforunityandroid.ClearVRForUnityForWaveVRAndroidActivity", true);
		}

        [MenuItem("ClearVR/Android/Setup Manifest XML/PicoVR")]
		public static void SetupAndroidManifestForPicoVR() {
			XMLHelpers.UpdateAndroidManifestXml(ANDROID_MANIFEST_XML_FILE_FULL_PATH, "/manifest/application/activity", "android:name", "com.tiledmedia.clearvrforunityandroid.ClearVRForUnityForPicoVRAndroidActivity", true);
		}

		public static bool CheckClearVRActivityInAndroidManifest() {
			String activitySpecifiedInAndroidManifest = XMLHelpers.GetValueFromKeyInAndroidManifestXmlSafely(ANDROID_MANIFEST_XML_FILE_FULL_PATH, "/manifest/application/activity", "android:name");
			if(String.IsNullOrEmpty(activitySpecifiedInAndroidManifest)) {
				UnityEngine.Debug.LogWarning(String.Format("[ClearVR] Unable to determine android activity from {0}. Please make sure to use the ClearVR activity through the ClearVR -> Android menu.", ANDROID_MANIFEST_XML_FILE_FULL_PATH));
				return false;
			}
			if(!(activitySpecifiedInAndroidManifest.ToLower().Contains("clearvr"))) {
				UnityEngine.Debug.LogError(String.Format("[ClearVR] The activity {0} specified in {1} does not point to a ClearVR activity. Please make sure to use the ClearVR activity through the ClearVR -> Android menu.", activitySpecifiedInAndroidManifest, ANDROID_MANIFEST_XML_FILE_FULL_PATH));
				return false;
			}
			return true;
		}
	}
}
#endif