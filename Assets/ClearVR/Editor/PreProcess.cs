using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Diagnostics;
using UnityEditor.Build;
using System.Linq;
#if UNITY_IOS
using UnityEditor.iOS.Xcode.Extensions;
using UnityEditor.iOS.Xcode;
#endif
#if UNITY_2018_1_OR_NEWER
using UnityEditor.Build.Reporting;
#endif

namespace com.tiledmedia.clearvr {
#if UNITY_2018_1_OR_NEWER
    public class PreProcess : IPreprocessBuildWithReport {
#else
    public class PreProcess : IPreprocessBuild {
#endif    
        public int callbackOrder { get { return 0; } }
#if UNITY_2018_1_OR_NEWER
        public void OnPreprocessBuild(BuildReport argReport) {
            BuildTarget buildTarget = argReport.summary.platform;
            String buildPath = argReport.summary.outputPath;
#else
        public void OnPreprocessBuild(BuildTarget buildTarget, string buildPath) {
#endif
            switch(buildTarget) {
                case BuildTarget.iOS:
                    if(!IOSSelectProperNativeRendererPlugin(buildTarget, buildPath)) {
                        throw new Exception("[ClearVR] Unable to include correct native renderer plugin on iOS. The generated Xcode project might be broken. Please contact Tiledmedia.");
                    }
                    break;
                case BuildTarget.Android:
                    AndroidCheckAndroidManifestXML(buildTarget, buildPath);
                    break;
                default:
                    break;
            } 
        }
        /// <summary>
        /// Include the Unity 2017 or Unity 2018-compatible NativeRendererPlugin.
        /// </summary>
        /// <param name="argBuildTarget"></param>
        /// <param name="argBuildPath"></param>
        /// <returns></returns>
        private bool IOSSelectProperNativeRendererPlugin(BuildTarget argBuildTarget, String argBuildPath) {
            String enableVersion;
            String disableVersion;
#if UNITY_2018_1_OR_NEWER
            enableVersion = "2018";
            disableVersion = "2017";
#else
            enableVersion = "2017";
            disableVersion = "2018";
#endif
            String sourceFileRelativePath = "Assets/ClearVR/Plugins/iOS/libClearVRNativeRendererPlugin-{0}.a";
            AssetDatabase.MoveAsset(String.Format(sourceFileRelativePath, enableVersion) + ".disabled", String.Format(sourceFileRelativePath, enableVersion));
            AssetDatabase.MoveAsset(String.Format(sourceFileRelativePath, disableVersion), String.Format(sourceFileRelativePath, disableVersion) + ".disabled");
            AssetDatabase.ImportAsset(String.Format(sourceFileRelativePath, enableVersion), ImportAssetOptions.ForceUpdate);
            AssetDatabase.ImportAsset(String.Format(sourceFileRelativePath, disableVersion) + ".disabled", ImportAssetOptions.ForceUpdate);
            return true;
        }

        private bool AndroidCheckAndroidManifestXML(BuildTarget argBuildTarget, String argBuildPath) {
            return ClearVRLinter.CheckClearVRActivityInAndroidManifest();
        }
    }    
}