using UnityEngine;
using UnityEditor;
using System;
using System.IO;

#if UNITY_EDITOR
namespace com.tiledmedia.clearvr {
    [InitializeOnLoad]
	public class ClearVRSDKUpdater {
		static ClearVRSDKUpdater() {
			EditorApplication.delayCall += OnDelayCall;
		}
		private static void OnDelayCall() {
			if(ShouldAttemptPluginUpdate()) {
				AttemptPluginUpdate();
				AttemptPicoVRPatch();
			}
		}
		private static void AttemptPluginUpdate() {
			CheckForUpgradeToClearVRSDKv51Android();
		}

		private static bool ShouldAttemptPluginUpdate() {
			return !Application.isPlaying;
		}
        
        /// <summary>
        /// This method checks if we upgrade to a ClearVRSDK v5.1. ClearVRSDK v5.1 introduced fat multi-arch AAR Android libraries and changes the names of the existing libraries.
		/// We need to remove the old ones as they will clash with the new ones, resulting in failing builds.
        /// </summary>
        private static void CheckForUpgradeToClearVRSDKv51Android() {
			// Check for any older -arm-v7a-*.aar files. They have been renamed to filenames without an architecture now that we support ARM64 and arm-v7a on Android through fat AAR files.
            String androidPluginsFolderRelativePath = "Assets/ClearVR/Plugins/Android/";
            string [] fileEntries = Directory.GetFiles(androidPluginsFolderRelativePath, "*ClearVR*-arm-v7a*");
			bool isOldSDKFileFound = false;
            foreach(string fileName in fileEntries) {
				if(Path.GetExtension(fileName) != ".meta") { // ignore residual meta files, they are harmless and will be removed by Unity lateron anyway.
					isOldSDKFileFound = true;
					break;
				}
            }
			if(isOldSDKFileFound) {
				bool result = EditorUtility.DisplayDialog("ClearVR - Removing obsolete libraries", "One or multiple obsolete ClearVR Android libraries (.aar) files have been detected. As part of upgrading to the latest ClearVR SDK they must be removed. This process will not touch any other file.", "Remove", "Cancel");
				if(result) {
					foreach(string fileName in fileEntries) {
						AssetDatabase.DeleteAsset(fileName);
					}
				}
			}
        }
		private static void AttemptPicoVRPatch() {
            String androidPluginsFolderRelativePath = "Assets/Plugins/Android/";
            string [] fileEntries = Directory.GetFiles(androidPluginsFolderRelativePath, "android-support-v4.jar");
			if(fileEntries.Length == 1) {
				bool result = EditorUtility.DisplayDialog("ClearVR - Removing PicoVR support library", "PicoVR and ClearVR both provide the same android-support-v4.jar library. In order to prevent build errors, we will now remove one of them. This process will not touch any other file.", "Remove", "Cancel");
				if(result) {
					foreach(string fileName in fileEntries) {
						AssetDatabase.DeleteAsset(fileName);
					}
				}
			}

		}
	}
}
#endif