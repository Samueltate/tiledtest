using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Diagnostics;
using UnityEditor.Build;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
using UnityEditor.iOS.Xcode.Extensions;
#endif
#if UNITY_2018_1_OR_NEWER
using UnityEditor.Build.Reporting;
#endif

namespace com.tiledmedia.clearvr {
#if UNITY_2018_1_OR_NEWER
    public class PostProcess : IPostprocessBuildWithReport {
#else
    public class PostProcess : IPostprocessBuild {
#endif
        public int callbackOrder { get { return 0; } }
#if UNITY_2018_1_OR_NEWER
        public void OnPostprocessBuild(BuildReport argReport) {
            BuildTarget buildTarget = argReport.summary.platform;
            String buildPath = argReport.summary.outputPath;
#else
        public void OnPostprocessBuild(BuildTarget buildTarget, string buildPath) {
#endif
            switch(buildTarget) {
                case BuildTarget.iOS:
#if UNITY_IOS
                    var projPath = buildPath + "/Unity-iPhone.xcodeproj/project.pbxproj";
                    var proj = new PBXProject();
                    proj.ReadFromFile(projPath);
                    var targetGuid = proj.TargetGuidByName(PBXProject.GetUnityTargetName());
                    
                    proj.SetBuildProperty(targetGuid, "ENABLE_BITCODE", "NO");
                    proj.AddBuildProperty(targetGuid, "LD_RUNPATH_SEARCH_PATHS", "@executable_path/Frameworks $(PROJECT_DIR)/lib/$(CONFIGURATION) $(inherited)");
                    // proj.AddBuildProperty(targetGuid, "FRAMEWORK_SEARCH_PATHS",
                    //     "$(inherited)\n$(PROJECT_DIR)\n$(PROJECT_DIR)/Frameworks\n$(PROJECT_DIR)/Data/Raw/");
                    proj.AddBuildProperty(targetGuid, "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES", "YES");
                    proj.AddBuildProperty(targetGuid, "DYLIB_INSTALL_NAME_BASE", "@rpath");
                    proj.AddBuildProperty(targetGuid, "LD_DYLIB_INSTALL_NAME",
                        "@executable_path/../Frameworks/$(EXECUTABLE_PATH)");
                    proj.AddBuildProperty(targetGuid, "DEFINES_MODULE", "YES");
                    proj.AddBuildProperty(targetGuid, "SWIFT_VERSION", "4.0");
                    proj.AddBuildProperty(targetGuid, "COREML_CODEGEN_LANGUAGE", "Swift");
                    proj.SetBuildProperty(targetGuid, "LD_RUNPATH_SEARCH_PATHS", "$(inherited) @executable_path/Frameworks");

                    // Remove broken link to the Unity added MediaFlow.framework
                    string fileGuid = proj.FindFileGuidByProjectPath("Frameworks/ClearVR/Plugins/iOS/MediaFlow.framework");
                    if (fileGuid != null) {
                        proj.RemoveFile(fileGuid);
                    }
                    
                    // Get paths for MediaFlow.framework
                    string mediaFlowFrameworkName = "MediaFlow.framework";
                    string frameworkDiskPath = buildPath + "/Frameworks/ClearVR/Plugins/iOS/" + mediaFlowFrameworkName;
                    string frameworkRelativePath = "Frameworks/ClearVR/Plugins/iOS/" + mediaFlowFrameworkName;
                    string frameworkProjectPath = "Frameworks/" + mediaFlowFrameworkName;
                    string frameworkPhaseGuid = proj.AddFrameworksBuildPhase(targetGuid);
                    
                    // Add MediaFlow.framework to project and to "Linked Frameworks and Libraries"
                    fileGuid = proj.AddFile(frameworkRelativePath, frameworkProjectPath, PBXSourceTree.Source);
                    proj.AddFileToBuildSection(targetGuid, frameworkPhaseGuid, fileGuid);
                    
                    // Add MediaFlow.framework to "EmbeddedBinaries"
                    PBXProjectExtensions.AddFileToEmbedFrameworks(proj, targetGuid, fileGuid);
                    proj.WriteToFile(projPath);
#endif
                    break;
                case BuildTarget.Android:
                    // Intentionally left empty.
                    break;
                default:
                    break;

            }
        }
    }
}